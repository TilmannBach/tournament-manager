# Heavy Tournament Manager

## Overview
Heavy Tournamnet Manager is a tool to manage tournaments and whole LAN-Parties. 
User can register for tournaments, create teams and challenge each other.
Users can get in touch with each other using collaborative features. 

Heavy Tournamnet Manager has all Features you need for your LAN-Parties:
* Event management (past and current events)
* User Management (self-service including forming of Clans)
* Tournaments (which of course is the main feature!)
* Chat
* Live notifications (user specific)
* News
* Admission management 

## Installation
The tournament manager is available as container image and needs an OpenIdConnect (OIDC) service for authentication/authorization.
User creation and management is offloaded to the OIDC provider.

### Keycloak as OIDC service
* Install keycloak as described in the offical docs or use our docker-compose example in `/build`
* If using our `docker-compose-keycloak.yml` example you have to follow these steps:
  * uncomment the lines containing the ENV variables for `KEYCLOAK_USER` and `KEYCLOAK_PASSWORD` **ONLY** for the first startup
  * the values set here are your admin account for Keycloak
  * restart the service with the lines commented out again
  * you can access the Keycloak UI now under [http://127.0.0.1:8080](http://127.0.0.1:8080/auth/admin/master/console)
* Keycloak needs to be configured for usage with Heavy Tournament Manager:
  * Create a new "Client" with the Client ID `tournaments`
  
  ![create_client](docs/images/oidc-add-client.png "Create OIDC Client")
  ![client configuration](docs/images/oidc-tournaments-client.png "OIDC Client configuration")
  
  * Provide additional roles for this client
  
  ![client roles](docs/images/oidc-tournaments-roles.png "OIDC Client roles")

  * Assign this role to your admin accounts, e.g. using a "Group"
  
  ![group roles](docs/images/oidc-tournaments-group-roles.png "OIDC Group roles")

  * Assign this group to the users you want to be "Managers" for your tournament manager
  * Create a new Protocol Mapper
  
  ![create protocol mapper](docs/images/oidc-tournaments-client-create-protocol-mapper.png "OIDC Create Protocol Mapper")
### Tournament Manager service
* Provide a MariaDB instance and start the backend
  * you can use our  `docker-compose.yml` from the `/build` directory

### SSL/TLS is required
You need a SSL endpoint serving the Heavy Tournament Manager app! 
This is because Webbrowsers wont open insecure Websocket connections to the Backend.
Only for local testing purposes (localhost) those connections will work.

We decided not to include SSL termination in Heavy Tournament Manager.
SSL termination is also not included in Keycloak by default.
We recommend you to use a reverse proxy doing SSL offloading.
For our LAN-Party events we are using the great [Traefik](https://doc.traefik.io/traefik/) project.

## Developing

### Prerequisites
* docker & docker-compose
* .NET 6 SDK (https://dotnet.microsoft.com/download)

### Run it
* start database: `docker-compose -f build/docker-compose.yml up`
  * root login can be found in [docker-compose.yml](docker-compose.yml)
  * You can access the DB with any MySQL client
* start keycloak: `docker-compose -f build/docker-compose-keycloak.yml up`
  * configure keycloak like decribed in [Keycloak as OIDC service](#keycloak-as-oidc-service)
* run the project: `dotnet run`

### Configuration
* configuration is done via appsettings.json / environment variables
* set client secret for keycloak client: `dotnet user-secrets set "OpenIdConnect:ClientSecret" "SOMESECRET"`

![client_secret](docs/images/oidc_clientsecret.png "OIDC Client Secret")

* set client secret for keycloak admin user password: `dotnet user-secrets set "Keycloak:AdminPassword" "ADMINPASSWORD"`

## Deployment

The Tournament Manager is build to support `multiarch` images.
Gitlab registry hosts images for `amd64` and `arm64`.
You can use them either by adding `-amd64`/`-arm64` or you let the environment decide which image should be pulled.

### Backend


```shell
registry.gitlab.com/clanwars/tournament-manager/backend:<tag>
```

Configuration is done via environment variables.

### Frontend
```shell
registry.gitlab.com/clanwars/tournament-manager/fronted:<tag>
```

For configuration please update `/usr/share/nginx/html/appsettings.json` inside the container:

1) Docker
   - mount a local `appsettings.json` file to this location.
2) Kubernetes
   - use a `ConfigMap` which generates this file in the container.


## Linux

### Install certificates
 
- get `mkcert` (https://github.com/FiloSottile/mkcert)

```shell
$ mkcert -install
$ mkdir -p /<user>/certs
$ cd /<user>/certs
$ mkcert example.com "*.example.com" example.test localhost 127.0.0.1 ::1
$ openssl pkcs12 -export -out example.com.pfx -inkey example.com+5-key.pem -in example.com+5.pem
# add env variable to bashrc or zshrc
cat << EOF >> ~/.bashrc
# .NET
export ASPNETCORE_Kestrel__Certificates__Default__Password="PASSWORD" # if password is used
export ASPNETCORE_Kestrel__Certificates__Default__Path="/<user>/certs/example.com.pfx"
EOF
```
