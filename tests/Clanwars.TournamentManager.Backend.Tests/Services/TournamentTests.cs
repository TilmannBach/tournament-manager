using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Backend.Services.Pipeline;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using FluentAssertions;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using Sieve.Models;
using Sieve.Services;
using SignalR_UnitTestingSupportCommon.IHubContextSupport;
using Xunit;

namespace Clanwars.TournamentManager.Backend.Tests.Services;

[ExcludeFromCodeCoverage]
public class TournamentsDoubleEliminationMatchesGeneratorTests
{
    [Fact]
    public async Task StartSingleEliminationTournament_With4teams_ShouldGenerateTreeWith4matches()
    {
        // arrange
        var tournament = new Tournament
        {
            Name = "4 Teams Single Elimination",
            Mode = TournamentMode.SingleElimination,
            Teams = new List<TournamentTeam>
            {
                new()
                {
                    Name = "Team A"
                },
                new()
                {
                    Name = "Team B"
                },
                new()
                {
                    Name = "Team C"
                },
                new()
                {
                    Name = "Team D"
                },
            }
        };
        var options = new DbContextOptionsBuilder<TournamentManagerDbContext>()
            .UseInMemoryDatabase(databaseName: "TournamentManager")
            .Options;
        var context = new TournamentManagerDbContext(options);
        var sieveProcessor = new Mock<ISieveProcessor>();
        var sieveOptions = new Mock<IOptions<SieveOptions>>();
        var tournamentRepository = new TournamentRepository(context, sieveProcessor.Object, sieveOptions.Object);
        var tournamentMatchRepository = new TournamentMatchRepository(context, sieveProcessor.Object, sieveOptions.Object);
        var tournamentTeamRepository = new TournamentTeamRepository(context, sieveProcessor.Object, sieveOptions.Object);

        var iHubContextSupport = new UnitTestingSupportForIHubContext<NotificationsHub, INotificationsSender>();

        var eventService = new EventService(
            new EventRepository(context, sieveProcessor.Object, sieveOptions.Object),
            iHubContextSupport.IHubContextMock.Object,
            new Mock<IBeamerContentProvider>().Object
        );

        var tournamentMatchService = new TournamentMatchService(
            tournamentMatchRepository,
            new Mock<IHubContext<NotificationsHub, INotificationsSender>>().Object,
            new Mock<IBeamerContentProvider>().Object
        );
        var tournamentTeamService = new TournamentTeamService(
            tournamentTeamRepository,
            iHubContextSupport.IHubContextMock.Object
        );
        var tournamentService = new TournamentService(
            tournamentRepository,
            tournamentTeamService,
            tournamentMatchService,
            iHubContextSupport.IHubContextMock.Object,
            eventService
        );

        await tournamentRepository.AddTournamentAsync(tournament);

        // act
        await tournamentService.StartTournamentAsync(tournament);

        // assert
        tournament.Rounds.Should().HaveCount(2);
        tournament.Rounds.SelectMany(r => r.Matches).Should().HaveCount(4);

        iHubContextSupport.ClientsAllMock
            .Verify(
                x => x.SendTournamentsChanged(new TournamentHubMessage { Id = tournament.Id, Event = TournamentChangeEvent.Started }),
                Times.Once()
            );
    }
}
