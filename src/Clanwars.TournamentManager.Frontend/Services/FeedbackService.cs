using Clanwars.TournamentManager.Lib.Messages.Feedback;
using MudBlazor;

namespace Clanwars.TournamentManager.Frontend.Services;

public class FeedbackService(HttpClient httpClient, ISnackbar snackbar) : AbstractHttpService(httpClient, snackbar), IFeedbackService
{
    private const string BasePath = "api/v1/feedback";

    public async Task<FeedbackResponse> CreateFeedbackAsync(CreateFeedbackMessage createFeedbackMessage)
    {
        var newFeedback = await PostAsync<CreateFeedbackMessage, FeedbackResponse>($"{BasePath}", createFeedbackMessage);
        return newFeedback;
    }

    public async Task<IList<FeedbackResponse>> GetFeedbacksAsync()
    {
        return await GetAllAsync<FeedbackResponse>(BasePath);
    }

    public async Task SetFeedbackResolvedState(FeedbackResponse feedback, bool contextResolved)
    {
        var newState = contextResolved ? "resolve" : "unresolve";
        await PutAsync($"{BasePath}/{feedback.Id}/{newState}");
    }
}
