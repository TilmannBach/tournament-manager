using Clanwars.TournamentManager.Lib.Messages;
using Clanwars.TournamentManager.Lib.Messages.Images;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface IImagesService
{
    Task SetTournamentImageAsync(int tournamentId, ImageUpload imageUpload);
    Task SetTournamentTeamImageAsync(int tournamentId, ImageUpload imageUpload);
    Task SetClanImageAsync(int tournamentId, ImageUpload imageUpload);
    Task SetUserImageAsync(int tournamentId, ImageUpload imageUpload);
    Task AddBeamerImageAsync(ImageUpload imageUpload);
    Task RemoveImageAsync(string imageHashId);
    Task<IList<ImageResponse>> GetBeamerImageIdsAsync();
    Task ActivateBeamerImageAsync(string imageHashId, int timeout);
    Task SetShowOnStreamForBeamerImageAsync(string imageHashId, bool showOnStream);
}
