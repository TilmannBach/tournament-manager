using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Shouts;
using MudBlazor;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public class ShoutsService : AbstractHttpService, IShoutsService
{
    private const string BasePath = "api/v1/shouts";

    public ShoutsService(HttpClient httpClient, ISnackbar snackbar) : base(httpClient, snackbar)
    {
    }

    public Task<IPagedResult<ShoutResponse>> GetAsync(SieveModel model)
    {
        return GetPagedAsync<ShoutResponse>(BasePath, model);
    }

    public async Task<ShoutResponse> GetAsync(int id)
    {
        var pagedResult = await GetPagedAsync<ShoutResponse>(BasePath, new SieveModel {Filters = $"Id=={id}", PageSize = 1});
        if (pagedResult.TotalCount != 1)
        {
            throw new ArgumentException($"Cannot find shout with id {id}", nameof(id));
        }

        return pagedResult.Results.Single();
    }

    public Task<IList<ShoutResponse>> GetAllAsync(SieveModel model)
    {
        return GetAllAsync<ShoutResponse>(BasePath, model.Filters, model.Sorts);
    }

    public Task<ShoutResponse> CreateAsync(CreateShoutMessage createShoutMessage)
    {
        return PostAsync<CreateShoutMessage, ShoutResponse>(BasePath, createShoutMessage);
    }

    public Task<ShoutResponse> UpdateAsync(int shoutId, UpdateShoutMessage updateShoutMessage)
    {
        return PutAsync<UpdateShoutMessage, ShoutResponse>($"{BasePath}/{shoutId}", updateShoutMessage);
    }

    public Task DeleteAsync(int shoutId)
    {
        return DeleteAsync($"{BasePath}/{shoutId}");
    }
}
