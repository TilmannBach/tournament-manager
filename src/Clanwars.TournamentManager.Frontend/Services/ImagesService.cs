using Clanwars.TournamentManager.Lib.Messages;
using Clanwars.TournamentManager.Lib.Messages.Images;
using MudBlazor;

namespace Clanwars.TournamentManager.Frontend.Services;

public class ImagesService(HttpClient httpClient, ISnackbar snackbar) : AbstractHttpService(httpClient, snackbar), IImagesService
{
    private const string BasePath = "api/v1/images";

    public Task SetTournamentImageAsync(int tournamentId, ImageUpload imageUpload)
    {
        return PostAsync($"{BasePath}/upload/tournament/{tournamentId}", imageUpload);
    }

    public Task SetTournamentTeamImageAsync(int tournamentId, ImageUpload imageUpload)
    {
        return PostAsync($"{BasePath}/upload/tournament-team/{tournamentId}", imageUpload);
    }

    public Task SetClanImageAsync(int tournamentId, ImageUpload imageUpload)
    {
        return PostAsync($"{BasePath}/upload/clan/{tournamentId}", imageUpload);
    }

    public Task SetUserImageAsync(int tournamentId, ImageUpload imageUpload)
    {
        return PostAsync($"{BasePath}/upload/user/{tournamentId}", imageUpload);
    }

    public Task AddBeamerImageAsync(ImageUpload imageUpload)
    {
        return PostAsync($"{BasePath}/upload/beamer", imageUpload);
    }

    public Task RemoveImageAsync(string imageHashId)
    {
        return DeleteAsync($"{BasePath}/{imageHashId}");
    }

    public Task<IList<ImageResponse>> GetBeamerImageIdsAsync()
    {
        return GetAllAsync<ImageResponse>($"{BasePath}?Filters=Type==Beamer");
    }

    public Task ActivateBeamerImageAsync(string imageHashId, int timeout)
    {
        return PostAsync($"api/v1/Pipeline/activate-image/{imageHashId}/{timeout}");
    }

    public Task SetShowOnStreamForBeamerImageAsync(string imageHashId, bool showOnStream)
    {
        var action = showOnStream ? "show" : "hide";
        return PutAsync($"api/v1/Images/{imageHashId}/beamer/{action}");
    }
}
