using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface ITournamentTeamsService
{
    Task<IPagedResult<TournamentTeamResponse>> GetAsync(SieveModel model);
    Task<TournamentTeamResponse> GetAsync(int id);
    Task<IList<TournamentTeamResponse>> GetAllAsync(SieveModel model);
    Task<TournamentTeamResponse> CreateAsync(CreateTournamentTeamMessage createTournamentTeamMessage);
    Task<TournamentTeamResponse> UpdateAsync(int tournamentTeamId, UpdateTournamentTeamMessage updateTournamentTeamMessage);
    Task DeleteAsync(int tournamentTeamId);
    Task<TournamentTeamResponse> JoinAsync(int tournamentTeamId);
    Task LeaveAsync(int tournamentTeamId);
    Task<TournamentTeamResponse> ApproveJoinAsync(int tournamentTeamId, int targetUserId);
    Task RemoveUserAsync(int tournamentTeamId, int targetUserId);
    Task GrantOwnershipAsync(int tournamentTeamId, int targetUserId);
    Task RevokeOwnershipAsync(int tournamentTeamId, int targetUserId);
}
