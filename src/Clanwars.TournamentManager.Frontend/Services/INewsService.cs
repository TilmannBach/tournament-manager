using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.News;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface INewsService
{
    Task<IPagedResult<NewsResponse>> GetAsync(SieveModel model);
    Task<NewsResponse> GetAsync(int id);
    Task<IList<NewsResponse>> GetAllAsync(SieveModel model);
    Task<NewsResponse> CreateAsync(CreateNewsMessage createNewsMessage);
    Task<NewsResponse> UpdateAsync(int newsId, UpdateNewsMessage updateNewsMessage);
    Task DeleteAsync(int newsId);
}
