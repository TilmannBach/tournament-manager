using Clanwars.TournamentManager.Lib.Messages.Feedback;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface IFeedbackService
{
    Task<FeedbackResponse> CreateFeedbackAsync(CreateFeedbackMessage createFeedbackMessage);

    Task<IList<FeedbackResponse>> GetFeedbacksAsync();
    Task SetFeedbackResolvedState(FeedbackResponse feedback, bool contextResolved);
}
