using Clanwars.TournamentManager.Lib.Messages.User;
using Microsoft.AspNetCore.Components.Authorization;
using MudBlazor;

namespace Clanwars.TournamentManager.Frontend.Services;

internal class CurrentUserService(
    HttpClient httpClient,
    AuthenticationStateProvider authenticationStateProvider,
    ISnackbar snackbar)
    : AbstractHttpService(httpClient, snackbar), ICurrentUserService
{
    public async Task<UserResponse> GetCurrentUser()
    {
        return (await GetAsync<UserResponse>("api/v1/users/me"))!;
    }

    public async Task<bool> CurrentUserIsManager()
    {
        var userAuthState = await authenticationStateProvider.GetAuthenticationStateAsync();
        return userAuthState.User.IsInRole("tournament-moderator");
    }
}
