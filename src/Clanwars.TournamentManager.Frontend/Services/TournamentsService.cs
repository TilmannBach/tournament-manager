using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using MudBlazor;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public class TournamentsService : AbstractHttpService, ITournamentsService
{
    private const string BasePath = "api/v1/tournaments";

    public TournamentsService(HttpClient httpClient, ISnackbar snackbar) : base(httpClient, snackbar)
    {
    }

    public Task<IPagedResult<TournamentResponse>> GetAsync(SieveModel model)
    {
        return GetPagedAsync<TournamentResponse>(BasePath, model);
    }

    public async Task<TournamentResponse> GetAsync(int id)
    {
        var pagedResult = await GetPagedAsync<TournamentResponse>(BasePath, new SieveModel {Filters = $"Id=={id}", PageSize = 1});
        if (pagedResult.TotalCount != 1)
        {
            throw new ArgumentException($"Cannot find tournament with id {id}", nameof(id));
        }

        return pagedResult.Results.Single();
    }

    public Task<IList<TournamentResponse>> GetAllAsync(SieveModel model)
    {
        return GetAllAsync<TournamentResponse>(BasePath, model.Filters, model.Sorts);
    }

    public Task<TournamentResponse> CreateAsync(CreateTournamentMessage createTournamentMessage)
    {
        return PostAsync<CreateTournamentMessage, TournamentResponse>(BasePath, createTournamentMessage);
    }

    public Task<TournamentResponse> UpdateAsync(int tournamentId, UpdateTournamentMessage updateTournamentMessage)
    {
        return PutAsync<UpdateTournamentMessage, TournamentResponse>($"{BasePath}/{tournamentId}", updateTournamentMessage);
    }

    public Task DeleteAsync(int tournamentId)
    {
        return DeleteAsync($"{BasePath}/{tournamentId}");
    }

    public Task<TournamentResponse> StartTournamentAsync(int tournamentId)
    {
        return PutAsync<TournamentResponse>($"{BasePath}/{tournamentId}/start");
    }

    public Task<TournamentMatchResponse> ScoreMatchAsync(int tournamentMatchId, ScoreMatchMessage scoreMatchMessage)
    {
        return PutAsync<ScoreMatchMessage, TournamentMatchResponse>($"{BasePath}/score-match/" + tournamentMatchId, scoreMatchMessage);
    }
}
