using System.Collections.Concurrent;
using Clanwars.TournamentManager.Frontend.Options;
using Clanwars.TournamentManager.Lib.Messages.Pipeline;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Options;
using MudBlazor;

namespace Clanwars.TournamentManager.Frontend.Services;

public sealed class BeamerPipelineService : AbstractHttpService, IBeamerPipelineService, IDisposable
{
    private const string BasePath = "api/v1/pipeline";
    private readonly HubConnection _hubConnection;
    private bool _currentAutoplayState;
    private readonly ConcurrentBag<IAutoplayStateObserver> _autoplayStateObservers = [];
    private IBeamerHubMessage? _currentBeamerHubMessage;
    private readonly ConcurrentBag<IBeamerHubMessageObserver> _beamerHubMessageObservers = [];

    public BeamerPipelineService(HttpClient httpClient, ISnackbar snackbar, IOptions<BackendOptions> backendOptions) : base(httpClient, snackbar)
    {
        _hubConnection = new HubConnectionBuilder()
            .WithUrl(backendOptions.Value.BaseAddress.TrimEnd('/') + "/beamer-hub")
            .WithAutomaticReconnect()
            .Build();

        _hubConnection.On<bool>(nameof(IBeamerHub.AutoplayState), async autoplayState => { await NotifyAutoplayStateObservers(autoplayState); });

        _hubConnection.On<NewsBeamerHubMessage>(nameof(IBeamerHub.PublishNews), async msg => { await NotifyBeamerHubMessageObservers(msg); });
        _hubConnection.On<ShoutBeamerHubMessage>(nameof(IBeamerHub.PublishShout), async msg => { await NotifyBeamerHubMessageObservers(msg); });
        _hubConnection.On<WelcomeBeamerHubMessage>(nameof(IBeamerHub.PublishWelcome), async msg => { await NotifyBeamerHubMessageObservers(msg); });
        _hubConnection.On<ImageBeamerHubMessage>(nameof(IBeamerHub.PublishImage), async msg => { await NotifyBeamerHubMessageObservers(msg); });
        _hubConnection.On<TournamentBeamerHubMessage>(nameof(IBeamerHub.PublishTournaments), async msg => { await NotifyBeamerHubMessageObservers(msg); });
        _hubConnection.On<UpcomingMatchesBeamerHubMessage>(nameof(IBeamerHub.PublishUpcomingMatches), async msg => { await NotifyBeamerHubMessageObservers(msg); });
        _hubConnection.On<FinishedMatchesBeamerHubMessage>(nameof(IBeamerHub.PublishFinishedMatches), async msg => { await NotifyBeamerHubMessageObservers(msg); });
        _hubConnection.On<TournamentMatchResultBeamerHubMessage>(nameof(IBeamerHub.PublishTournamentMatchResult), async msg => { await NotifyBeamerHubMessageObservers(msg); });
        
        _hubConnection.StartAsync();
    }

    public async Task<IList<PipelineModuleStateResponse>> GetAllModuleStatesAsync()
    {
        return (await GetAsync<IList<PipelineModuleStateResponse>>($"{BasePath}/modulestates"))!;
    }

    public async Task Run()
    {
        await PostAsync($"{BasePath}/Run");
    }

    public async Task Stop()
    {
        await PostAsync($"{BasePath}/Stop");
    }

    public async Task Skip()
    {
        await PostAsync($"{BasePath}/Skip");
    }

    public async Task SetModuleTimeout(ModuleType module, int timeoutInitial, int timeoutRecurring)
    {
        var body = new UpdateModuleTimeoutsMessage
        {
            TimeoutInitial = timeoutInitial,
            TimeoutRecurring = timeoutRecurring
        };
        await PutAsync($"{BasePath}/{module.ToString()}", body);
    }

    public async Task EnableModule(ModuleType module)
    {
        await PutAsync($"{BasePath}/{module.ToString()}/Enable");
    }

    public async Task DisableModule(ModuleType module)
    {
        await PutAsync($"{BasePath}/{module.ToString()}/Disable");
    }

    public void RegisterAutoplayStateObserver(IAutoplayStateObserver observer)
    {
        _autoplayStateObservers.Add(observer);
        observer.UpdateAutoplayState(_currentAutoplayState);
    }

    public void UnregisterAutoplayStateObserver(IAutoplayStateObserver observer)
    {
        _autoplayStateObservers.TryTake(out observer);
    }

    private async Task NotifyAutoplayStateObservers(bool autoplayState)
    {
        _currentAutoplayState = autoplayState;
        foreach (var observer in _autoplayStateObservers)
        {
            await observer.UpdateAutoplayState(autoplayState);
        }

        if (!autoplayState)
        {
            _currentBeamerHubMessage = null;
            foreach (var observer in _beamerHubMessageObservers)
            {
                await observer.UpdateBeamerHubMessage(null);
            }
        }
    }

    public void RegisterBeamerHubMessageObserver(IBeamerHubMessageObserver observer)
    {
        _beamerHubMessageObservers.Add(observer);
        observer.UpdateBeamerHubMessage(_currentBeamerHubMessage);
    }

    public void UnregisterBeamerHubMessageObserver(IBeamerHubMessageObserver observer)
    {
        _beamerHubMessageObservers.TryTake(out observer);
    }

    private async Task NotifyBeamerHubMessageObservers(IBeamerHubMessage beamerHubMessage)
    {
        _currentBeamerHubMessage = beamerHubMessage;

        // debounce event propagation a little so not all clients rush the backend at the same time
        var rnd = new Random(DateTime.Now.Millisecond);
        Thread.Sleep(rnd.Next(0, 100));

        foreach (var observer in _beamerHubMessageObservers)
        {
            await observer.UpdateBeamerHubMessage(beamerHubMessage);
        }
    }

    public void Dispose()
    {
        _hubConnection.StopAsync();
    }
}

public interface IAutoplayStateObserver
{
    Task UpdateAutoplayState(bool autoplayState);
}

public interface IBeamerHubMessageObserver
{
    Task UpdateBeamerHubMessage(IBeamerHubMessage? beamerHubMessage);
}
