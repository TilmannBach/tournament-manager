using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Rooms;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface IRoomsService
{
    Task<IPagedResult<RoomResponse>> GetAsync(SieveModel model);
    Task<RoomResponse> GetAsync(int id);
    Task<IList<RoomResponse>> GetAllAsync(SieveModel model);
    Task<RoomResponse> CreateAsync(CreateRoomMessage createRoomMessage);
    Task<RoomResponse> CopyAsync(int roomIdToCopy, CopyRoomMessage copyRoomMessage);
    Task<RoomResponse> UpdateAsync(int roomId, UpdateRoomMessage updateRoomMessage);
    Task DeleteAsync(int roomId);
    Task<GridSquareResponse> PlaceUserAsync(int roomId, PlaceUserMessage placeUserMessage);
    Task<GridSquareResponse> UpdateGridSquareAsync(int roomId, UpdateGridMessage updateGridMessage);
    Task DeleteGridSquareAsync(int roomId, GridCoordinate deleteGridMessage);
}
