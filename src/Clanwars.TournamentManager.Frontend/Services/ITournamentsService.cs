using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using Sieve.Models;

namespace Clanwars.TournamentManager.Frontend.Services;

public interface ITournamentsService
{
    Task<IPagedResult<TournamentResponse>> GetAsync(SieveModel model);
    Task<TournamentResponse> GetAsync(int id);
    Task<IList<TournamentResponse>> GetAllAsync(SieveModel model);
    Task<TournamentResponse> CreateAsync(CreateTournamentMessage createTournamentMessage);
    Task<TournamentResponse> UpdateAsync(int tournamentId, UpdateTournamentMessage updateTournamentMessage);
    Task DeleteAsync(int tournamentId);
    Task<TournamentResponse> StartTournamentAsync(int tournamentId);

    /// <exception cref="HttpRequestException">Exception is thrown if backend answer was a non-successful status code</exception>
    Task<TournamentMatchResponse> ScoreMatchAsync(int tournamentMatchId, ScoreMatchMessage scoreMatchMessage);
}
