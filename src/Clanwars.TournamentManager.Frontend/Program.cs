using System.Globalization;
using Clanwars.TournamentManager.Frontend;
using Clanwars.TournamentManager.Frontend.Helpers;
using Clanwars.TournamentManager.Frontend.Options;
using Clanwars.TournamentManager.Frontend.Services;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MudBlazor;
using MudBlazor.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddOidcAuthentication(options =>
{
    options.ProviderOptions.DefaultScopes.Add("email");
    options.ProviderOptions.DefaultScopes.Add("offline_access");
    // Configure your authentication provider options here.
    // For more information, see https://aka.ms/blazor-standalone-auth
    builder.Configuration.Bind("OpenIdConnect", options.ProviderOptions);
    options.UserOptions.RoleClaim = "roles";
}).AddAccountClaimsPrincipalFactory<ArrayClaimsPrincipalFactory<RemoteUserAccount>>();
builder.Services.AddScoped<CustomAuthorizationMessageHandler>();
var backendOptionsSection = builder.Configuration.GetSection(BackendOptions.OptionsKey);
builder.Services.Configure<BackendOptions>(backendOptionsSection);

builder.Services.AddHttpClient("TournamentApi", client => { client.BaseAddress = new Uri(backendOptionsSection.Get<BackendOptions>().BaseAddress.TrimEnd('/')); })
    .AddHttpMessageHandler<CustomAuthorizationMessageHandler>();

builder.Services.AddScoped(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("TournamentApi"));

builder.Services.AddScoped<IEventsService, EventsService>();
builder.Services.AddScoped<IClansService, ClansService>();
builder.Services.AddScoped<IUsersService, UsersService>();
builder.Services.AddScoped<IUserStoreService, UserStoreService>();
builder.Services.AddScoped<INewsService, NewsService>();
builder.Services.AddScoped<ICurrentUserService, CurrentUserService>();
builder.Services.AddScoped<ITournamentsService, TournamentsService>();
builder.Services.AddScoped<ITournamentTeamsService, TournamentTeamsService>();
builder.Services.AddScoped<IRoomsService, RoomsService>();
builder.Services.AddScoped<IImagesService, ImagesService>();
builder.Services.AddScoped<INotificationsService, NotificationsService>();
builder.Services.AddScoped<IPaymentSystemService, PaymentSystemService>();
builder.Services.AddScoped<IShoutsService, ShoutsService>();
builder.Services.AddScoped<IFeedbackService, FeedbackService>();
builder.Services.AddScoped<IBeamerPipelineService, BeamerPipelineService>();

builder.Services.AddMudServices();
builder.Services.AddMudMarkdownServices();

builder.Services.AddLocalization();
// TODO: add culture configuration to settings
CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("de-DE");

await builder.Build().RunAsync();
