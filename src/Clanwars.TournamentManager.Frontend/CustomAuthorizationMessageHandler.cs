using Clanwars.TournamentManager.Frontend.Options;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.Extensions.Options;

namespace Clanwars.TournamentManager.Frontend;

public class CustomAuthorizationMessageHandler : AuthorizationMessageHandler
{
    public CustomAuthorizationMessageHandler(IAccessTokenProvider provider, NavigationManager navigation, IOptions<BackendOptions> backendOptions) : base(provider, navigation)
    {
        ConfigureHandler(authorizedUrls: new[] {backendOptions.Value.BaseAddress.TrimEnd('/')});
    }
}
