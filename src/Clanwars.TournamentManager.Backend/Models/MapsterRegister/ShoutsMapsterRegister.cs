using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Shouts;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class ShoutsMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<PagedResult<Shout>, PagedResult<ShoutResponse>>();

        config.NewConfig<Shout, ShoutResponse>();

        config.NewConfig<CreateShoutMessage, Shout>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.User)
            .Ignore(dst => dst.UserId)
            .Map(dst => dst.CreatedAt, src => DateTime.Now);

        config.NewConfig<UpdateShoutRequest, Shout>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.User)
            .Ignore(dst => dst.UserId)
            .Map(dst => dst.CreatedAt, src => DateTime.Now);
    }
}
