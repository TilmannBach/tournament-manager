using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Clans;
using HashidsNet;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class ClansMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<CreateClanMessage, Clan>()
            .Ignore(dst => dst.Members)
            .Ignore(dst => dst.Image!)
            .Ignore(dst => dst.ImageId!)
            .Ignore(dst => dst.CreatedAt)
            .Ignore(dst => dst.UpdatedAt)
            .Ignore(dst => dst.Id);

        config.NewConfig<PagedResult<Clan>, PagedResult<ClanResponse>>();

        config.NewConfig<Clan, ClanResponse>()
            .Map(dst => dst.ImageUrl, src => src.Image == null ? null : $"api/v1/images/{MapContext.Current.GetService<IHashids>().Encode(src.ImageId!.Value)}")
            .Map(dst => dst.Members, src => src.Members);

        config.NewConfig<ClanUpdateRequest, Clan>()
            .Map(dst => dst.Id, src => src.ClanId)
            .Ignore(dst => dst.ImageId!)
            .Ignore(dst => dst.Image!)
            .Ignore(dst => dst.CreatedAt)
            .Ignore(dst => dst.UpdatedAt)
            .Ignore(dst => dst.Members);
    }
}
