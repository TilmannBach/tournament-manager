using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using HashidsNet;
using Mapster;

namespace Clanwars.TournamentManager.Backend.Models.MapsterRegister;

public class TournamentTeamMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<PagedResult<TournamentTeam>, PagedResult<TournamentTeamResponse>>();

        config.NewConfig<TournamentTeam, TournamentTeamResponse>()
            .Map(dst => dst.ImageUrl, src => src.Image == null ? null : $"api/v1/images/{MapContext.Current.GetService<IHashids>().Encode(src.ImageId!.Value)}")
            .MaxDepth(2);

        config.NewConfig<TournamentTeamMember, TournamentTeamMemberResponse>()
            .MaxDepth(2);

        config.NewConfig<CreateTournamentTeamMessage, TournamentTeam>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.Tournament)
            .Ignore(dst => dst.Members)
            .Ignore(dst => dst.Image!)
            .Ignore(dst => dst.ImageId!)
            .Ignore(dst => dst.Sealed);

        config.NewConfig<UpdateTournamentTeamRequest, TournamentTeam>()
            .Map(dst => dst.Id, src => src.TournamentTeamId)
            .Ignore(dst => dst.Members)
            .Ignore(dst => dst.Sealed)
            .Ignore(dst => dst.Tournament)
            .Ignore(dst => dst.ImageId!)
            .Ignore(dst => dst.Image!);
    }
}
