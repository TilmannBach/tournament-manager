using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.TournamentManager.Backend.ModelBinder;
using Clanwars.TournamentManager.Lib.Messages.Shouts;

namespace Clanwars.TournamentManager.Backend.Models;

public record UpdateShoutRequest : UpdateShoutMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public int ShoutId { get; set; }
}
