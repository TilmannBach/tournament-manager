using Clanwars.TournamentManager.Backend.Repositories;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations.Room;

public class UpdateRoomValidator : AbstractValidator<UpdateRoomRequest>
{
    private readonly IEventRepository _eventRepository;
    private readonly IRoomRepository _roomRepository;

    public UpdateRoomValidator(
        IEventRepository eventRepository,
        IRoomRepository roomRepository
    )
    {
        _eventRepository = eventRepository;
        _roomRepository = roomRepository;

        RuleFor(x => x.EventId)
            .Must(IsValidEventId())
            .WithMessage("The event id is unknown");

        RuleFor(x => x.SizeX)
            .Must(BeEqualOrBigger(nameof(Entities.Rooms.Room.SizeX)))
            .WithMessage("The room can not be shrunk in X coordinate");

        RuleFor(x => x.SizeY)
            .Must(BeEqualOrBigger(nameof(Entities.Rooms.Room.SizeY)))
            .WithMessage("The room can not be shrunk in Y coordinate");
    }

    private Func<int, bool> IsValidEventId()
    {
        return eventId =>
        {
            var @event = _eventRepository
                .GetEvents()
                .SingleOrDefault(e => e.Id == eventId);

            return @event is not null;
        };
    }

    private Func<UpdateRoomRequest, int, bool> BeEqualOrBigger(string coordinatePropertyName)
    {
        return (updateRoomRequest, size) =>
        {
            var room = _roomRepository
                .GetRooms()
                .SingleOrDefault(r => r.Id == updateRoomRequest.RoomId);

            if (room is null)
            {
                return true;
            }

            var t = typeof(Entities.Rooms.Room);
            var member = t.GetProperty(coordinatePropertyName)!;
            var coordinateValue = (int) member.GetValue(room)!;

            return size >= coordinateValue;
        };
    }
}
