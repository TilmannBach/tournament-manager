using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Enums.Rooms;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations.Room;

public class UpdateGridValidator : AbstractValidator<UpdateGridRequest>
{
    private readonly IRoomRepository _roomRepository;

    public UpdateGridValidator(
        IRoomRepository roomRepository
    )
    {
        _roomRepository = roomRepository;

        RuleFor(x => x.Grid.X)
            .Must(BeInsideRoomCoordinateX())
            .WithMessage("The X coordinate is not inside the Room");
        RuleFor(x => x.Grid.Y)
            .Must(BeInsideRoomCoordinateY())
            .WithMessage("The Y coordinate is not inside the Room");

        RuleFor(x => x.Rotation)
            .IsInEnum();
        RuleFor(x => x.Rotation.ToString())
            .IsEnumName(typeof(GridSquareRotation), caseSensitive: false);

        RuleFor(x => x.Type)
            .IsInEnum();
        RuleFor(x => x.Type.ToString())
            .IsEnumName(typeof(GridSquareType), caseSensitive: false);
    }

    private Func<UpdateGridRequest, int, bool> BeInsideRoomCoordinateX()
    {
        return (updateGridRequest, coordinateX) =>
        {
            var room = _roomRepository
                .GetRooms()
                .SingleOrDefault(r => r.Id == updateGridRequest.RoomId);

            return room is null || coordinateX <= room.SizeX;
        };
    }

    private Func<UpdateGridRequest, int, bool> BeInsideRoomCoordinateY()
    {
        return (updateGridRequest, coordinateY) =>
        {
            var room = _roomRepository
                .GetRooms()
                .SingleOrDefault(r => r.Id == updateGridRequest.RoomId);

            return room is null || coordinateY <= room.SizeY;
        };
    }
}
