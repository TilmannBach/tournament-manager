using Clanwars.TournamentManager.Backend.Repositories;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class UpdateEventValidator : AbstractValidator<UpdateEventRequest>
{
    private readonly IEventRepository _eventRepository;

    public UpdateEventValidator(
        IEventRepository eventRepository
    )
    {
        _eventRepository = eventRepository;

        RuleFor(e => e.EventId)
            .Must(BeValidEventId())
            .WithMessage("Event id does not exist.");

        RuleFor(x => x.Name)
            .Must(NotSameNameLikeOtherEvents())
            .WithMessage("The same event name cannot be used twice");
    }

    private Func<int, bool> BeValidEventId()
    {
        return eventId =>
        {
            var @event = _eventRepository
                .GetEvents()
                .SingleOrDefault(e => e.Id == eventId);

            return @event is not null;
        };
    }

    private Func<UpdateEventRequest, string, bool> NotSameNameLikeOtherEvents()
    {
        return (cem, eventName) =>
        {
            var elementWithSameName = _eventRepository
                .GetEvents()
                .Where(ev => ev.Name == eventName)
                .FirstOrDefault(ev => ev.Id != cem.EventId);

            return elementWithSameName is null;
        };
    }
}
