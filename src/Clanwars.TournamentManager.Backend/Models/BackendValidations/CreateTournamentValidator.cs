using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class CreateTournamentValidator : AbstractValidator<CreateTournamentMessage>
{
    private readonly IEventRepository _eventRepository;

    public CreateTournamentValidator(
        IEventRepository eventRepository
    )
    {
        _eventRepository = eventRepository;

        RuleFor(x => x.EventId)
            .Must(IsValidEventId())
            .WithMessage("The event id is unknown");
    }

    private Func<int, bool> IsValidEventId()
    {
        return eventId =>
        {
            var @event = _eventRepository.GetEvents()
                .SingleOrDefault(e => e.Id == eventId);

            return @event is not null;
        };
    }
}
