using Clanwars.TournamentManager.Backend.Repositories;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class LeaveEventValidator : AbstractValidator<LeaveEventRequest>
{
    private readonly IEventRepository _eventRepository;

    public LeaveEventValidator(
        IEventRepository eventRepository
    )
    {
        _eventRepository = eventRepository;

        RuleFor(x => x.UserId)
            .Must(IsParticipant())
            .WithMessage("The user is not a participant of this event");
    }

    private Func<LeaveEventRequest, int, bool> IsParticipant()
    {
        return (jem, userId) =>
        {
            var elementWithUser = _eventRepository
                .GetEvents()
                .Where(ev => ev.Id == jem.EventId)
                .FirstOrDefault(ev => ev.Participants.Any(p => p.UserId == userId));    

            return elementWithUser is not null;
        };
    }
}
