using Clanwars.TournamentManager.Backend.Repositories;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class JoinEventValidator : AbstractValidator<JoinEventRequest>
{
    private readonly IEventRepository _eventRepository;

    public JoinEventValidator(
        IEventRepository eventRepository
    )
    {
        _eventRepository = eventRepository;

        RuleFor(x => x.UserId)
            .Must(NotAlreadyParticipant())
            .WithMessage("The user already participates on this event");
    }

    private Func<JoinEventRequest, int, bool> NotAlreadyParticipant()
    {
        return (jem, userId) =>
        {
            var elementWithUser = _eventRepository
                .GetEvents()
                .Where(ev => ev.Id == jem.EventId)
                .FirstOrDefault(ev => ev.Participants.Any(p => p.UserId == userId));

            return elementWithUser is null;
        };
    }
}
