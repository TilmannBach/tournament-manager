using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Backend.Repositories;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations.User;

public class IsValidUserIdAttribute : ValidationAttribute
{
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value == null)
        {
            return ValidationResult.Success;
        }

        var userId = (int) value;
        var repository = validationContext.GetRequiredService<IUserRepository>();

        var element = repository
            .GetUsers()
            .SingleOrDefault(u => u.Id == userId);
        return element is null ? new ValidationResult(GetErrorMessage(userId), new[] {nameof(userId)}) : ValidationResult.Success;
    }

    private static string GetErrorMessage(int id)
    {
        return $"A user with the id {id} does not exist.";
    }
}
