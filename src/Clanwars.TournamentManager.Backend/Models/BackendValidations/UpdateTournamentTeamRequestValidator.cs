using Clanwars.TournamentManager.Backend.Repositories;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class UpdateTournamentTeamRequestValidator : AbstractValidator<UpdateTournamentTeamRequest>
{
    private readonly ITournamentTeamRepository _tournamentTeamRepository;
    private readonly ITournamentRepository _tournamentRepository;

    public UpdateTournamentTeamRequestValidator(
        ITournamentTeamRepository tournamentTeamRepository,
        ITournamentRepository tournamentRepository
    )
    {
        _tournamentTeamRepository = tournamentTeamRepository;
        _tournamentRepository = tournamentRepository;

        RuleFor(x => x.TournamentId)
            .Must(BeValidTournamentId())
            .WithMessage("tournamentId is not valid");

        RuleFor(x => x.Name)
            .Must(HaveUniqueTournamentTeamName())
            .WithMessage("tournament team name is already in use");
    }

    private Func<int, bool> BeValidTournamentId()
    {
        return tournamentId =>
        {
            var tournament = _tournamentRepository.GetTournamentByIdAsync(tournamentId).Result;

            return tournament is not null;
        };
    }

    private Func<UpdateTournamentTeamRequest, string, bool> HaveUniqueTournamentTeamName()
    {
        return (updateTournamentTeamRequest, teamName) =>
        {
            var result = _tournamentTeamRepository.GetTournamentTeams()
                .Where(tt => tt.Name == teamName)
                .SingleOrDefault(tt => tt.Id != updateTournamentTeamRequest.TournamentTeamId);

            return result is null;
        };
    }
}
