using Clanwars.TournamentManager.Backend.Repositories;
using FluentValidation;

namespace Clanwars.TournamentManager.Backend.Models.BackendValidations;

public class UpdateClanValidator : AbstractValidator<ClanUpdateRequest>
{
    private readonly IClanRepository _clanRepository;

    public UpdateClanValidator(
        IClanRepository clanRepository
    )
    {
        _clanRepository = clanRepository;

        RuleFor(x => x.Name)
            .Must(NotSameNameLikeOtherClans())
            .WithMessage("The same clan name cannot be used twice");
        RuleFor(x => x.ClanTag)
            .Must(NotSameClanTagLikeOtherClans())
            .WithMessage("The same clan tag cannot be used twice");
    }

    private Func<ClanUpdateRequest, string, bool> NotSameNameLikeOtherClans()
    {
        return (cur, name) =>
        {
            var elementWithSameName = _clanRepository
                .GetClans()
                .Where(clan => clan.Name == name)
                .FirstOrDefault(clan => clan.Id != cur.ClanId);
            return elementWithSameName is null;
        };
    }

    private Func<ClanUpdateRequest, string, bool> NotSameClanTagLikeOtherClans()
    {
        return (cur, tagName) =>
        {
            var elementWithSameName = _clanRepository
                .GetClans()
                .Where(clan => clan.ClanTag == tagName)
                .FirstOrDefault(clan => clan.Id != cur.ClanId);
            return elementWithSameName is null;
        };
    }
}
