using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.TournamentManager.Backend.ModelBinder;
using Clanwars.TournamentManager.Lib.Messages.Tournaments;

namespace Clanwars.TournamentManager.Backend.Models;

public record UpdateTournamentTeamRequest : UpdateTournamentTeamMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public int TournamentTeamId { get; set; }
}
