using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.TournamentManager.Backend.ModelBinder;
using Clanwars.TournamentManager.Lib.Messages.Rooms;

namespace Clanwars.TournamentManager.Backend.Models;

public record UpdateRoomRequest : UpdateRoomMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public int RoomId { get; set; }
}
