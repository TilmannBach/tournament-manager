namespace Clanwars.TournamentManager.Backend.Helpers.Exceptions;

public class ModuleDisabledException : Exception;
