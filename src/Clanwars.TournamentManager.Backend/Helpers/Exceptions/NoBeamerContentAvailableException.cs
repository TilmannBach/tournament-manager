namespace Clanwars.TournamentManager.Backend.Helpers.Exceptions;

public class NoBeamerContentAvailableException(string? message) : Exception(message);
