using Clanwars.TournamentManager.Backend.Entities;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Helpers.Sieve;

public class SieveCustomFilterMethods : ISieveCustomFilterMethods
{
#pragma warning disable S2325
    public IQueryable<User> ParticipatingEventId(IQueryable<User> source, string op, string[] values)
#pragma warning restore S2325
    {
        if (op != "==")
        {
            throw new ArgumentException("Operator is not allowed here", nameof(op));
        }

        if (values.Length != 1)
        {
            throw new ArgumentException("Only one value is not allowed here", nameof(values));
        }

        if (!int.TryParse(values[0], out var value))
        {
            throw new ArgumentException("Value is not a number", nameof(values));
        }

        var result = source.Where(u => u.Events.Any(ev => ev.UserId == value));

        return result;
    }
}
