namespace Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
public class AdditionalFilterableEntitiesAttribute : Attribute
{
    public List<string> AdditionFilterableEntities { get; init; } = [];

    public AdditionalFilterableEntitiesAttribute(params string[] filterableEntities)
    {
        AdditionFilterableEntities.AddRange(filterableEntities);
    }
}
