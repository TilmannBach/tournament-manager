namespace Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
public class AdditionalSortableEntitiesAttribute : Attribute
{
    public List<string> AdditionSortableEntities { get; init; } = [];

    public AdditionalSortableEntitiesAttribute(params string[] sortableEntities)
    {
        AdditionSortableEntities.AddRange(sortableEntities);
    }
}
