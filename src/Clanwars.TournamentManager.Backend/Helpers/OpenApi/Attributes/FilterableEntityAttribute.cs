namespace Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;

[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
public class FilterableEntityAttribute : Attribute
{
    public FilterableEntityAttribute(Type entityType)
    {
        EntityType = entityType;
    }

    public Type EntityType { get; }
}