using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Repositories;

public interface ITournamentRepository
{
    IQueryable<Tournament> GetTournaments();
    Task<PagedResult<Tournament>> GetTournamentsAsync(SieveModel sieveModel);
    Task<Tournament?> GetTournamentByIdAsync(int tournamentId);
    Task AddTournamentAsync(Tournament tournament);
    Task UpdateTournamentAsync(Tournament tournament);
    Task DeleteTournamentAsync(Tournament tournament);
}
