using Clanwars.TournamentManager.Backend.Entities.Rooms;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Repositories;

public interface IRoomRepository
{
    IQueryable<Room> GetRooms();
    Task<PagedResult<Room>> GetRoomsAsync(SieveModel sieveModel);
    Task<Room?> GetRoomByIdAsync(int roomId);
    Task AddRoomAsync(Room room);
    Task DeleteRoomAsync(Room room);
    Task UpdateRoomAsync(Room room);

    IQueryable<GridSquare> GetGridSquares();
    Task<PagedResult<GridSquare>> GetGridSquaresAsync(SieveModel sieveModel);
    Task<GridSquare?> GetGridSquareByIdAsync(int gridSquareId);
    Task<GridSquare?> GetGridSquareByCoordinatesAsync(int roomId, int x, int y);
    Task AddGridSquareAsync(GridSquare gridSquare);
    Task DeleteGridSquareAsync(GridSquare gridSquare);
    Task UpdateGridSquareAsync(GridSquare gridSquare);

    Task PlaceUserAsync(GridSquare gridSquare);
}
