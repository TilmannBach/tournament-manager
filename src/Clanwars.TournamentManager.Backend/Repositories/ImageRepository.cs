using System.Security.Cryptography;
using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

internal class ImageRepository : IImageRepository
{
    private readonly TournamentManagerDbContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;


    public ImageRepository(TournamentManagerDbContext context, ISieveProcessor sieveProcessor, IOptions<SieveOptions> sieveOptions)
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    private IQueryable<ImageWithContent> GetImagesWithContent()
    {
        var result = _context.ImagesWithContent;
        return result;
    }
    
    private IQueryable<Image> GetImages()
    {
        var result = _context.Images;
        return result;
    }

    public async Task<PagedResult<Image>> GetImagesAsync(SieveModel sieveModel)
    {
        var result = GetImages()
            .AsNoTrackingWithIdentityResolution();
        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task<ImageWithContent?> GetImageWithContentByIdAsync(int imageId)
    {
        var queryable = GetImagesWithContent()
            .Where(img => img.Id == imageId);
        return await queryable.SingleOrDefaultAsync();
    }

    public async Task<Image?> GetImageByIdAsync(int imageId)
    {
        var queryable = GetImages()
            .Where(img => img.Id == imageId);
        return await queryable.SingleOrDefaultAsync();
    }

    public Task UpdateImageAsync(Image image)
    {
        return _context.SaveChangesAsync();
    }

    public async Task<ImageWithContent?> GetImageWithContentByHashAndImageTypeAsync(byte[] hash, ImageType imageType)
    {
        var queryable = GetImagesWithContent()
            .Where(img => img.Hash == hash);
        return await queryable.SingleOrDefaultAsync(img => img.Type == imageType);
    }

    public async Task<ImageWithContent> AddImageWithContentAsync(ImageType type, string content, byte[] hash)
    {
        var image = new Image
        {
            Type = type,
            Hash = hash,
            ImageWithContent = new ImageWithContent
            {
                Content = content,
                Hash = hash,
                Type = type
            }
        };
        await _context.AddAsync(image);
        await _context.SaveChangesAsync();
        return image.ImageWithContent;
    }

    public async Task RemoveImageAsync(int imageId)
    {
        var image = await _context.Images.FindAsync(imageId);
        if (image == null) return;

        _context.Images.Remove(image);
        await _context.SaveChangesAsync();
    }

    public async Task<Image?> GetRandomImageAsync(ImageType imageType)
    {
        var imageCount = await _context.Images.CountAsync(img => img.Type == imageType && img.ShowOnStream == true);
        if (imageCount == 0)
        {
            return null;
        }

        var randomIndex = new Random().Next(imageCount);

        var randomEntry = await _context.Images.Where(i => i.Type == imageType && i.ShowOnStream == true).Skip(randomIndex).FirstAsync();
        return randomEntry;
    }
}
