using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

public class FeedbackRepository : IFeedbackRepository
{
    private readonly TournamentManagerDbContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;

    public FeedbackRepository(
        TournamentManagerDbContext context,
        ISieveProcessor sieveProcessor,
        IOptions<SieveOptions> sieveOptions
    )
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<Feedback> GetFeedbacks()
    {
        var result = _context
            .Feedbacks
            .Include(f => f.User);

        return result;
    }

    public async Task<PagedResult<Feedback>> GetFeedbacksAsync(SieveModel sieveModel)
    {
        var result = GetFeedbacks()
            .AsNoTrackingWithIdentityResolution();
        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task AddFeedbackAsync(Feedback feedback)
    {
        await _context.Feedbacks.AddAsync(feedback);
        await _context.SaveChangesAsync();
    }

    public Task<Feedback> GetFeedbackByIdAsync(int id)
    {
        return GetFeedbacks().Where(f => f.Id == id).SingleAsync();
    }

    public async Task UpdateFeedbackAsync(Feedback feedback)
    {
        _context.Feedbacks.Attach(feedback);
        await _context.SaveChangesAsync();
    }
}
