using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities.Rooms;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

public class RoomRepository : IRoomRepository
{
    private readonly TournamentManagerDbContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;


    public RoomRepository(TournamentManagerDbContext context, ISieveProcessor sieveProcessor, IOptions<SieveOptions> sieveOptions)
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<Room> GetRooms()
    {
        var result = _context
            .Rooms
            .Include(r => r.GridSquares).ThenInclude(gs => gs.User).ThenInclude(u => u!.Image)
            .Include(r => r.Event).ThenInclude(e => e.Participants)
            .Include(r => r.Event).ThenInclude(e => e.Tournaments);
        return result;
    }

    public Task<PagedResult<Room>> GetRoomsAsync(SieveModel sieveModel)
    {
        var result = GetRooms()
            .AsNoTrackingWithIdentityResolution();
        return _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public Task<Room?> GetRoomByIdAsync(int roomId)
    {
        var result = GetRooms()
            .Where(c => c.Id == roomId);
        return result.SingleOrDefaultAsync();
    }

    public async Task AddRoomAsync(Room room)
    {
        await _context.Rooms.AddAsync(room);
        await _context.SaveChangesAsync();
    }

    public async Task DeleteRoomAsync(Room room)
    {
        _context.Rooms.Remove(room);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateRoomAsync(Room room)
    {
        await _context.SaveChangesAsync();
    }

    public IQueryable<GridSquare> GetGridSquares()
    {
        var result = _context
            .GridSquares
            .Include(s => s.Room).ThenInclude(r => r.Event)
#nullable disable
            .Include(s => s.User).ThenInclude(u => u.Clan);
#nullable restore
        return result;
    }

    public async Task<PagedResult<GridSquare>> GetGridSquaresAsync(SieveModel sieveModel)
    {
        var result = GetGridSquares()
            .AsNoTrackingWithIdentityResolution();
        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task<GridSquare?> GetGridSquareByIdAsync(int gridSquareId)
    {
        var result = GetGridSquares()
            .Where(gs => gs.Id == gridSquareId);
        return await result.SingleOrDefaultAsync();
    }

    public async Task<GridSquare?> GetGridSquareByCoordinatesAsync(int roomId, int x, int y)
    {
        var result = GetGridSquares()
            .Where(gs => gs.RoomId == roomId)
            .Where(gs => gs.X == x)
            .Where(gs => gs.Y == y);
        return await result.SingleOrDefaultAsync();
    }

    public async Task AddGridSquareAsync(GridSquare gridSquare)
    {
        await _context.GridSquares.AddAsync(gridSquare);
        await _context.SaveChangesAsync();
    }

    public async Task DeleteGridSquareAsync(GridSquare gridSquare)
    {
        _context.GridSquares.Remove(gridSquare);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateGridSquareAsync(GridSquare gridSquare)
    {
        await _context.SaveChangesAsync();
    }

    public async Task PlaceUserAsync(GridSquare gridSquare)
    {
        await _context.SaveChangesAsync();
    }
}
