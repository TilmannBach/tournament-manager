using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

public class ShoutRepository(
    TournamentManagerDbContext context,
    ISieveProcessor sieveProcessor,
    IOptions<SieveOptions> sieveOptions,
    IEventService eventService)
    : IShoutRepository
{
    private readonly SieveOptions _sieveOptions = sieveOptions.Value;

    public IQueryable<Shout> GetShouts()
    {
        var result = context
                .Shouts
                .Include(s => s.User).ThenInclude(u => u.Clan)
                .Include(s => s.User).ThenInclude(u => u.Image)
            ;
        return result;
    }

    public Task<Shout?> GetShoutByIdAsync(int shoutId)
    {
        var result = GetShouts()
            .Where(s => s.Id == shoutId);
        return result.SingleOrDefaultAsync();
    }

    public async Task<PagedResult<Shout>> GetShoutsAsync(SieveModel sieveModel)
    {
        var result = GetShouts()
            .AsNoTrackingWithIdentityResolution();
        return await sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task AddShoutAsync(Shout shout)
    {
        await context.Shouts.AddAsync(shout);
        await context.SaveChangesAsync();
    }

    public async Task DeleteShoutAsync(Shout shout)
    {
        context.Shouts.Remove(shout);
        await context.SaveChangesAsync();
    }

    public async Task UpdateShoutAsync(Shout shout)
    {
        await context.SaveChangesAsync();
    }

    // TODO actually that could return null if count = 0
    public async Task<Shout?> GetRandomAsync()
    {
        var currentEvent = await eventService.GetRunningEventAsync();
        if (currentEvent is null)
        {
            return null;
        }

        var shoutCount = await GetShouts()
            .Where(n => n.CreatedAt.Date > currentEvent.StartDate.Date - TimeSpan.FromDays(7))
            .CountAsync();
        if (shoutCount == 0)
        {
            return null;
        }

        var randomIndex = new Random().Next(shoutCount);

        var randomEntry = await GetShouts()
            .Where(n => n.CreatedAt.Date > currentEvent.StartDate.Date - TimeSpan.FromDays(7))
            .Skip(randomIndex)
            .FirstAsync();

        return randomEntry;
    }
}
