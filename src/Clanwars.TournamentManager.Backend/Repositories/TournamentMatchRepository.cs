using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.TournamentManager.Backend.Repositories;

public class TournamentMatchRepository : ITournamentMatchRepository
{
    private readonly TournamentManagerDbContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;


    public TournamentMatchRepository(TournamentManagerDbContext context, ISieveProcessor sieveProcessor, IOptions<SieveOptions> sieveOptions)
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<TournamentMatch> GetTournamentMatches()
    {
        var result = _context
            .TournamentMatches
            .Include(tm => tm.TournamentRound).ThenInclude(tr => tr.Tournament)
            .Include(tm => tm.Scores);
        return result;
    }

    public IQueryable<TournamentMatchScore> GetTournamentMatchScores()
    {
        return _context
            .TournamentMatchScores
            .Include(tms => tms.TournamentMatch).ThenInclude(tm => tm.TournamentRound).ThenInclude(tr => tr.Tournament)
            .Include(tms => tms.TournamentTeam);
    }

    public Task<PagedResult<TournamentMatch>> GetTournamentMatchesAsync(SieveModel sieveModel)
    {
        var result = GetTournamentMatches()
            .AsNoTrackingWithIdentityResolution();
        return _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public Task<TournamentMatch?> GetTournamentMatchByIdAsync(int tournamentMatchId)
    {
        var result = GetTournamentMatches()
            .Where(c => c.Id == tournamentMatchId);
        return result.SingleOrDefaultAsync();
    }

    public async Task ScoreMatchAsync(TournamentMatch score)
    {
        var matchTarget = await GetTournamentMatches()
            .Include(tm => tm.TournamentRound).ThenInclude(tr => tr.Tournament)
            .SingleAsync(tm => tm.Id == score.Id);

        foreach (var matchScore in matchTarget.Scores)
        {
            matchScore.Score = score.Scores.SingleOrDefault(s => s.TournamentTeamId == matchScore.TournamentTeamId)?.Score ?? 0;
        }

        var subsequentMatchScores = _context
            .TournamentMatchScores
            .Include(tms => tms.TournamentMatch).ThenInclude(tm => tm.TournamentRound)
            .Where(tms =>
                tms.TournamentMatch.TournamentRound.TournamentId == matchTarget.TournamentRound.TournamentId &&
                tms.PreviousMatchIndex == matchTarget.MatchIndex
            );

        foreach (var subsequentMatchScore in subsequentMatchScores)
        {
            subsequentMatchScore.TournamentTeamId = subsequentMatchScore.PreviousMatchRank switch
            {
                MatchRank.Loser => matchTarget.Scores.OrderByDescending(s => s.Score).Last().TournamentTeamId,
                MatchRank.Winner => matchTarget.Scores.OrderByDescending(s => s.Score).First().TournamentTeamId,
                _ => subsequentMatchScore.TournamentTeamId
            };
        }

        await _context.SaveChangesAsync();

        var nextMatches = await _context
            .TournamentMatches
            .Include(tm => tm.Scores).ThenInclude(tms => tms.TournamentTeam)
            .Include(tm => tm.TournamentRound).ThenInclude(tr => tr.Tournament)
            .Where(tm => subsequentMatchScores.Any(ssm => ssm.TournamentMatchId == tm.Id)).ToListAsync();

        foreach (var nextMatch in nextMatches.Where(nextMatch => nextMatch.Scores.Any(nms => nextMatch.Scores.All(tms => tms.TournamentTeam != null) && nms.TournamentTeam.IsByeTeam)))
        {
            foreach (var nextByeMatchScore in nextMatch.Scores)
            {
                nextByeMatchScore.Score = nextByeMatchScore.TournamentTeam.IsByeTeam ? 0 : 1;
            }

            if (nextMatch.Scores.All(tms => tms.Score == 0))
            {
                // Only Bye vs Bye? -> randomly select first one :)
                nextMatch.Scores[0].Score = 1;
            }

            await ScoreMatchAsync(nextMatch);
        }
    }
}
