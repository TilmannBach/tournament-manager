using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Repositories;

public interface IShoutRepository
{
    IQueryable<Shout> GetShouts();
    Task<Shout?> GetShoutByIdAsync(int shoutId);
    Task<PagedResult<Shout>> GetShoutsAsync(SieveModel sieveModel);
    Task AddShoutAsync(Shout shout);
    Task DeleteShoutAsync(Shout shout);
    Task UpdateShoutAsync(Shout shout);
    Task<Shout?> GetRandomAsync();
}
