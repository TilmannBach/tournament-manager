using System.Reflection;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Clanwars.TournamentManager.Backend.ModelBinder;

public class MapToModelModelBinder : IModelBinder
{
    private readonly IModelBinder _bodyBinder;

    public MapToModelModelBinder(IModelBinder bodyBinder)
    {
        _bodyBinder = bodyBinder;
    }

    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        ArgumentNullException.ThrowIfNull(bindingContext);

        await _bodyBinder.BindModelAsync(bindingContext);

        var modelTypeProperties = bindingContext.ModelType.GetProperties();
        var paramsToMap = modelTypeProperties
            .Where(pi => pi.GetCustomAttribute<MapFromRouteAttribute>() != null);
        var model = bindingContext.Result.Model;
        if (model == null) return;

        foreach (var paramToMap in paramsToMap)
        {
            var value = bindingContext.ValueProvider.GetValue(paramToMap.Name);
            paramToMap.SetValue(model, Convert.ChangeType(value.FirstValue, paramToMap.PropertyType));
        }
    }
}
