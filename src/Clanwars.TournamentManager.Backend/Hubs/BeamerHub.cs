using Clanwars.TournamentManager.Backend.Services.Pipeline;
using Clanwars.TournamentManager.Lib.Messages.Pipeline;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace Clanwars.TournamentManager.Backend.Hubs;

[AllowAnonymous]
public class BeamerHub(IBeamerPublisher beamerPublisher) : Hub<IBeamerHub>
{
    public override async Task OnConnectedAsync()
    {
        await Clients.Caller.AutoplayState(beamerPublisher.PublisherAutoMode);
        if (beamerPublisher.PublisherAutoMode)
        {
            switch (beamerPublisher.CurrentEntry.ModuleType)
            {
                case ModuleType.News:
                    await Clients.Caller.PublishNews((NewsBeamerHubMessage)beamerPublisher.CurrentEntry);
                    break;
                case ModuleType.Shouts:
                    await Clients.Caller.PublishShout((ShoutBeamerHubMessage)beamerPublisher.CurrentEntry);
                    break;
                case ModuleType.Tournaments:
                    await Clients.Caller.PublishTournaments((TournamentBeamerHubMessage)beamerPublisher.CurrentEntry);
                    break;
                case ModuleType.Matches:
                    await Clients.Caller.PublishUpcomingMatches((UpcomingMatchesBeamerHubMessage)beamerPublisher.CurrentEntry);
                    break;
                case ModuleType.MatchResults:
                    await Clients.Caller.PublishFinishedMatches((FinishedMatchesBeamerHubMessage)beamerPublisher.CurrentEntry);
                    break;
                case ModuleType.MatchResult:
                    await Clients.Caller.PublishTournamentMatchResult((TournamentMatchResultBeamerHubMessage)beamerPublisher.CurrentEntry);
                    break;
                case ModuleType.Images:
                    await Clients.Caller.PublishImage((ImageBeamerHubMessage)beamerPublisher.CurrentEntry);
                    break;
                case ModuleType.WelcomeMessage:
                    await Clients.Caller.PublishWelcome((WelcomeBeamerHubMessage)beamerPublisher.CurrentEntry);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(beamerPublisher), "Module type not supported");
            }
        }

        await base.OnConnectedAsync();
    }
}
