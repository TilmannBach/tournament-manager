﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clanwars.TournamentManager.Backend.Migrations
{
    public partial class AddImageUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Hash",
                table: "Images",
                type: "varbinary(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: new byte[0]);

            migrationBuilder.AddColumn<int>(
                name: "ImageId",
                table: "Clans",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Images_Type_Hash",
                table: "Images",
                columns: new[] { "Type", "Hash" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Clans_ImageId",
                table: "Clans",
                column: "ImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clans_Images_ImageId",
                table: "Clans",
                column: "ImageId",
                principalTable: "Images",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clans_Images_ImageId",
                table: "Clans");

            migrationBuilder.DropIndex(
                name: "IX_Images_Type_Hash",
                table: "Images");

            migrationBuilder.DropIndex(
                name: "IX_Clans_ImageId",
                table: "Clans");

            migrationBuilder.DropColumn(
                name: "Hash",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "Clans");
        }
    }
}
