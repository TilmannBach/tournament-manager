﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clanwars.TournamentManager.Backend.Migrations
{
    public partial class AddLpsIdColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LanpartyScreenId",
                table: "Tournaments",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LanpartyScreenMatchId",
                table: "TournamentMatches",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LanpartyScreenId",
                table: "News",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LanpartyScreenId",
                table: "Tournaments");

            migrationBuilder.DropColumn(
                name: "LanpartyScreenMatchId",
                table: "TournamentMatches");

            migrationBuilder.DropColumn(
                name: "LanpartyScreenId",
                table: "News");
        }
    }
}
