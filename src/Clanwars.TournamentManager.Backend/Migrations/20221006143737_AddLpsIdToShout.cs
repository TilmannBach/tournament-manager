﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clanwars.TournamentManager.Backend.Migrations
{
    public partial class AddLpsIdToShout : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LanpartyScreenId",
                table: "Shouts",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LanpartyScreenId",
                table: "Shouts");
        }
    }
}
