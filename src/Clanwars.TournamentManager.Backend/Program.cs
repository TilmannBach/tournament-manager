using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using System.Security.Claims;
using Clanwars.TournamentManager.Backend.Context;
using Clanwars.TournamentManager.Backend.Filters;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi;
using Clanwars.TournamentManager.Backend.Helpers.Sieve;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.ModelBinder;
using Clanwars.TournamentManager.Backend.Options;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Backend.Services.PaymentSystem;
using Clanwars.TournamentManager.Backend.Services.PaymentSystem.Client;
using Clanwars.TournamentManager.Backend.Services.PaymentSystem.OidcTokenAuthenticationClient;
using Clanwars.TournamentManager.Backend.Services.Pipeline;
using Clanwars.TournamentManager.Lib.Extensions;
using Clanwars.TournamentManager.UserStores.Interfaces;
using Clanwars.TournamentManager.UserStores.Keycloak.Services;
using FluentValidation.AspNetCore;
using HashidsNet;
using Mapster;
using MapsterMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Sieve.Models;
using Sieve.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services
    .AddAuthentication(options =>
    {
        options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(o =>
        {
            var openIdConnectOptions = builder.Configuration.GetSection(OpenIdConnectOptions.Option).Get<OpenIdConnectOptions>()!;
            o.Authority = openIdConnectOptions.Authority;
            o.RequireHttpsMetadata = openIdConnectOptions.RequireHttpsMetadata;
            o.SaveToken = openIdConnectOptions.SaveTokens;
            o.ClaimsIssuer = openIdConnectOptions.Authority;
            o.ForwardSignOut = openIdConnectOptions.SignedOutRedirectUri;
            o.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                ValidateIssuer = true,
                ValidateLifetime = true,
                ValidateAudience = false
            };
        }
    );

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Add("name", ClaimTypes.Name);

builder.Services.Configure<KeycloakOptions>(builder.Configuration.GetSection(KeycloakOptions.Option));


builder.Services.AddScoped<ISieveProcessor, ApplicationSieveProcessor>();
builder.Services.Configure<SieveOptions>(builder.Configuration.GetSection("Sieve"));
builder.Services.AddScoped<ISieveCustomSortMethods, SieveCustomSortMethods>();
builder.Services.AddScoped<ISieveCustomFilterMethods, SieveCustomFilterMethods>();

builder.Services.AddDbContext<TournamentManagerDbContext>(
    options =>
    {
        var dbConfig = builder.Configuration.GetSection(DatabaseOptions.Option).Get<DatabaseOptions>()!;
        options
            .UseMySql(
                dbConfig.GetConnectionString(),
                new MariaDbServerVersion(new Version(10, 11, 7)),
                sqlOptions => { sqlOptions.UseMicrosoftJson(); }
            );
    });


builder.Services.AddControllers(options =>
    {
        var bodyProvider = options.ModelBinderProviders.Single(provider => provider.GetType() == typeof(BodyModelBinderProvider)) as BodyModelBinderProvider;
        options.ModelBinderProviders.Insert(0, new MapToModelModelBinderProvider(bodyProvider!));

        options.Filters.Add(typeof(PagedResultResponseFilter));
    })
    // .AddMicrosoftIdentityUI() ???
    .AddJsonOptions(options => JsonSerializerExtensions.JsonSerializerOptions(options.JsonSerializerOptions));

builder.Services.AddFluentValidationAutoValidation().AddFluentValidationClientsideAdapters();

builder.Services.AddResponseCompression(opts =>
{
    opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
        ["application/octet-stream"]);
});
builder.Services.AddSignalR();
builder.Services.AddHealthChecks();

builder.Services.AddAuthorization(options =>
{
    // By default, all incoming requests will be authorized according to the default policy
    options.FallbackPolicy = options.DefaultPolicy;
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    var openIdConnectOptions = builder.Configuration.GetSection(OpenIdConnectOptions.Option).Get<OpenIdConnectOptions>()!;
    c.UseCommonFilters();
    c.AddSecurityDefinition("JwtToken", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.OpenIdConnect,
        OpenIdConnectUrl = new Uri(openIdConnectOptions.Authority + "/.well-known/openid-configuration"),
        Scheme = "bearer",
        In = ParameterLocation.Header,
        Description = "Clanwars Keycloak"
    });
    // Set the comments path for the Swagger JSON and UI.
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowAllCorsPolicy", corsPolicyBuilder =>
    {
        corsPolicyBuilder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
            .WithExposedHeaders("*");
    });
});

// Mapster configuration
var mapsterConfig = TypeAdapterConfig.GlobalSettings;
mapsterConfig.RequireExplicitMapping = true;
mapsterConfig.RequireDestinationMemberSource = true;
mapsterConfig.Scan(Assembly.GetExecutingAssembly());
mapsterConfig.Compile();

builder.Services.AddSingleton(mapsterConfig);
builder.Services.AddScoped<IMapper, ServiceMapper>();

builder.Services.AddSingleton<IHashids>(_ =>
{
    var options = builder.Configuration.GetRequiredSection(HashIdOptions.Option).Get<HashIdOptions>()!;
    return new Hashids(options.Salt, options.MinLength);
});

builder.Services.AddScoped<IEventRepository, EventRepository>();
builder.Services.AddScoped<IRoomRepository, RoomRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IClanRepository, ClanRepository>();
builder.Services.AddScoped<ITournamentRepository, TournamentRepository>();
builder.Services.AddScoped<ITournamentTeamRepository, TournamentTeamRepository>();
builder.Services.AddScoped<ITournamentMatchRepository, TournamentMatchRepository>();
builder.Services.AddScoped<IImageRepository, ImageRepository>();
builder.Services.AddScoped<INewsRepository, NewsRepository>();
builder.Services.AddScoped<IShoutRepository, ShoutRepository>();
builder.Services.AddScoped<IFeedbackRepository, FeedbackRepository>();
builder.Services.AddScoped<IPipelineRepository, PipelineRepository>();

builder.Services.AddScoped<IEventService, EventService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<ITournamentService, TournamentService>();
builder.Services.AddScoped<ITournamentMatchService, TournamentMatchService>();
builder.Services.AddScoped<ITournamentTeamService, TournamentTeamService>();
builder.Services.AddScoped<IImageService, ImageService>();
builder.Services.AddScoped<IClanService, ClanService>();
builder.Services.AddScoped<IRoomService, RoomService>();
builder.Services.AddScoped<INewsService, NewsService>();
builder.Services.AddScoped<IShoutService, ShoutService>();
builder.Services.AddScoped<IFeedbackService, FeedbackService>();
builder.Services.AddSingleton<IBeamerPublisher, BeamerPublisher>();
builder.Services.AddSingleton<IBeamerContentProvider, BeamerContentProvider>();

// register optional payment system services
var paymentSystemOptionsSection = builder.Configuration.GetSection(PaymentSystemOptions.Option);
builder.Services.Configure<PaymentSystemOptions>(paymentSystemOptionsSection);
var paymentSystemOptions = paymentSystemOptionsSection.Get<PaymentSystemOptions>()!;
if (paymentSystemOptions.Enabled)
{
    if (paymentSystemOptions.Api is null || paymentSystemOptions.OidcOptions?.ClientId is null || paymentSystemOptions.OidcOptions?.ClientSecret is null ||
        paymentSystemOptions.OidcOptions?.IdpUrl is null)
    {
        throw new ArgumentException("Payment System is enabled in appsettings but not configured properly!");
    }

    builder.Services.AddHttpClient<IPaymentSystemClient, PaymentSystemClient>((httpClient, provider) =>
    {
        httpClient.BaseAddress = paymentSystemOptions.Api;

        var logger = provider.GetRequiredService<ILogger<PaymentSystemClient>>();
        var httpClientFactory = provider.GetRequiredService<IHttpClientFactory>();
        var authenticationHttpClient = httpClientFactory.CreateClient();
        var oidcTokenAuthenticationClient = new OidcTokenAuthenticationClient(paymentSystemOptions.OidcOptions, authenticationHttpClient);

        return new PaymentSystemClient(logger, httpClient, oidcTokenAuthenticationClient);
    });

    builder.Services.AddScoped<IPaymentSystemService, PaymentSystemService>();
}

// configure beamer options
builder.Services.Configure<BeamerOptions>(builder.Configuration.GetSection(BeamerOptions.Option));

// add external user store
builder.Services.Configure<KeycloakOptions>(builder.Configuration.GetSection(KeycloakOptions.Option));
builder.Services.AddScoped<IExternalUserStore, KeycloakUserStore>();

var app = builder.Build();

app.UseResponseCompression();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    // app.UseWebAssemblyDebugging();
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("v1/swagger.json", "TournamentManager v1");
        c.OAuthClientId("tournaments");
        c.OAuthUsePkce();
    });
}

if (app.Configuration.GetSection(DatabaseOptions.Option).Get<DatabaseOptions>()!.AutoApplyMigrations)
{
    await using var scope = app.Services.GetService<IServiceScopeFactory>()!.CreateAsyncScope();
    var db = scope.ServiceProvider.GetRequiredService<TournamentManagerDbContext>();
    await db.Database.MigrateAsync();
}

if (app.Environment.IsDevelopment())
{
    app.UseCors("AllowAllCorsPolicy");
}
else
{
    app.UseCors();
}

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();
app.MapHub<NotificationsHub>("/notifications");
app.MapHub<BeamerHub>("/beamer-hub");
app.MapHealthChecks("/healthz").AllowAnonymous();

await app.RunAsync();
