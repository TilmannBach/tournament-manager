namespace Clanwars.TournamentManager.Backend.Options;

public class BeamerOptions
{
    public const string Option = "Beamer";

    public OscOptions? Osc { get; set; }
}

public class OscOptions
{
    public OscCommands Commands { get; set; } = null!;
    public OscEndpoint Endpoint { get; set; } = null!;
}

public record OscCommands
{
    public string[] NewsPublished { get; init; } = null!;
    public string[] ShoutPublished { get; init; } = null!;
    public string[] WelcomePublished { get; init; } = null!;
    public string[] TournamentMatchPublished { get; init; } = null!;
}

public record OscEndpoint
{
    public string Host { get; init; } = null!;
    public int Port { get; init; }
    public string Prefix { get; init; } = null!;
}
