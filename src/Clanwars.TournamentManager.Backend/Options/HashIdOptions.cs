using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Backend.Options;

public class HashIdOptions
{
    public const string Option = "HashIds";

    [Required]
    public string Salt { get; set; } = null!;

    [Required]
    public int MinLength { get; set; } = 6;
}
