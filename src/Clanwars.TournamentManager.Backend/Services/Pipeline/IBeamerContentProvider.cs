using Clanwars.TournamentManager.Backend.Helpers.Exceptions;
using Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;

namespace Clanwars.TournamentManager.Backend.Services.Pipeline;

public interface IBeamerContentProvider
{
    void QueueNewsMessage(int newsId);
    void QueueShoutMessage(int shoutId);

    void QueueWelcomeMessage(int userId);

    void QueueImageMessage(string beamerImageHashId, int timeout);

    void QueueTournamentMatch(int tournamentId, int tournamentMatchId);

    /// <exception cref="NoBeamerContentAvailableException">Thrown if no entry could be determined weather from the queue nor from some random content.</exception>
    Task<(IBeamerHubMessage beamerHubMessage, int suggestedDisplayTime)> Dequeue();
}
