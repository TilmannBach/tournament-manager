using System.Net.Sockets;
using System.Text.Json;
using System.Timers;
using Clanwars.TournamentManager.Backend.Helpers.Exceptions;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Options;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;
using CoreOSC;
using CoreOSC.IO;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;

namespace Clanwars.TournamentManager.Backend.Services.Pipeline;

public sealed class BeamerPublisher : IBeamerPublisher
{
    private bool _autoPublishOn;

    private readonly IHubContext<BeamerHub, IBeamerHub> _pipelineHub;
    private readonly IBeamerContentProvider _beamerContentProvider;
    private readonly Timer _nextPublishTimer;
    private readonly Timer _reactivationTimer;
    private readonly BeamerOptions _beamerOptions;
    private readonly ILogger<BeamerPublisher> _logger;

    public BeamerPublisher(
        ILogger<BeamerPublisher> logger,
        IHubContext<BeamerHub, IBeamerHub> pipelineHub,
        IBeamerContentProvider beamerContentProvider,
        IOptions<BeamerOptions> beamerOptions
    )
    {
        _logger = logger;
        _pipelineHub = pipelineHub;
        _beamerContentProvider = beamerContentProvider;
        _beamerOptions = beamerOptions.Value;
        _nextPublishTimer = new Timer(10 * 1000); // default tick is 10 seconds
        _nextPublishTimer.Elapsed += OnNextPublishTimerElapsed;
        _nextPublishTimer.AutoReset = false;

        _reactivationTimer = new Timer();
        _reactivationTimer.Elapsed += OnReactivationTimerElapsed;
        _reactivationTimer.AutoReset = false;

        PublisherAutoMode = true;
    }

    public bool PublisherAutoMode
    {
        get => _autoPublishOn;
        set
        {
            _autoPublishOn = value;
            if (_autoPublishOn)
            {
                EnableAutoPublishing();
            }
            else
            {
                DisableAutoPublishing();
            }
        }
    }

    public IBeamerHubMessage CurrentEntry { get; private set; }

    private void OnNextPublishTimerElapsed(object? sender, ElapsedEventArgs e)
    {
        _logger.LogWarning("next tick");
        PublishNextAsync().Wait();
    }

    private async Task PublishNextAsync()
    {
        try
        {
            var next = await _beamerContentProvider.Dequeue();
            CurrentEntry = next.beamerHubMessage;
            await SendNextEntryToSignalrHubAsync(next);
            if (next.beamerHubMessage.IsNew && _beamerOptions.Osc is not null)
            {
                await SendOscCommand(CurrentEntry);
            }

            _nextPublishTimer.Interval = next.suggestedDisplayTime;
            _nextPublishTimer.Start();
        }
        catch (NoBeamerContentAvailableException)
        {
            PublisherAutoMode = false;
        }
    }

    private async Task SendNextEntryToSignalrHubAsync((IBeamerHubMessage beamerHubMessage, int suggestedDisplayTime) next)
    {
        _logger.LogDebug("Sending DisplayTime: {Time} Msg: {Msg}", next.suggestedDisplayTime, JsonSerializer.Serialize((object)next.beamerHubMessage));

        switch (next.beamerHubMessage)
        {
            case ShoutBeamerHubMessage shoutMessage:
                await _pipelineHub.Clients.All.PublishShout(shoutMessage);
                break;
            case NewsBeamerHubMessage newsMessage:
                await _pipelineHub.Clients.All.PublishNews(newsMessage);
                break;
            case WelcomeBeamerHubMessage welcomeMessage:
                await _pipelineHub.Clients.All.PublishWelcome(welcomeMessage);
                break;
            case ImageBeamerHubMessage imageBeamerMessage:
                await _pipelineHub.Clients.All.PublishImage(imageBeamerMessage);
                break;
            case TournamentBeamerHubMessage tournamentBeamerMessage:
                await _pipelineHub.Clients.All.PublishTournaments(tournamentBeamerMessage);
                break;
            case UpcomingMatchesBeamerHubMessage upcomingMatchesBeamerHubMessage:
                await _pipelineHub.Clients.All.PublishUpcomingMatches(upcomingMatchesBeamerHubMessage);
                break;
            case FinishedMatchesBeamerHubMessage finishedMatchesBeamerHubMessage:
                await _pipelineHub.Clients.All.PublishFinishedMatches(finishedMatchesBeamerHubMessage);
                break;
            case TournamentMatchResultBeamerHubMessage tournamentMatchResultBeamerHubMessage:
                await _pipelineHub.Clients.All.PublishTournamentMatchResult(tournamentMatchResultBeamerHubMessage);
                break;
        }
    }

    private void OnReactivationTimerElapsed(object? sender, ElapsedEventArgs e)
    {
        PublisherAutoMode = true;
    }

    private void EnableAutoPublishing()
    {
        _pipelineHub.Clients.All.AutoplayState(true);
        PublishNextAsync().Wait();
    }

    private void DisableAutoPublishing()
    {
        _nextPublishTimer.Stop();
        _pipelineHub.Clients.All.AutoplayState(false);
    }

    private async Task SendOscCommand(IBeamerHubMessage message)
    {
        if (_beamerOptions.Osc == null)
        {
            _logger.LogWarning("OSC options are not configured");
            return;
        }

        var command = message switch
        {
            NewsBeamerHubMessage => _beamerOptions.Osc.Commands.NewsPublished,
            ShoutBeamerHubMessage => _beamerOptions.Osc.Commands.ShoutPublished,
            WelcomeBeamerHubMessage => _beamerOptions.Osc.Commands.WelcomePublished,
            TournamentMatchResultBeamerHubMessage => _beamerOptions.Osc.Commands.TournamentMatchPublished,
            _ => null
        };
        if (command is null || command.Length < 2)
        {
            _logger.LogDebug("No command found for message {Message}", message.GetType());
            return;
        }

        var oscEndpoint = _beamerOptions.Osc.Endpoint;
        using var udpClient = new UdpClient(oscEndpoint.Host, oscEndpoint.Port);
        var oscMessage = new OscMessage(new Address($"{oscEndpoint.Prefix}{command[0]}"), command.Skip(1));
        await udpClient.SendMessageAsync(oscMessage);
    }

    public async Task Skip()
    {
        if (PublisherAutoMode)
        {
            _nextPublishTimer.Stop();
            await PublishNextAsync();
        }
    }

    public void Dispose()
    {
        _nextPublishTimer.Stop();
        _nextPublishTimer.Dispose();
    }
}
