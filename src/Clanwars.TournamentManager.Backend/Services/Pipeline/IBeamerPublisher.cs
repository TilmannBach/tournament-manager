using Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;

namespace Clanwars.TournamentManager.Backend.Services.Pipeline;

public interface IBeamerPublisher : IDisposable
{
    bool PublisherAutoMode { get; set; }
    IBeamerHubMessage CurrentEntry { get; }
    Task Skip();
}
