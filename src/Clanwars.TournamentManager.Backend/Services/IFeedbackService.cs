using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public interface IFeedbackService
{
    Task<PagedResult<Feedback>> GetFeedbacksAsync(SieveModel sieveModel);

    Task AddFeedbackAsync(Feedback feedback);
    Task MarkAsResolvedAsync(int id, bool resolvedState = true);
}
