using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Extensions;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public class ImageService(IImageRepository imageRepository) : IImageService
{
    public Task<PagedResult<Image>> GetImagesAsync(SieveModel sieveModel)
    {
        return imageRepository.GetImagesAsync(sieveModel);
    }

    public Task<ImageWithContent?> GetImageWithContentByIdAsync(int imageId)
    {
        return imageRepository.GetImageWithContentByIdAsync(imageId);
    }

    public Task<Image?> GetImageByIdAsync(int imageId)
    {
        return imageRepository.GetImageByIdAsync(imageId);
    }

    public Task UpdateImageAsync(Image image)
    {
        return imageRepository.UpdateImageAsync(image);
    }

    public Task<ImageWithContent?> GetImageWithContentByHashAndImageTypeAsync(byte[] hash, ImageType imageType)
    {
        return imageRepository.GetImageWithContentByHashAndImageTypeAsync(hash, imageType);
    }

    public Task<ImageWithContent> AddImageWithContentAsync(ImageType type, string content)
    {
        var byteArray = content.Sha256();
        return imageRepository.AddImageWithContentAsync(type, content, byteArray);
    }

    public Task RemoveImageAsync(int imageId)
    {
        return imageRepository.RemoveImageAsync(imageId);
    }

    public Task<Image?> GetRandomImageAsync(ImageType imageType)
    {
        return imageRepository.GetRandomImageAsync(imageType);
    }
}
