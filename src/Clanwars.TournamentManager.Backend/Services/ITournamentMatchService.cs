using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public interface ITournamentMatchService
{
    Task<PagedResult<TournamentMatch>> GetTournamentMatchesAsync(SieveModel sieveModel);
    Task<TournamentMatch?> GetTournamentMatchByIdAsync(int tournamentMatchId);
    Task ScoreMatchAsync(TournamentMatch match);
}
