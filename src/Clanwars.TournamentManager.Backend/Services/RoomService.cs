using Clanwars.TournamentManager.Backend.Entities.Rooms;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public class RoomService : IRoomService
{
    private readonly IRoomRepository _roomRepository;
    private readonly IHubContext<NotificationsHub, INotificationsSender> _notificationsHub;

    public RoomService(
        IRoomRepository roomRepository,
        IHubContext<NotificationsHub, INotificationsSender> notificationsHub
    )
    {
        _roomRepository = roomRepository;
        _notificationsHub = notificationsHub;
    }

    public Task<PagedResult<Room>> GetRoomsAsync(SieveModel sieveModel)
    {
        return _roomRepository.GetRoomsAsync(sieveModel);
    }

    public Task<Room?> GetRoomByIdAsync(int roomId)
    {
        return _roomRepository.GetRoomByIdAsync(roomId);
    }

    public async Task AddRoomAsync(Room room)
    {
        await _roomRepository.AddRoomAsync(room);
        await _notificationsHub.Clients.All.SendRoomsChanged(new RoomHubMessage
        {
            Id = room.Id,
            Event = RoomChangeEvent.Created
        });
    }

    public async Task DeleteRoomAsync(Room room)
    {
        await _roomRepository.DeleteRoomAsync(room);
        await _notificationsHub.Clients.All.SendRoomsChanged(new RoomHubMessage
        {
            Id = room.Id,
            Event = RoomChangeEvent.Deleted
        });
    }

    public async Task UpdateRoomAsync(Room room)
    {
        await _roomRepository.UpdateRoomAsync(room);
        await _notificationsHub.Clients.All.SendRoomsChanged(new RoomHubMessage
        {
            Id = room.Id,
            Event = RoomChangeEvent.Changed
        });
    }

    public async Task<Room> CopyRoomAsync(string name, Room roomToCopy, int targetEventId)
    {
        var newRoom = new Room
        {
            EventId = targetEventId,
            Name = name,
            SizeX = roomToCopy.SizeX,
            SizeY = roomToCopy.SizeY,
            GridSquares = roomToCopy.GridSquares.Select(square => new GridSquare
            {
                X = square.X,
                Y = square.Y,
                Type = square.Type,
                Rotation = square.Rotation
            }).ToList()
        };
        await _roomRepository.AddRoomAsync(newRoom);
        return newRoom;
    }

    public Task<PagedResult<GridSquare>> GetGridSquaresAsync(SieveModel sieveModel)
    {
        return _roomRepository.GetGridSquaresAsync(sieveModel);
    }

    public Task<GridSquare?> GetGridSquareByIdAsync(int gridSquareId)
    {
        return _roomRepository.GetGridSquareByIdAsync(gridSquareId);
    }

    public Task<GridSquare?> GetGridSquareByCoordinatesAsync(int roomId, int x, int y)
    {
        return _roomRepository.GetGridSquareByCoordinatesAsync(roomId, x, y);
    }

    public async Task AddGridSquareAsync(GridSquare gridSquare)
    {
        await _roomRepository.AddGridSquareAsync(gridSquare);
        await _notificationsHub.Clients.All.SendRoomsChanged(new RoomHubMessage
        {
            Id = gridSquare.Room.Id,
            Event = RoomChangeEvent.Changed
        });
    }

    public async Task DeleteGridSquareAsync(GridSquare gridSquare)
    {
        await _roomRepository.DeleteGridSquareAsync(gridSquare);
        await _notificationsHub.Clients.All.SendRoomsChanged(new RoomHubMessage
        {
            Id = gridSquare.Room.Id,
            Event = RoomChangeEvent.Changed
        });
    }

    public async Task UpdateGridSquareAsync(GridSquare gridSquare)
    {
        await _roomRepository.UpdateGridSquareAsync(gridSquare);
        await _notificationsHub.Clients.All.SendRoomsChanged(new RoomHubMessage
        {
            Id = gridSquare.Room.Id,
            Event = RoomChangeEvent.Changed
        });
    }

    public async Task PlaceUserAsync(GridSquare gridSquare)
    {
        var usersCurrentSeat = await _roomRepository.GetGridSquares().Where(gs => gs.RoomId == gridSquare.RoomId && gs.UserId == gridSquare.UserId).SingleOrDefaultAsync();
        if (usersCurrentSeat is not null)
        {
            usersCurrentSeat.UserId = null;
        }

        await _roomRepository.PlaceUserAsync(gridSquare);
        await _notificationsHub.Clients.All.SendRoomsChanged(new RoomHubMessage
        {
            Id = gridSquare.Room.Id,
            Event = RoomChangeEvent.Changed
        });
    }
}
