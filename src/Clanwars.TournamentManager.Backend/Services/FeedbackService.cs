using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public class FeedbackService : IFeedbackService
{
    private readonly IFeedbackRepository _feedbackRepository;

    public FeedbackService(IFeedbackRepository feedbackRepository)
    {
        _feedbackRepository = feedbackRepository;
    }

    public async Task<PagedResult<Feedback>> GetFeedbacksAsync(SieveModel sieveModel)
    {
        return await _feedbackRepository.GetFeedbacksAsync(sieveModel);
    }

    public async Task AddFeedbackAsync(Feedback feedback)
    {
        await _feedbackRepository.AddFeedbackAsync(feedback);
    }

    public async Task MarkAsResolvedAsync(int id, bool resolvedState = true)
    {
        var feedback = await _feedbackRepository.GetFeedbackByIdAsync(id);
        feedback.Resolved = resolvedState;
        await _feedbackRepository.UpdateFeedbackAsync(feedback);
    }
}
