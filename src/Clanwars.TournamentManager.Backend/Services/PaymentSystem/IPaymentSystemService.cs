using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Clanwars.PaymentSystem.Lib.Messages.Transaction;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services.PaymentSystem;

public interface IPaymentSystemService
{
    Task<AccountInfo?> GetAccountAsync(Guid accountId);
    Task<AccountInfo?> GetAccountByEmailAsync(string email);
    Task<AccountInfo> CreateAccountAsync(CreateAccountMessage createAccountMessage);
    Task<AccountInfo> UpdateAccountAsync(Guid accountId, UpdateAccountMessage updateAccountMessage);
    Task<IList<PointOfSaleInfoWithCurrentCard>> GetCurrentCardsAsync();
    Task<IPagedResult<TransactionResponse>> GetTransactionsAsync(SieveModel sieveModel);
}
