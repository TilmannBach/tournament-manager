namespace Clanwars.TournamentManager.Backend.Services.PaymentSystem.OidcTokenAuthenticationClient;

public interface IOidcTokenAuthenticationClient
{
    Task AddAuthorizationHeaderToHttpRequestAsync(HttpRequestMessage request);
}
