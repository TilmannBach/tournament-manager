using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Backend.Services.Pipeline;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using Microsoft.AspNetCore.SignalR;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public class ShoutService : IShoutService
{
    private readonly IShoutRepository _shoutRepository;
    private readonly IHubContext<NotificationsHub, INotificationsSender> _notificationsHub;
    private readonly IBeamerContentProvider _pipelinePublisher;

    public ShoutService(
        IShoutRepository shoutRepository,
        IHubContext<NotificationsHub, INotificationsSender> notificationsHub,
        IBeamerContentProvider pipelinePublisher
    )
    {
        _shoutRepository = shoutRepository;
        _notificationsHub = notificationsHub;
        _pipelinePublisher = pipelinePublisher;
    }

    public Task<PagedResult<Shout>> GetShoutsAsync(SieveModel sieveModel)
    {
        return _shoutRepository.GetShoutsAsync(sieveModel);
    }

    public Task<Shout?> GetShoutByIdAsync(int shoutId)
    {
        return _shoutRepository.GetShoutByIdAsync(shoutId);
    }

    public async Task AddShoutAsync(Shout shout)
    {
        shout.CreatedAt = DateTime.Now;
        await _shoutRepository.AddShoutAsync(shout);
        await _notificationsHub.Clients.All.SendShoutsChanged(new ShoutHubMessage
        {
            Id = shout.Id,
            Event = ShoutChangeEvent.Created
        });

        _pipelinePublisher.QueueShoutMessage(shout.Id);
    }

    public async Task DeleteShoutAsync(Shout shout)
    {
        await _shoutRepository.DeleteShoutAsync(shout);
        await _notificationsHub.Clients.All.SendShoutsChanged(new ShoutHubMessage
        {
            Id = shout.Id,
            Event = ShoutChangeEvent.Deleted
        });
    }

    public async Task UpdateShoutAsync(Shout shout)
    {
        shout.CreatedAt = DateTime.Now;
        await _shoutRepository.UpdateShoutAsync(shout);
        await _notificationsHub.Clients.All.SendShoutsChanged(new ShoutHubMessage
        {
            Id = shout.Id,
            Event = ShoutChangeEvent.Changed
        });
    }

    public Task<Shout?> GetRandomAsync()
    {
        return _shoutRepository.GetRandomAsync();
    }
}
