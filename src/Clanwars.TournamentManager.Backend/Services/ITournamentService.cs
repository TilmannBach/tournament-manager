using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Lib;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public interface ITournamentService
{
    Task<PagedResult<Tournament>> GetTournamentsAsync(SieveModel sieveModel);
    Task<Tournament?> GetTournamentByIdAsync(int tournamentId);
    Task AddTournamentAsync(Tournament tournament);
    Task StartTournamentAsync(Tournament tournament);
    Task UpdateTournamentAsync(Tournament tournament);
    Task DeleteTournamentAsync(Tournament tournament);
    Task<IList<Tournament>> GetTournamentsFromRunningEvent();
}
