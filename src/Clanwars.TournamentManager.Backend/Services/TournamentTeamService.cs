using Clanwars.TournamentManager.Backend.Entities.Tournaments;
using Clanwars.TournamentManager.Backend.Hubs;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Signalr;
using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;
using Microsoft.AspNetCore.SignalR;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Services;

public class TournamentTeamService : ITournamentTeamService
{
    private readonly ITournamentTeamRepository _tournamentTeamRepository;
    private readonly IHubContext<NotificationsHub, INotificationsSender> _notificationsHub;

    public TournamentTeamService(
        ITournamentTeamRepository tournamentTeamRepository,
        IHubContext<NotificationsHub, INotificationsSender> notificationsHub
    )
    {
        _tournamentTeamRepository = tournamentTeamRepository;
        _notificationsHub = notificationsHub;
    }

    public Task<PagedResult<TournamentTeam>> GetTournamentTeamsAsync(SieveModel model)
    {
        return _tournamentTeamRepository.GetTournamentTeamsAsync(model);
    }

    public Task<TournamentTeam?> GetTournamentTeamByIdAsync(int tournamentTeamId)
    {
        return _tournamentTeamRepository.GetTournamentTeamByIdAsync(tournamentTeamId);
    }

    public async Task AddTournamentTeamAsync(TournamentTeam tournamentTeam)
    {
        tournamentTeam.Tournament.UpdatedAt=DateTime.Now;
        await _tournamentTeamRepository.AddTournamentTeamAsync(tournamentTeam);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournamentTeam.TournamentId,
            Event = TournamentChangeEvent.Changed
        });
        
        // if (_lanpartyScreenOptions.Enabled)
        // {
        //     await SendUpdateTournamentEntryToLps(tournamentTeam);
        // }
    }

    // private async Task SendUpdateTournamentEntryToLps(TournamentTeam tournamentTeam)
    // {
    //     var tournament = tournamentTeam.Tournament;
    //
    //     var updateTournamentEntry = new UpdateTournamentEntry
    //     {
    //         Mode = tournament.Mode.ToLps(),
    //         State = tournament.State.ToLps(),
    //         Name = tournament.Name,
    //         TeamLimit = _lanpartyScreenOptions.TeamLimit,
    //         StartTime = tournament.CreatedAt,
    //         PlayersPerTeam = tournament.PlayerPerTeam ?? 1,
    //         Teams = tournament.Teams.Count(tt => !tt.IsByeTeam)
    //     };
    //     await _tournamentsClient.TournamentsPUTAsync(tournament.LanpartyScreenId!.Value, updateTournamentEntry);
    // }

    public async Task UpdateTournamentTeamAsync(TournamentTeam tournamentTeam)
    {
        tournamentTeam.Tournament.UpdatedAt = DateTime.Now;
        await _tournamentTeamRepository.UpdateTournamentTeamAsync(tournamentTeam);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournamentTeam.TournamentId,
            Event = TournamentChangeEvent.Changed
        });
    }

    public async Task DeleteTournamentTeamAsync(TournamentTeam tournamentTeam)
    {
        tournamentTeam.Tournament.UpdatedAt=DateTime.Now;
        await _tournamentTeamRepository.DeleteTournamentTeamAsync(tournamentTeam);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournamentTeam.TournamentId,
            Event = TournamentChangeEvent.Changed
        });
        
        // TODO: Implement this
        // if (_lanpartyScreenOptions.Enabled)
        // {
        //     await SendUpdateTournamentEntryToLps(tournamentTeam);
        // }
    }

    public async Task AddUserToTeamAsync(TournamentTeam tournamentTeam, int userId, EntityPermission permission)
    {
        tournamentTeam.Tournament.UpdatedAt=DateTime.Now;
        await _tournamentTeamRepository.AddUserToTeamAsync(tournamentTeam, userId, permission);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournamentTeam.TournamentId,
            Event = TournamentChangeEvent.Changed
        });
    }

    public async Task RemoveUserFromTeamAsync(TournamentTeam tournamentTeam, TournamentTeamMember member)
    {
        tournamentTeam.Tournament.UpdatedAt=DateTime.Now;
        await _tournamentTeamRepository.RemoveUserFromTeamAsync(tournamentTeam, member);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournamentTeam.TournamentId,
            Event = TournamentChangeEvent.Changed
        });
    }

    public async Task SealTeamAsync(TournamentTeam tournamentTeam)
    {
        tournamentTeam.Tournament.UpdatedAt=DateTime.Now;
        await _tournamentTeamRepository.SealTeamAsync(tournamentTeam);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournamentTeam.TournamentId,
            Event = TournamentChangeEvent.Changed
        });
    }

    public async Task AddPseudoTeamsToEliminationTournamentsAsync(Tournament tournament)
    {
        tournament.UpdatedAt=DateTime.Now;
        await _tournamentTeamRepository.AddPseudoTeamsToEliminationTournamentsAsync(tournament);
        await _notificationsHub.Clients.All.SendTournamentsChanged(new TournamentHubMessage
        {
            Id = tournament.Id,
            Event = TournamentChangeEvent.Changed
        });
    }
}
