using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities;

public class Image
{
    public int Id { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    [Required]
    public ImageType Type { get; set; }

    [Required]
    // hash based on Content string
    [MaxLength(32)]
    public byte[] Hash { get; set; } = null!;

    public ImageWithContent ImageWithContent { get; set; } = null!;
    
    [Sieve(CanFilter = true)]
    public bool? ShowOnStream { get; set; }
}
