using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities;

public class ImageWithContent
{
    public int Id { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    [Required]
    public ImageType Type { get; set; }

    [Required]
    public string Content { get; set; } = null!;

    [Required]
    // hash based on Content string
    [MaxLength(32)]
    public byte[] Hash { get; set; } = null!;
}
