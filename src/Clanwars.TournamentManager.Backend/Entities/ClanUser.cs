using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums;

namespace Clanwars.TournamentManager.Backend.Entities;

public class ClanUser
{
    public int Id { get; set; }

    [Required]
    public int UserId { get; set; }

    [Required]
    public int ClanId { get; set; }

    public User User { get; set; } = null!;
    public Clan Clan { get; set; } = null!;

    public EntityPermission Permission { get; set; }
}
