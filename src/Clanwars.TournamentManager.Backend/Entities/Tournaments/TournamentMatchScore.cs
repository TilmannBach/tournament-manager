using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums;

namespace Clanwars.TournamentManager.Backend.Entities.Tournaments;

public class TournamentMatchScore
{
    public int Id { get; set; }

    public TournamentMatch TournamentMatch { get; set; } = null!;

    [Required]
    public int TournamentMatchId { get; set; }

    public int? PreviousMatchIndex { get; set; }
    public MatchRank? PreviousMatchRank { get; set; }

    public TournamentTeam TournamentTeam { get; set; } = null!;
    public int? TournamentTeamId { get; set; }

    public int? Score { get; set; }
}
