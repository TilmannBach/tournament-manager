using Clanwars.TournamentManager.Lib.Enums;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;

namespace Clanwars.TournamentManager.Backend.Entities.Tournaments.Extensions;

public static partial class TournamentExtensions
{
    public static void GenerateSingleEliminationMatches(this Tournament tournament)
    {
        var roundsCount = Convert.ToInt32(Math.Log(tournament.Teams.Count, 2));
        var shuffledTeams = ShuffleTeams(tournament.Teams);

        var matchIndex = 0;
        var rounds = new List<TournamentRound>();

        // all rounds
        for (var round = 1; round <= roundsCount; round++)
        {
            var matchesCountRound = (int) Math.Pow(2, roundsCount - round);
            var matchesCountTotalBefore = rounds.Take(rounds.Count - 1).Sum(r => r.Matches.Count);

            var winnerRound = new TournamentRound
            {
                Matches = new List<TournamentMatch>(),
                Order = round,
                Type = round == roundsCount ? TournamentRoundType.Final : TournamentRoundType.WinnerBracket
            };
            rounds.Add(winnerRound);

            // winner bracket
            for (var j = 0; j < matchesCountRound * 2; j++)
            {
                var m = new TournamentMatch
                {
                    MatchIndex = matchIndex++,
                    Scores = new List<TournamentMatchScore>
                    {
                        new()
                        {
                            PreviousMatchIndex = round == 1 ? null : j + matchesCountTotalBefore,
                            PreviousMatchRank = MatchRank.Winner,
                            TournamentTeamId = round == 1 ? shuffledTeams[j].Id : null
                        },
                        new()
                        {
                            PreviousMatchIndex = round == 1 ? null : j + 1 + matchesCountTotalBefore,
                            PreviousMatchRank = MatchRank.Winner,
                            TournamentTeamId = round == 1 ? shuffledTeams[j + 1].Id : null
                        }
                    }
                };
                j += 1;
                winnerRound.Matches.Add(m);
            }
        }

        // small final
        var semifinal = new TournamentMatch
        {
            MatchIndex = matchIndex,
            Scores = new List<TournamentMatchScore>
            {
                new()
                {
                    PreviousMatchIndex = rounds.Sum(r => r.Matches.Count) - 3,
                    PreviousMatchRank = MatchRank.Loser
                },
                new()
                {
                    PreviousMatchIndex = rounds.Sum(r => r.Matches.Count) - 2,
                    PreviousMatchRank = MatchRank.Loser
                }
            }
        };
        rounds.Last().Matches.Add(semifinal);


        tournament.Rounds = rounds;
    }
}
