using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums.Tournaments;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities.Tournaments;

public class Tournament
{
    [Sieve(CanFilter = true, CanSort = true)]
    public int Id { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public string Name { get; set; } = null!;

    public Image? Image { get; set; }
    public int? ImageId { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public int EventId { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public TournamentMode Mode { get; set; }

    public int? PlayerPerTeam { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime CreatedAt { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime? UpdatedAt { get; set; }

    public IList<TournamentTeam> Teams { get; set; } = new List<TournamentTeam>();

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public TournamentState State { get; set; }

    public IList<TournamentRound> Rounds { get; set; } = new List<TournamentRound>();
}
