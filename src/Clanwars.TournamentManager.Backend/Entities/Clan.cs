using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Lib.Enums;
using Sieve.Attributes;

namespace Clanwars.TournamentManager.Backend.Entities;

public class Clan
{
    [Sieve(CanFilter = true, CanSort = true)]
    public int Id { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    [MaxLength(255)]
    public string Name { get; set; } = null!;

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    [MaxLength(50)]
    public string ClanTag { get; set; } = null!;

    public Image? Image { get; set; }
    public int? ImageId { get; set; }
    
    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime CreatedAt { get; set; }
    
    [Sieve(CanFilter = true, CanSort = true)]
    public DateTime? UpdatedAt { get; set; }
    
    public IList<ClanUser> Members { get; set; } = new List<ClanUser>();
}

public static class ClanExtensions
{
    public static bool IsClanOwner(this Clan clan, User user)
    {
        return clan.Members.Any(m => m.UserId == user.Id && m.Permission == EntityPermission.Owner);
    }

    public static bool HasPendingJoins(this Clan clan)
    {
        return clan.Members.Any(m => m.Permission == EntityPermission.Pending);
    }

    public static ClanUser? GetClanMember(this Clan clan, int userId)
    {
        return clan.Members.SingleOrDefault(cu => cu.UserId == userId);
    }
}
