﻿using System.ComponentModel.DataAnnotations;
using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Repositories;
using Clanwars.TournamentManager.Backend.Services.Pipeline;
using Clanwars.TournamentManager.Lib.Messages;
using Clanwars.TournamentManager.Lib.Messages.Pipeline;
using HashidsNet;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class PipelineController(
    IPipelineRepository pipelineRepository,
    IBeamerPublisher publisher,
    IMapper mapper,
    IHashids hashids,
    ILogger<PipelineController> logger,
    IBeamerContentProvider beamerContentProvider) : ControllerBase
{
    private const int AnimationOffset = 3000;

    // GET api/v1/pipeline/modulestates
    [HttpGet("modulestates")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IList<PipelineModuleStateResponse>>> GetModuleStates()
    {
        return Ok(mapper.Map<IList<PipelineModuleStateResponse>>(await pipelineRepository.ActivationStatesAsync()));
    }

    // POST api/v1/pipeline/run
    [HttpPost("{signal:regex(^(run|stop|skip)$)}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public IActionResult SetPipelineStatus([FromRoute] PipelineStatusAction signal)
    {
        switch (signal)
        {
            case PipelineStatusAction.Run:
                publisher.PublisherAutoMode = true;
                break;
            case PipelineStatusAction.Stop:
                publisher.PublisherAutoMode = false;
                break;
            case PipelineStatusAction.Skip:
                publisher.Skip();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(signal), signal, null);
        }

        return NoContent();
    }

    /// <summary>
    /// Sets slide timeouts for a module.
    /// </summary>
    /// <remarks>
    /// Sample request:
    /// 
    ///     {
    ///         "timeoutInitial": 900,
    ///         "timeoutRecurring": 60
    ///     }
    /// 
    /// </remarks>
    /// <param name="module"></param>
    /// <param name="moduleSettings"></param>
    /// <returns></returns>
    [HttpPut("{module}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<PipelineModuleState>> SetModuleSettings([FromRoute] ModuleType module, [FromBody] [Required] UpdateModuleTimeoutsMessage moduleSettings)
    {
        var mod = await pipelineRepository.SetModuleTimeoutsAsync(module, (uint)moduleSettings.TimeoutInitial, (uint)moduleSettings.TimeoutRecurring);
        return Ok(mod);
    }

    // PUT api/v1/pipeline/{module}/enable
    [HttpPut("{module}/{signal:regex(^(enable|disable)$)}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult> SetModuleState([FromRoute] ModuleType module, [FromRoute] PipelineModuleStatusAction signal)
    {
        switch (signal)
        {
            case PipelineModuleStatusAction.Enable:
                await pipelineRepository.EnableModuleAsync(module);
                break;
            case PipelineModuleStatusAction.Disable:
                await pipelineRepository.DisableModuleAsync(module);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(signal), signal, null);
        }

        return NoContent();
    }

    // POST api/v1/pipeline/activate-image/<hashId>/<timeout>
    [HttpPost("activate-image/{imageHashId}/{timeout:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [Authorize(Roles = "tournament-moderator")]
    public Task<ActionResult> ActivateStaticPicture(
        [FromRoute] string imageHashId,
        [FromRoute][Range(0, int.MaxValue)] int timeout = 600
    )
    {
        var rawId = hashids.Decode(imageHashId);

        if (rawId.Length == 0)
        {
            return Task.FromResult<ActionResult>(NotFound());
        }

        var timeoutWithAnimationOffset = timeout switch
        {
            > 0 => timeout * 1000 + AnimationOffset,
            _ => int.MaxValue
        };

        logger.LogDebug("activating image {ImageHashId} for {Timeout} seconds", imageHashId, timeout);

        beamerContentProvider.QueueImageMessage(imageHashId, timeoutWithAnimationOffset);
        publisher.Skip();
        return Task.FromResult<ActionResult>(NoContent());
    }
}
