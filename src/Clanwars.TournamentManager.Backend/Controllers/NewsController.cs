using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.Models;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.News;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class NewsController : ControllerBase
{
    private readonly INewsService _newsService;
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public NewsController(
        INewsService newsService,
        IUserService userService,
        IMapper mapper
    )
    {
        _newsService = newsService;
        _userService = userService;
        _mapper = mapper;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<NewsResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(News))]
    public async Task<ActionResult<IEnumerable<NewsResponse>>> GetNews([FromQuery] SieveModel sieveModel)
    {
        var pagedResult = await _newsService.GetNewsAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<NewsResponse>>(pagedResult));
    }

    [HttpPost]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<NewsResponse>> CreateNews(
        [FromBody] CreateNewsMessage createNewsMessage
    )
    {
        var newNews = _mapper.Map<News>(createNewsMessage);
        newNews.Author = await _userService.GetByClaimsPrincipalAsync(User);

        await _newsService.AddNewsAsync(newNews);

        return CreatedAtAction(nameof(GetNews), new {filters = $"id=={newNews.Id}"}, _mapper.Map<NewsResponse>(newNews));
    }

    [HttpPut("{newsId:int}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<NewsResponse>> UpdateNews(
        [FromBody] UpdateNewsRequest updateRequest,
        [FromRoute] int newsId
    )
    {
        var news = await _newsService.GetNewsByIdAsync(newsId);

        if (news is null)
        {
            return NotFound();
        }

        var updatedNews = _mapper.Map(updateRequest, news);
        await _newsService.UpdateNewsAsync(news);

        return Ok(_mapper.Map<NewsResponse>(updatedNews));
    }

    [HttpDelete("{newsId:int}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> DeleteNews(
        [FromRoute] int newsId
    )
    {
        var news = await _newsService.GetNewsByIdAsync(newsId);

        if (news is null)
        {
            return NotFound();
        }

        await _newsService.DeleteNewsAsync(news);
        return NoContent();
    }
}
