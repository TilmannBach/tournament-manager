using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.User;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class UsersController : ControllerBase
{
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public UsersController(
        IUserService userService,
        IMapper mapper
    )
    {
        _userService = userService;
        _mapper = mapper;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<UserResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(User))]
    public async Task<ActionResult<PagedResult<UserResponse>>> GetUsers(
        [FromQuery] SieveModel sieveModel
    )
    {
        var pagedResult = await _userService.GetUsersAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<UserResponse>>(pagedResult));
    }

    [HttpGet("me")]
    public async Task<ActionResult<UserResponse>> GetAuthenticatedUser()
    {
        var user = await _userService.GetByClaimsPrincipalAsync(User);

        return Ok(_mapper.Map<UserResponse>(user));
    }

    [HttpPut("me")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<UserResponse>> UpdateUser(
        [FromBody] UpdateUserMessage updateUserMessage
    )
    {
        var currentUser = await _userService.GetByClaimsPrincipalAsync(User);

        if (!await _userService.IsUniqueNickname(currentUser.Id, updateUserMessage.Nickname))
        {
            ModelState.AddModelError("nickname","Nickname is already in use");
            return BadRequest(ProblemDetailsFactory.CreateValidationProblemDetails(HttpContext,ModelState));
        }
        
        await _userService.UpdateUserAsync(_mapper.Map(updateUserMessage, currentUser));

        return Ok(_mapper.Map<UserResponse>(currentUser));
    }

    [HttpPost]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(typeof(UserResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult<UserResponse>> CreateUserSkeleton(
        [FromBody] CreateUserSkeletonMessage request
    )
    {
        var skeleton = await _userService.CreateUserSkeletonAsync(request.Email.Trim(), request.FirstName.Trim(), request.LastName.Trim());
        return Ok(skeleton);
    }
}
