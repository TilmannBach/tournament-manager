using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Feedback;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class FeedbackController : ControllerBase
{
    private readonly IFeedbackService _feedbackService;
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public FeedbackController(
        IFeedbackService feedbackService,
        IUserService userService,
        IMapper mapper
    )
    {
        _feedbackService = feedbackService;
        _userService = userService;
        _mapper = mapper;
    }

    [HttpGet]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(typeof(IEnumerable<FeedbackResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(Feedback))]
    public async Task<ActionResult<IEnumerable<FeedbackResponse>>> GetFeedbacks(
        [FromQuery] SieveModel sieveModel
    )
    {
        var pagedResult = await _feedbackService.GetFeedbacksAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<FeedbackResponse>>(pagedResult));
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<ActionResult<FeedbackResponse>> CreateFeedback(
        [FromBody] CreateFeedbackMessage createFeedbackMessage
    )
    {
        var user = await _userService.GetByClaimsPrincipalAsync(User);

        var newFeedback = _mapper.Map<Feedback>(createFeedbackMessage);
        newFeedback.UserId = user.Id;
        newFeedback.CreatedAt = DateTime.Now;

        await _feedbackService.AddFeedbackAsync(newFeedback);

        return CreatedAtAction(nameof(GetFeedbacks), new { filter = $"id=={newFeedback.Id}" }, _mapper.Map<FeedbackResponse>(newFeedback));
    }

    [HttpPut("{feedbackId:int}/resolve")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult> SetFeedbackResolved([FromRoute] int feedbackId)
    {
        await _feedbackService.MarkAsResolvedAsync(feedbackId);

        return NoContent();
    }

    [HttpPut("{feedbackId:int}/unresolve")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult> SetFeedbackUnresolved([FromRoute] int feedbackId)
    {
        await _feedbackService.MarkAsResolvedAsync(feedbackId, false);

        return NoContent();
    }
}
