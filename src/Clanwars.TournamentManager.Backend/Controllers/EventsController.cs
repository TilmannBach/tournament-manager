using Clanwars.TournamentManager.Backend.Entities;
using Clanwars.TournamentManager.Backend.Helpers.OpenApi.Attributes;
using Clanwars.TournamentManager.Backend.Models;
using Clanwars.TournamentManager.Backend.Services;
using Clanwars.TournamentManager.Lib;
using Clanwars.TournamentManager.Lib.Messages.Events;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.TournamentManager.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
[Authorize]
public class EventsController : ControllerBase
{
    private readonly IEventService _eventService;
    private readonly IMapper _mapper;

    public EventsController(
        IEventService eventService,
        IMapper mapper
    )
    {
        _eventService = eventService;
        _mapper = mapper;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<EventResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(Event))]
    public async Task<ActionResult<IEnumerable<EventResponse>>> GetEvents(
        [FromQuery] SieveModel sieveModel
    )
    {
        var pagedResult = await _eventService.GetEventsAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<EventResponse>>(pagedResult));
    }

    [HttpGet("registrations")]
    [ProducesResponseType(typeof(IEnumerable<RegistrationResponse>), StatusCodes.Status200OK)]
    [FilterableEntity(typeof(EventUser))]
    public async Task<ActionResult<IEnumerable<RegistrationResponse>>> GetRegistrations(
        [FromQuery] SieveModel sieveModel
    )
    {
        var pagedResult = await _eventService.GetRegistrationsAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<RegistrationResponse>>(pagedResult));
    }

    [HttpPost]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<EventResponse>> CreateEvent(
        [FromBody] CreateEventMessage createEventMessage
    )
    {
        var newEvent = _mapper.Map<Event>(createEventMessage);

        await _eventService.AddEventAsync(newEvent);

        return CreatedAtAction(nameof(GetEvents), new {filters = $"id=={newEvent.Id}"}, _mapper.Map<EventResponse>(newEvent));
    }

    [HttpPut("{eventId:int}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<EventResponse>> UpdateEvent(
        [FromBody] UpdateEventRequest updateEventRequest,
        [FromRoute] int eventId
    )
    {
        var @event = await _eventService.GetEventByIdAsync(eventId);

        if (@event is null)
        {
            return NotFound();
        }

        _mapper.Map(updateEventRequest, @event);

        await _eventService.UpdateEventAsync(@event);

        return Ok(_mapper.Map<EventResponse>(@event));
    }

    [HttpDelete("{eventId:int}")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteEvent(
        [FromRoute] int eventId
    )
    {
        var @event = await _eventService.GetEventByIdAsync(eventId);

        if (@event is null)
        {
            return NotFound();
        }

        await _eventService.DeleteEventAsync(@event);
        return NoContent();
    }

    [HttpPut("{eventId:int}/join")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> JoinEvent(
        [FromBody] JoinEventRequest joinEventRequest,
        [FromRoute] int eventId
    )
    {
        var @event = await _eventService.GetEventByIdAsync(eventId);

        if (@event is null)
        {
            return NotFound();
        }

        await _eventService.AddUserToEventAsync(@event, joinEventRequest.UserId);
        
        return NoContent();
    }

    [HttpPut("{eventId:int}/leave")]
    [Authorize(Roles = "tournament-moderator")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> LeaveEvent(
        [FromBody] LeaveEventRequest leaveEventMessage,
        [FromRoute] int eventId
    )
    {
        var @event = await _eventService.GetEventByIdAsync(eventId);

        if (@event is null)
        {
            return NotFound();
        }

        await _eventService.RemoveUserFromEventAsync(@event, leaveEventMessage.UserId);

        return NoContent();
    }
}
