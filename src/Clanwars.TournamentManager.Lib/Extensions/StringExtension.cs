using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;

namespace Clanwars.TournamentManager.Lib.Extensions;

public static class StringExtension
{
    public static byte[] Sha256( this string value )
    {
        using var hash = SHA256.Create();
        var byteArray = hash.ComputeHash( Encoding.UTF8.GetBytes( value ) );
        // return Convert.ToHexString( byteArray ).ToLower();
        return byteArray;
    }
    
    public static string Sha256ToString( this string value )
    {
        var byteArray = value.Sha256();
        return Convert.ToHexString( byteArray ).ToLower();
    }
}
