namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public record ClanHubMessage : AbstractHubMessage
{
    public ClanChangeEvent Event { get; init; }
}
