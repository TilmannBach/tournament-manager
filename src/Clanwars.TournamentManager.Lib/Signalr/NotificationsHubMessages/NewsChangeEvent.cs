namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public enum NewsChangeEvent
{
    Created,
    Changed,
    Deleted
}
