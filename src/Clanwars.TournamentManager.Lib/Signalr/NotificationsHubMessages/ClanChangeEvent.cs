namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public enum ClanChangeEvent
{
    Created,
    Changed,
    Deleted
}
