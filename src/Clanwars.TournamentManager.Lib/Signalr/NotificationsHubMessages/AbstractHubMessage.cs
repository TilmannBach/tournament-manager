namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public abstract record AbstractHubMessage
{
    public int Id { get; init; }
}
