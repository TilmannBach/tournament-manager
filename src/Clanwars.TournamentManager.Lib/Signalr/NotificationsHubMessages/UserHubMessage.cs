namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public record UserHubMessage : AbstractHubMessage
{
    public UserChangeEvent Event { get; init; }
}
