namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public record ShoutHubMessage : AbstractHubMessage
{
    public ShoutChangeEvent Event { get; init; }
}
