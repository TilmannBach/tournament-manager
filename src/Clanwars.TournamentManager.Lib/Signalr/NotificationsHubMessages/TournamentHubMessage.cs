namespace Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

public record TournamentHubMessage : AbstractHubMessage
{
    public TournamentChangeEvent Event { get; init; }
}
