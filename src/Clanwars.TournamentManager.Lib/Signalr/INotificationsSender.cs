using Clanwars.TournamentManager.Lib.Signalr.NotificationsHubMessages;

namespace Clanwars.TournamentManager.Lib.Signalr;

public interface INotificationsSender
{
    Task SendTmVersion(Version? tmVersion);
    Task SendNewsChanged(NewsHubMessage nhMessage);
    Task SendTournamentsChanged(TournamentHubMessage thMessage);
    Task SendEventsChanged(EventHubMessage ehMessage);
    Task SendClansChanged(ClanHubMessage chMessage);
    Task SendRoomsChanged(RoomHubMessage rhMessage);
    Task SendUserChanged(UserHubMessage uhMessage);
    Task SendShoutsChanged(ShoutHubMessage shMessage);
}
