using Clanwars.TournamentManager.Lib.Messages.Pipeline;

namespace Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;

public record FinishedMatchesBeamerHubMessage(int TournamentId, bool IsNew = true) : IBeamerHubMessage
{
    public ModuleType ModuleType { get; init; } = ModuleType.MatchResults;
}
