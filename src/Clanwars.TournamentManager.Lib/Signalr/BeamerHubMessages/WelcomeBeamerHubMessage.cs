using Clanwars.TournamentManager.Lib.Messages.Pipeline;

namespace Clanwars.TournamentManager.Lib.Signalr.BeamerHubMessages;

public record WelcomeBeamerHubMessage(int UserId, bool IsNew = true) : IBeamerHubMessage
{
    public ModuleType ModuleType { get; init; } = ModuleType.WelcomeMessage;
}
