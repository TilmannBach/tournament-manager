using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.User;

public record UpdateUserMessage
{
    [Required]
    [StringLength(255, MinimumLength = 1)]
    public string FirstName { get; set; } = null!;

    [Required]
    [StringLength(255, MinimumLength = 1)]
    public string LastName { get; set; } = null!;

    [Required]
    [StringLength(255, MinimumLength = 1)]
    public string Nickname { get; set; } = null!;
}
