using Clanwars.TournamentManager.Lib.Enums;

namespace Clanwars.TournamentManager.Lib.Messages.User;

public record UserPermissionResponse
{
    public UserResponse User { get; set; } = null!;
    public int UserId { get; set; }

    public EntityPermission Permission { get; set; }
}
