using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.News;

public record UpdateNewsMessage
{
    [Required]
    [StringLength(255, MinimumLength = 8)]
    public string Headline { get; set; } = null!;

    [Required]
    public string Body { get; set; } = null!;
}
