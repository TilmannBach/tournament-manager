using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Events;

public record UpdateEventMessage
{
    [Required]
    [StringLength(255, MinimumLength = 8)]
    public string Name { get; set; } = null!;

    [Required]
    public DateTime StartDate { get; set; }

    [Required]
    public DateTime EndDate { get; set; }
}
