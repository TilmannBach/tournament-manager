using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Lib.Messages.Events;

public record RegistrationResponse
{
    public UserResponse User { get; init; } = null!;
    public EventResponse Event { get; init; } = null!;
    public DateTime? CheckinTime { get; init; }
}
