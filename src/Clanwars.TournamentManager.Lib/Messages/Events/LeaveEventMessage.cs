using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Events;

public record LeaveEventMessage
{
    [Required]
    public int UserId { get; set; }
}
