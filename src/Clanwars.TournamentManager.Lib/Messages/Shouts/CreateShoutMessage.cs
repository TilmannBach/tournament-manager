using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Shouts;

public record CreateShoutMessage
{
    [Required]
    [StringLength(255, MinimumLength = 1)]
    public string Text { get; set; } = null!;
}
