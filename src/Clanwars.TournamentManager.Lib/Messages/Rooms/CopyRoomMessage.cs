using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Rooms;

public record CopyRoomMessage
{
    [Required]
    [MinLength(5)]
    public string Name { get; set; } = null!;

    [Required]
    public int TargetEventId { get; set; }
}
