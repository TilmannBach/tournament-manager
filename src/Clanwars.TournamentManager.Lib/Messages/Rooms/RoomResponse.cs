using Clanwars.TournamentManager.Lib.Messages.Events;

namespace Clanwars.TournamentManager.Lib.Messages.Rooms;

public record RoomResponse
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int SizeX { get; set; }

    public int SizeY { get; set; }

    public int CountSeats { get; set; }

    public int EventId { get; set; }

    public EventResponse Event { get; set; } = null!;

    public IList<GridSquareResponse> GridSquares { get; set; } = new List<GridSquareResponse>();
}
