using Clanwars.TournamentManager.Lib.Enums.Rooms;
using Clanwars.TournamentManager.Lib.Messages.User;

namespace Clanwars.TournamentManager.Lib.Messages.Rooms;

public class GridSquareResponse
{
    public int Id { get; set; }

    public int X { get; set; }

    public int Y { get; set; }

    public GridSquareType Type { get; set; }

    public GridSquareRotation Rotation { get; set; }

    public int RoomId { get; set; }

    public UserResponse? User { get; set; }
}
