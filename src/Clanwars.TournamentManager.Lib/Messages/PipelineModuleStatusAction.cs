namespace Clanwars.TournamentManager.Lib.Messages;

public enum PipelineModuleStatusAction
{
    Enable,
    Disable
}
