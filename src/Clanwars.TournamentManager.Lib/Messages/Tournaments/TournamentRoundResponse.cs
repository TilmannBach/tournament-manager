using Clanwars.TournamentManager.Lib.Enums.Tournaments;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public class TournamentRoundResponse
{
    public int Id { get; set; }
    public TournamentRoundType? Type { get; set; }

    public double Order { get; set; }
    public IList<TournamentMatchResponse> Matches { get; set; } = null!;
}
