using System.ComponentModel.DataAnnotations;

namespace Clanwars.TournamentManager.Lib.Messages.Tournaments;

public class TournamentMatchScoreResponse
{
    public int Id { get; set; }

    public TournamentTeamResponse? TournamentTeam { get; set; } = null!;

    [Range(0, 1024)]
    public int? Score { get; set; }
}
