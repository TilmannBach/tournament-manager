using System.Text.Json.Serialization;

namespace Clanwars.TournamentManager.Lib.Enums.Tournaments;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum TournamentState
{
    Planning = 0, // when created
    OpenForRegistration = 1, // updateable
    Running = 2, // when start()'ed
    Finished = 3, // when last game is played
    Closed = 4, // updateable
}
