using System.Text.Json.Serialization;

namespace Clanwars.TournamentManager.Lib.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum ImageType
{
    UserImage = 0,
    ClanImage = 1,
    TournamentImage = 2,
    TournamentTeamImage = 3,
    Beamer = 4,
}
