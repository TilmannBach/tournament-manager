using System.Text.Json.Serialization;

namespace Clanwars.TournamentManager.Lib.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum MatchRank
{
    Winner = 1,
    Loser = 2,
}
