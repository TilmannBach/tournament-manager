using System.Text.Json.Serialization;

namespace Clanwars.TournamentManager.Lib.Enums.Rooms;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum SeatState
{
    Empty = 0,
    Locked = 1,
    Occupied = 2,
}
