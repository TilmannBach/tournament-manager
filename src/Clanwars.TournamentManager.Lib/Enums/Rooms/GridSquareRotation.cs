using System.Text.Json.Serialization;

namespace Clanwars.TournamentManager.Lib.Enums.Rooms;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum GridSquareRotation
{
    Degree0 = 0,
    Degree45 = 45,
    Degree90 = 90,
    Degree135 = 135,
    Degree180 = 180,
    Degree225 = 225,
    Degree270 = 270,
    Degree315 = 315,
}
