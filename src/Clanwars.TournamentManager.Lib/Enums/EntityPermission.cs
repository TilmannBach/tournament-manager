using System.Text.Json.Serialization;

namespace Clanwars.TournamentManager.Lib.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum EntityPermission
{
    Owner = 0,
    Member = 1,
    Pending = 2
}
