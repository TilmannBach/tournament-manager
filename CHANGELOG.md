## [3.0.11](https://gitlab.com/clanwars/tournament-manager/compare/3.0.10...3.0.11) (2024-12-08)


### Bug Fixes

* **deps:** update dotnet monorepo to 8.0.11 ([fd8f249](https://gitlab.com/clanwars/tournament-manager/commit/fd8f249cb53eb5dc1c518987eafef736ae89fa7d))

## [3.0.10](https://gitlab.com/clanwars/tournament-manager/compare/3.0.9...3.0.10) (2024-10-19)


### Bug Fixes

* **osc:** fix wrong message type for finished matches 2 ([e67dce2](https://gitlab.com/clanwars/tournament-manager/commit/e67dce22641ecea424c10f98d549793c3aaa7ea2))

## [3.0.9](https://gitlab.com/clanwars/tournament-manager/compare/3.0.8...3.0.9) (2024-10-19)


### Bug Fixes

* **osc:** fix wrong message type for finished matches ([4b00fda](https://gitlab.com/clanwars/tournament-manager/commit/4b00fda8edb8552c77e853e4d9fababb965bd12f))

## [3.0.8](https://gitlab.com/clanwars/tournament-manager/compare/3.0.7...3.0.8) (2024-10-18)


### Bug Fixes

* **bar-portfolio:** update to new pizza catalog ([56b2ad8](https://gitlab.com/clanwars/tournament-manager/commit/56b2ad8760f2945cc042da28275296ad30ed4504))
* **beamer:** automatically enable publish ([7e7b563](https://gitlab.com/clanwars/tournament-manager/commit/7e7b563ddfefae3c668a6368ecb81606dec17255))

## [3.0.7](https://gitlab.com/clanwars/tournament-manager/compare/3.0.6...3.0.7) (2024-10-18)


### Bug Fixes

* **feedback:** Order feedback items by resolved ([ef9cf7f](https://gitlab.com/clanwars/tournament-manager/commit/ef9cf7ff8a28a86cc05204fef7c896cfa4ba3d2c))

## [3.0.6](https://gitlab.com/clanwars/tournament-manager/compare/3.0.5...3.0.6) (2024-10-18)


### Bug Fixes

* **admission:** allow search with nickname ([0f13a57](https://gitlab.com/clanwars/tournament-manager/commit/0f13a5754413ab205a33939d7fda09e2e6cba573)), closes [#181](https://gitlab.com/clanwars/tournament-manager/issues/181)

## [3.0.5](https://gitlab.com/clanwars/tournament-manager/compare/3.0.4...3.0.5) (2024-10-18)


### Bug Fixes

* **beamerscreen:** REVERT: Allow beamerscreen for unauthorized ([f4131c4](https://gitlab.com/clanwars/tournament-manager/commit/f4131c4f258d1edd84905b912051f59c16099604))

## [3.0.4](https://gitlab.com/clanwars/tournament-manager/compare/3.0.3...3.0.4) (2024-10-18)


### Bug Fixes

* **beamerscreen:** Allow beamerscreen for unauthorized ([a094519](https://gitlab.com/clanwars/tournament-manager/commit/a094519de85d226069dae3b7a1eaf94ff2ee04a7))
* **beamerscreen:** Only reload window on new version ([847d2cd](https://gitlab.com/clanwars/tournament-manager/commit/847d2cd5ecb81a15766288528ec3b2507b7dc316))

## [3.0.3](https://gitlab.com/clanwars/tournament-manager/compare/3.0.2...3.0.3) (2024-10-18)


### Bug Fixes

* **beamercontentprovider:** Refactor GetRandomShoutFromDbAsync to return nullable ([688c626](https://gitlab.com/clanwars/tournament-manager/commit/688c62620c0e4898a1d91244407948a7f571ff46))

## [3.0.2](https://gitlab.com/clanwars/tournament-manager/compare/3.0.1...3.0.2) (2024-10-18)


### Bug Fixes

* **beamer:** auto reload window if beamer layout is shown ([eeeba45](https://gitlab.com/clanwars/tournament-manager/commit/eeeba45c37c65b48f5580d49c157a7d387a39404))
* **beamer:** fixes initial setting of slide timeouts ([ac2070c](https://gitlab.com/clanwars/tournament-manager/commit/ac2070ce1881d0dace3d068973caf1fd7c09bc65))

## [3.0.1](https://gitlab.com/clanwars/tournament-manager/compare/3.0.0...3.0.1) (2024-10-18)


### Bug Fixes

* **shouts:** only show shouts from running event ([5e83eed](https://gitlab.com/clanwars/tournament-manager/commit/5e83eed12d8dfe1d620523adebcfbe7f853a8a9e))

# [3.0.0](https://gitlab.com/clanwars/tournament-manager/compare/2.14.0...3.0.0) (2024-10-17)


### Features

* **release:** major version bump for LAN 2024 🥳 ([4204602](https://gitlab.com/clanwars/tournament-manager/commit/42046024ef71950ed70a39897df067ce01926cc5))


### BREAKING CHANGES

* **release:** no breaking change!

# [2.14.0](https://gitlab.com/clanwars/tournament-manager/compare/2.13.1...2.14.0) (2024-10-17)


### Features

* **admin:** Reimport users to identity provider and reset password at admission ([6c6a1bc](https://gitlab.com/clanwars/tournament-manager/commit/6c6a1bcac8a1d7326f9a073858123a98fbced4e0)), closes [#180](https://gitlab.com/clanwars/tournament-manager/issues/180)

## [2.13.1](https://gitlab.com/clanwars/tournament-manager/compare/2.13.0...2.13.1) (2024-10-17)


### Bug Fixes

* **beamer:** only show news in the time range of the current event on random access ([53440d8](https://gitlab.com/clanwars/tournament-manager/commit/53440d89e5408a6fd5781d30dcf61f16c5d8401e)), closes [#179](https://gitlab.com/clanwars/tournament-manager/issues/179)

# [2.13.0](https://gitlab.com/clanwars/tournament-manager/compare/2.12.1...2.13.0) (2024-10-17)


### Features

* **beamer:** adds osc commands for special beamer events ([cccdbf0](https://gitlab.com/clanwars/tournament-manager/commit/cccdbf0fce420829c62d56b7c4d1461e7ea77999)), closes [#174](https://gitlab.com/clanwars/tournament-manager/issues/174)

## [2.12.1](https://gitlab.com/clanwars/tournament-manager/compare/2.12.0...2.12.1) (2024-10-16)


### Bug Fixes

* **beamer:** renamed hub backend to not collide with /beamer frontend view ([97aa812](https://gitlab.com/clanwars/tournament-manager/commit/97aa8122008d1a9fa8a092e613c861fad2e12423))

# [2.12.0](https://gitlab.com/clanwars/tournament-manager/compare/2.11.3...2.12.0) (2024-10-16)


### Features

* **feeback:** adds possibility to mark feedback as resolved ([c2fc02f](https://gitlab.com/clanwars/tournament-manager/commit/c2fc02fd72d544f6c6c504bdac742ea9a0856f45)), closes [#178](https://gitlab.com/clanwars/tournament-manager/issues/178)

## [2.11.3](https://gitlab.com/clanwars/tournament-manager/compare/2.11.2...2.11.3) (2024-10-15)


### Bug Fixes

* **images:** add show on stream option ([aef20be](https://gitlab.com/clanwars/tournament-manager/commit/aef20be40499b6d095edb51efd2303aeb17ff411))

## [2.11.2](https://gitlab.com/clanwars/tournament-manager/compare/2.11.1...2.11.2) (2024-10-15)


### Bug Fixes

* **seats:** fixes a bug with filtering relevant users for seats ([937491b](https://gitlab.com/clanwars/tournament-manager/commit/937491bf9aefd1a0e446be53ae5bc43f08eaa90e))

## [2.11.1](https://gitlab.com/clanwars/tournament-manager/compare/2.11.0...2.11.1) (2024-10-14)


### Bug Fixes

* **cq:** fixes a lot of code quality issues including MudBlazor breaking changes from https://github.com/MudBlazor/MudBlazor/issues/8447 ([fffb5dd](https://gitlab.com/clanwars/tournament-manager/commit/fffb5ddff16cc3dcb1a17c39d6d2aff7cc1c02b6))

# [2.11.0](https://gitlab.com/clanwars/tournament-manager/compare/2.10.1...2.11.0) (2024-10-09)


### Features

* **beamer-view:** adds slide for match result on finished match ([90e7f56](https://gitlab.com/clanwars/tournament-manager/commit/90e7f561d63b7beb9df4893c8994ae3daa9beaad)), closes [#173](https://gitlab.com/clanwars/tournament-manager/issues/173)

## [2.10.1](https://gitlab.com/clanwars/tournament-manager/compare/2.10.0...2.10.1) (2024-10-06)


### Bug Fixes

* **images:** publish images directly and handle 0 value as infinite timeout ([fab2011](https://gitlab.com/clanwars/tournament-manager/commit/fab2011d49fffd55c6a938153973595f9035d9e6)), closes [#177](https://gitlab.com/clanwars/tournament-manager/issues/177)

# [2.10.0](https://gitlab.com/clanwars/tournament-manager/compare/2.9.1...2.10.0) (2024-09-26)


### Bug Fixes

* **beamer:** fixes db query for random beamer pictures ([5bb5b90](https://gitlab.com/clanwars/tournament-manager/commit/5bb5b90f1a4a0569bdf3b8a10dda61cf04ed8cf9))


### Features

* **images:** add image view ([e77f9c1](https://gitlab.com/clanwars/tournament-manager/commit/e77f9c1d380ab76ab3afea840b96fb1531e96f63)), closes [#169](https://gitlab.com/clanwars/tournament-manager/issues/169) [#175](https://gitlab.com/clanwars/tournament-manager/issues/175)

## [2.9.1](https://gitlab.com/clanwars/tournament-manager/compare/2.9.0...2.9.1) (2024-09-22)


### Bug Fixes

* **beamer:** adds beamer view for finished matches ([c56c84a](https://gitlab.com/clanwars/tournament-manager/commit/c56c84a03c29c9d80d8d3c6ecd9df8eefeaf01ab)), closes [#172](https://gitlab.com/clanwars/tournament-manager/issues/172)

# [2.9.0](https://gitlab.com/clanwars/tournament-manager/compare/2.8.3...2.9.0) (2024-09-09)


### Bug Fixes

* **beamer:** fix tournamentMatchId result ([00c27a4](https://gitlab.com/clanwars/tournament-manager/commit/00c27a4436ab65a05b9c1cc88e01b70c94ccc0e9))


### Features

* **beamer:** adds tournament next matches view ([10f887c](https://gitlab.com/clanwars/tournament-manager/commit/10f887c0ff92817c9babb19f811e10f0719b5f4e)), closes [#171](https://gitlab.com/clanwars/tournament-manager/issues/171)

## [2.8.3](https://gitlab.com/clanwars/tournament-manager/compare/2.8.2...2.8.3) (2024-09-09)


### Bug Fixes

* **entities:** remove lanpartscreenid from tables ([d316330](https://gitlab.com/clanwars/tournament-manager/commit/d316330510b79315b43810d2f834781c074cea02))

## [2.8.2](https://gitlab.com/clanwars/tournament-manager/compare/2.8.1...2.8.2) (2024-09-07)


### Bug Fixes

* **deps:** Update package versions ([b3928ef](https://gitlab.com/clanwars/tournament-manager/commit/b3928ef510a445bc59acea82394ee717e167ce5e))

## [2.8.1](https://gitlab.com/clanwars/tournament-manager/compare/2.8.0...2.8.1) (2024-09-06)


### Bug Fixes

* **tournaments:** fixes multiple bugs with team list (joining/leaving teams) ([fa0b32b](https://gitlab.com/clanwars/tournament-manager/commit/fa0b32bff45e57c2d8e4da99728c402c260b6837)), closes [#168](https://gitlab.com/clanwars/tournament-manager/issues/168)

# [2.8.0](https://gitlab.com/clanwars/tournament-manager/compare/2.7.0...2.8.0) (2024-09-04)


### Features

* **deployment:** show alert when new version of TM is deployed and user should reload the website ([8965aa8](https://gitlab.com/clanwars/tournament-manager/commit/8965aa85f64248551edbe869a8070e84166d493b)), closes [#159](https://gitlab.com/clanwars/tournament-manager/issues/159)

# [2.7.0](https://gitlab.com/clanwars/tournament-manager/compare/2.6.4...2.7.0) (2024-09-03)


### Features

* **beamer:** adds tournament overview slide ([2814a30](https://gitlab.com/clanwars/tournament-manager/commit/2814a30725b58473988d65a7908825bdcd497c75)), closes [#165](https://gitlab.com/clanwars/tournament-manager/issues/165)

## [2.6.4](https://gitlab.com/clanwars/tournament-manager/compare/2.6.3...2.6.4) (2024-09-02)


### Bug Fixes

* **frontend:** downgrade to 7.6.0 to resolve markdown issues ([a95c627](https://gitlab.com/clanwars/tournament-manager/commit/a95c6275d3d5c3e06dca76cd1c9988c670653c5e))

## [2.6.3](https://gitlab.com/clanwars/tournament-manager/compare/2.6.2...2.6.3) (2024-09-02)


### Bug Fixes

* **frontend:** remove mudblazor.extensions lib ([f7be6c3](https://gitlab.com/clanwars/tournament-manager/commit/f7be6c3901ceca81e3bd6caaa230db7d652986bc))

## [2.6.2](https://gitlab.com/clanwars/tournament-manager/compare/2.6.1...2.6.2) (2024-09-02)


### Bug Fixes

* **frontend:** downgrade mudblazor.extensions to make cropper work ([4bead44](https://gitlab.com/clanwars/tournament-manager/commit/4bead4482fe705fd4b2c88b0cbfb5a65f493496e))

## [2.6.1](https://gitlab.com/clanwars/tournament-manager/compare/2.6.0...2.6.1) (2024-08-31)


### Bug Fixes

* **deps:** updates all dotnet dependencies ([9240083](https://gitlab.com/clanwars/tournament-manager/commit/924008342d6a4f0aa1b945c8a827c6616551306b))

# [2.6.0](https://gitlab.com/clanwars/tournament-manager/compare/2.5.2...2.6.0) (2024-08-31)


### Features

* **beamer-view:** adds slide transitions and implementation for welcome messages ([16b6a04](https://gitlab.com/clanwars/tournament-manager/commit/16b6a04532200e91cb371198d7c88b1aa7f709c6)), closes [#167](https://gitlab.com/clanwars/tournament-manager/issues/167)

## [2.5.2](https://gitlab.com/clanwars/tournament-manager/compare/2.5.1...2.5.2) (2024-08-15)


### Bug Fixes

* **dotnet:** update deps ([ff8aa35](https://gitlab.com/clanwars/tournament-manager/commit/ff8aa35136f47a2a1224f1100bca163bc86d2c31))

## [2.5.1](https://gitlab.com/clanwars/tournament-manager/compare/2.5.0...2.5.1) (2024-08-07)


### Bug Fixes

* **dep:** replace keycloak local fork with fork of original project ([65f4dd4](https://gitlab.com/clanwars/tournament-manager/commit/65f4dd494f3a4bd750afc4b7a0f0dd27fb80eb53)), closes [#161](https://gitlab.com/clanwars/tournament-manager/issues/161)

# [2.5.0](https://gitlab.com/clanwars/tournament-manager/compare/2.4.3...2.5.0) (2024-08-07)


### Features

* **beamerscreen:** include lanparty screen functionality ([b64ebe5](https://gitlab.com/clanwars/tournament-manager/commit/b64ebe587c8efb07e986d0736bd5ed796d58bcfa))

## [2.4.3](https://gitlab.com/clanwars/tournament-manager/compare/2.4.2...2.4.3) (2024-06-03)


### Bug Fixes

* **frontend:** dont allow non manager users to access the admission page ([ef25381](https://gitlab.com/clanwars/tournament-manager/commit/ef25381a8eb346841dcd2f11dc51baa7e1e97513)), closes [#155](https://gitlab.com/clanwars/tournament-manager/issues/155)

## [2.4.2](https://gitlab.com/clanwars/tournament-manager/compare/2.4.1...2.4.2) (2024-06-03)


### Bug Fixes

* **user-avatar:** if user has no firstname and lastname fallback to "??" as avatar default ([cfeae84](https://gitlab.com/clanwars/tournament-manager/commit/cfeae84ec8c885188619280119ab224222eafd58)), closes [#163](https://gitlab.com/clanwars/tournament-manager/issues/163)

## [2.4.1](https://gitlab.com/clanwars/tournament-manager/compare/2.4.0...2.4.1) (2024-06-01)


### Bug Fixes

* **build:** fixes some build output issues ([0002c1f](https://gitlab.com/clanwars/tournament-manager/commit/0002c1f0603ad440424745cb19a09b10697a7de4))

# [2.4.0](https://gitlab.com/clanwars/tournament-manager/compare/2.3.0...2.4.0) (2024-05-30)


### Features

* **dotnet:** upgrade to version 8 ([bb2a02e](https://gitlab.com/clanwars/tournament-manager/commit/bb2a02ea4347f8f30c1d1436c35dc493869a08e6))

# [2.3.0](https://gitlab.com/clanwars/tournament-manager/compare/2.2.1...2.3.0) (2023-10-22)


### Features

* **tournaments:** show next matches on running tournaments and leaderboard on finished tournaments ([e0e2240](https://gitlab.com/clanwars/tournament-manager/commit/e0e2240633a38d6b31862292060d97886d809752))

## [2.2.1](https://gitlab.com/clanwars/tournament-manager/compare/2.2.0...2.2.1) (2023-10-21)


### Bug Fixes

* **news:** migrates to different markdown viewer for better presentation results of MD news entries ([d3ffe03](https://gitlab.com/clanwars/tournament-manager/commit/d3ffe034b578f2a2becc1df9020e9d654d2252ff))

# [2.2.0](https://gitlab.com/clanwars/tournament-manager/compare/2.1.0...2.2.0) (2023-10-21)


### Features

* **tournaments:** add goto link from tournament tree view to find team members in rooms view ([fd0bd3a](https://gitlab.com/clanwars/tournament-manager/commit/fd0bd3aab97af6dd7961f19570d4f2a3367c4882))

# [2.1.0](https://gitlab.com/clanwars/tournament-manager/compare/2.0.3...2.1.0) (2023-10-20)


### Features

* **pizza:** adding pizzas to the portfolio of the amazing bar lol ([ab8d922](https://gitlab.com/clanwars/tournament-manager/commit/ab8d922d117be6d4425baae4464c5d86e9abe788))

## [2.0.3](https://gitlab.com/clanwars/tournament-manager/compare/2.0.2...2.0.3) (2023-10-20)


### Bug Fixes

* **bar-goods:** update avaiable goods for the bar ([9105def](https://gitlab.com/clanwars/tournament-manager/commit/9105deff21e811411a56b4bef9a988802a6c76b4))

## [2.0.2](https://gitlab.com/clanwars/tournament-manager/compare/2.0.1...2.0.2) (2023-10-19)


### Bug Fixes

* **health-checks:** adds k8s health check endpoints to enhance rolling releases ([bd880f7](https://gitlab.com/clanwars/tournament-manager/commit/bd880f712dc99c0b4e7877b22620341a8740bc37))

## [2.0.1](https://gitlab.com/clanwars/tournament-manager/compare/2.0.0...2.0.1) (2023-10-19)


### Bug Fixes

* **avatars:** fixes avatars with new MudBlazor version ([5484c42](https://gitlab.com/clanwars/tournament-manager/commit/5484c429ee11b3efbf3b628ff2e5296d3dd391f3))

# [2.0.0](https://gitlab.com/clanwars/tournament-manager/compare/1.9.3...2.0.0) (2023-10-18)


### Features

* **rooms:** adds possibility to clone rooms from previous events ([0979345](https://gitlab.com/clanwars/tournament-manager/commit/0979345fa967754ddc11f82bceee9c1a9ddfa39e))


### BREAKING CHANGES

* **rooms:** ATTENTION - this release DOES NOT container breaking changes, but I wanted to release a version 2.0.0 für 2nd year of usage of TM

## [1.9.3](https://gitlab.com/clanwars/tournament-manager/compare/1.9.2...1.9.3) (2023-10-16)


### Bug Fixes

* **deps:** updates all nuget dependencies ([342dd8c](https://gitlab.com/clanwars/tournament-manager/commit/342dd8cab459cbd29a4c41fc33a2f65be10d85b9))

## [1.9.2](https://gitlab.com/clanwars/tournament-manager/compare/1.9.1...1.9.2) (2023-03-19)


### Bug Fixes

* **dotnet:** upgrade to version 7 ([99dd9fa](https://gitlab.com/clanwars/tournament-manager/commit/99dd9fafe4e2267cc9b6bf4d42573100491d239e)), closes [#160](https://gitlab.com/clanwars/tournament-manager/issues/160)

## [1.9.1](https://gitlab.com/clanwars/tournament-manager/compare/1.9.0...1.9.1) (2022-10-23)


### Bug Fixes

* **lib:** adds source link features to library nuget package ([f9e431c](https://gitlab.com/clanwars/tournament-manager/commit/f9e431c8ad688b26dd6cea3ae66cfd2b5dd73888))

# [1.9.0](https://gitlab.com/clanwars/tournament-manager/compare/1.8.1...1.9.0) (2022-10-08)


### Features

* **rooms:** adds user search with highlighting on seat plan ([e1d317e](https://gitlab.com/clanwars/tournament-manager/commit/e1d317e56b04492a3b33b69a53f932217ed59729)), closes [#64](https://gitlab.com/clanwars/tournament-manager/issues/64)

## [1.8.1](https://gitlab.com/clanwars/tournament-manager/compare/1.8.0...1.8.1) (2022-10-08)


### Bug Fixes

* **shoutbox:** fixes shoutbox query limiter with german dates ([3ff4364](https://gitlab.com/clanwars/tournament-manager/commit/3ff4364ee37dc5ab9e594fa7644fa692be057e55))

# [1.8.0](https://gitlab.com/clanwars/tournament-manager/compare/1.7.5...1.8.0) (2022-10-08)


### Features

* **payments:** show users transaction in personal payments menu ([b926a97](https://gitlab.com/clanwars/tournament-manager/commit/b926a97551be54b50f844fbdbe893810a509b326)), closes [#157](https://gitlab.com/clanwars/tournament-manager/issues/157)

## [1.7.5](https://gitlab.com/clanwars/tournament-manager/compare/1.7.4...1.7.5) (2022-10-08)


### Bug Fixes

* **navigation:** divide user and admin points ([3a6c67c](https://gitlab.com/clanwars/tournament-manager/commit/3a6c67cc8344a9b81d7ebb249a8e839b33d78e36))

## [1.7.4](https://gitlab.com/clanwars/tournament-manager/compare/1.7.3...1.7.4) (2022-10-08)


### Bug Fixes

* **performance:** add signalr event propagation debounce ([738005c](https://gitlab.com/clanwars/tournament-manager/commit/738005cceb9963f27a6e9021cf4ed30a08a44a34))

## [1.7.3](https://gitlab.com/clanwars/tournament-manager/compare/1.7.2...1.7.3) (2022-10-08)


### Bug Fixes

* **lpc:** fixes scoring for points ([3700b60](https://gitlab.com/clanwars/tournament-manager/commit/3700b60bf8a0f0a96d2aebb3e19cde37e9c89cad)), closes [#148](https://gitlab.com/clanwars/tournament-manager/issues/148)

## [1.7.2](https://gitlab.com/clanwars/tournament-manager/compare/1.7.1...1.7.2) (2022-10-08)


### Bug Fixes

* **perf:** tries to fixe more performance issues by removing JOINs ([511cd1c](https://gitlab.com/clanwars/tournament-manager/commit/511cd1cb835421e378637c86ecb725d79918b096))

## [1.7.1](https://gitlab.com/clanwars/tournament-manager/compare/1.7.0...1.7.1) (2022-10-08)


### Bug Fixes

* **tournaments:** first try of fixing performance issues ([5b7badd](https://gitlab.com/clanwars/tournament-manager/commit/5b7badd60e32ced0b9572ed12debad2bae3743c1))

# [1.7.0](https://gitlab.com/clanwars/tournament-manager/compare/1.6.6...1.7.0) (2022-10-07)


### Features

* **feedback:** Add internal feedback form ([cb20f2c](https://gitlab.com/clanwars/tournament-manager/commit/cb20f2cc459ba3aa366dd1a7d800cdea367ead39))

## [1.6.6](https://gitlab.com/clanwars/tournament-manager/compare/1.6.5...1.6.6) (2022-10-07)


### Bug Fixes

* **general:** Improve Blazor error message ([996efb7](https://gitlab.com/clanwars/tournament-manager/commit/996efb70b24670804d33641d75bdedf033f55ea7))
* **user:** handle badrequests when updating nickname ([6fe5cb3](https://gitlab.com/clanwars/tournament-manager/commit/6fe5cb305f3e9046494afd466d9e0736731656b7))

## [1.6.5](https://gitlab.com/clanwars/tournament-manager/compare/1.6.4...1.6.5) (2022-10-07)


### Bug Fixes

* **bar:** adds pizza list ([143e775](https://gitlab.com/clanwars/tournament-manager/commit/143e77554743a6cf80accb1717c656040c0e4d04))

## [1.6.4](https://gitlab.com/clanwars/tournament-manager/compare/1.6.3...1.6.4) (2022-10-07)


### Bug Fixes

* **rooms:** fixes unsubsription of room changes with redirects ([6d0c5ec](https://gitlab.com/clanwars/tournament-manager/commit/6d0c5ec04d1211ca0aa4c11e90e87cb0cf3b1c96))

## [1.6.3](https://gitlab.com/clanwars/tournament-manager/compare/1.6.2...1.6.3) (2022-10-07)


### Bug Fixes

* **room:** Add 45 degree rotation ([bc28797](https://gitlab.com/clanwars/tournament-manager/commit/bc287974c0ec92ebe50c2d439ffc2283226a8d33)), closes [#153](https://gitlab.com/clanwars/tournament-manager/issues/153)

## [1.6.2](https://gitlab.com/clanwars/tournament-manager/compare/1.6.1...1.6.2) (2022-10-07)


### Bug Fixes

* **fix:** several small fixes ([48b2aa4](https://gitlab.com/clanwars/tournament-manager/commit/48b2aa4ec6484716bdead1f5a27350c6b4b0f1ca))

## [1.6.1](https://gitlab.com/clanwars/tournament-manager/compare/1.6.0...1.6.1) (2022-10-07)


### Bug Fixes

* **user:** trim user data ([871f6c3](https://gitlab.com/clanwars/tournament-manager/commit/871f6c359fdf01f50d30495f5380a74531f0c9d1))

# [1.6.0](https://gitlab.com/clanwars/tournament-manager/compare/1.5.6...1.6.0) (2022-10-07)


### Features

* **bar:** Add simple menu ([7629e38](https://gitlab.com/clanwars/tournament-manager/commit/7629e38b9b3b2b6527b95f4b2ce6e8c3f15e4bcf)), closes [#152](https://gitlab.com/clanwars/tournament-manager/issues/152)

## [1.5.6](https://gitlab.com/clanwars/tournament-manager/compare/1.5.5...1.5.6) (2022-10-07)


### Bug Fixes

* **feedback:** Load Feedback with delay ([2295344](https://gitlab.com/clanwars/tournament-manager/commit/2295344138ef3f322a6b5df960788c4e1563451a)), closes [#150](https://gitlab.com/clanwars/tournament-manager/issues/150)

## [1.5.5](https://gitlab.com/clanwars/tournament-manager/compare/1.5.4...1.5.5) (2022-10-07)


### Bug Fixes

* **kerio:** add link to firewall login ([4910d5c](https://gitlab.com/clanwars/tournament-manager/commit/4910d5c717980a7e64f35013d9518c7f33446230))

## [1.5.4](https://gitlab.com/clanwars/tournament-manager/compare/1.5.3...1.5.4) (2022-10-06)


### Bug Fixes

* **tournament-tree:** highlight active and own matches ([b752731](https://gitlab.com/clanwars/tournament-manager/commit/b75273167f026ae47c8b50d360506a951e9a8bd6)), closes [#142](https://gitlab.com/clanwars/tournament-manager/issues/142)

## [1.5.3](https://gitlab.com/clanwars/tournament-manager/compare/1.5.2...1.5.3) (2022-10-06)


### Bug Fixes

* **lps:** add send welcome messages ([d03dfb3](https://gitlab.com/clanwars/tournament-manager/commit/d03dfb3b2a084b4edadee6f1e7be96616b78d660)), closes [#146](https://gitlab.com/clanwars/tournament-manager/issues/146)

## [1.5.2](https://gitlab.com/clanwars/tournament-manager/compare/1.5.1...1.5.2) (2022-10-06)


### Bug Fixes

* **lps:** adds shouts integration to LPS ([487f5c6](https://gitlab.com/clanwars/tournament-manager/commit/487f5c6c1150b0b8d8d47333deeba71337dc08a6)), closes [#145](https://gitlab.com/clanwars/tournament-manager/issues/145)

## [1.5.1](https://gitlab.com/clanwars/tournament-manager/compare/1.5.0...1.5.1) (2022-10-06)


### Bug Fixes

* **tournaments:** fixes crash when trying to create a team with an already existing team name ([fe241f5](https://gitlab.com/clanwars/tournament-manager/commit/fe241f5c606ef1f94269f4386c39e3a7da197c61)), closes [#144](https://gitlab.com/clanwars/tournament-manager/issues/144)

# [1.5.0](https://gitlab.com/clanwars/tournament-manager/compare/1.4.1...1.5.0) (2022-10-05)


### Features

* **lanpartyscreen:** add integration of news and tournament creation/update ([128659b](https://gitlab.com/clanwars/tournament-manager/commit/128659b8c29aa2b0ede738d01bd1cd57889c002b))

## [1.4.1](https://gitlab.com/clanwars/tournament-manager/compare/1.4.0...1.4.1) (2022-10-05)


### Bug Fixes

* **tournaments:** dont show close button on finished tournaments ([21125b6](https://gitlab.com/clanwars/tournament-manager/commit/21125b66122a96d44109dc7d54fbce4483fe96fd)), closes [#139](https://gitlab.com/clanwars/tournament-manager/issues/139)

# [1.4.0](https://gitlab.com/clanwars/tournament-manager/compare/1.3.13...1.4.0) (2022-10-05)


### Features

* **shoutbox:** adds shoutbox feature ([87e1288](https://gitlab.com/clanwars/tournament-manager/commit/87e1288d0a2cb5c228fe46b645ae9ffaca7a0dc0)), closes [#121](https://gitlab.com/clanwars/tournament-manager/issues/121)

## [1.3.13](https://gitlab.com/clanwars/tournament-manager/compare/1.3.12...1.3.13) (2022-10-04)


### Bug Fixes

* **tournaments:** show toasts on updating tournament states ([1885acf](https://gitlab.com/clanwars/tournament-manager/commit/1885acf5bd6b8a277a4397e2ed61b731b35e0c11)), closes [#136](https://gitlab.com/clanwars/tournament-manager/issues/136)

## [1.3.12](https://gitlab.com/clanwars/tournament-manager/compare/1.3.11...1.3.12) (2022-10-04)


### Bug Fixes

* **tournaments:** tournament moderators are able to score any scorable matches now ([3b58a2c](https://gitlab.com/clanwars/tournament-manager/commit/3b58a2cf5e3eaddcb738267ccafc2a9eae6a02c4)), closes [#127](https://gitlab.com/clanwars/tournament-manager/issues/127)

## [1.3.11](https://gitlab.com/clanwars/tournament-manager/compare/1.3.10...1.3.11) (2022-10-04)


### Bug Fixes

* **user-store:** search for email instead of username to prevent an error with searching kerio users ([051990a](https://gitlab.com/clanwars/tournament-manager/commit/051990a0efff7c6404c77b528ed1a12ab8672327))

## [1.3.10](https://gitlab.com/clanwars/tournament-manager/compare/1.3.9...1.3.10) (2022-10-03)


### Bug Fixes

* **admission:** fixes user store interaction after switching to email address as username ([c1364d4](https://gitlab.com/clanwars/tournament-manager/commit/c1364d4692356914c947c95943f1672d28d22768))

## [1.3.9](https://gitlab.com/clanwars/tournament-manager/compare/1.3.8...1.3.9) (2022-10-03)


### Bug Fixes

* **admission:** fixes creation of new users in keycloak with kerio user fedearation provider ([3735e42](https://gitlab.com/clanwars/tournament-manager/commit/3735e42296706a5a135b802ed5e4bdffc5bc2bed))

## [1.3.8](https://gitlab.com/clanwars/tournament-manager/compare/1.3.7...1.3.8) (2022-10-03)


### Bug Fixes

* **admission:** create users with email as username and default password ([1091814](https://gitlab.com/clanwars/tournament-manager/commit/1091814e0ff1691126851cfc8b641b53f3162bf0)), closes [#118](https://gitlab.com/clanwars/tournament-manager/issues/118)
* **admission:** fixes bug in template file ([7630e03](https://gitlab.com/clanwars/tournament-manager/commit/7630e03bbb4836359d06a25ac9fdc8a51928d6ac))

## [1.3.7](https://gitlab.com/clanwars/tournament-manager/compare/1.3.6...1.3.7) (2022-10-03)


### Bug Fixes

* **ui:** removes "loading" states of pages to prevent pages from flickering ([d9cc870](https://gitlab.com/clanwars/tournament-manager/commit/d9cc870239e69d11ba6f694079a76c296742a724))

## [1.3.6](https://gitlab.com/clanwars/tournament-manager/compare/1.3.5...1.3.6) (2022-10-03)


### Bug Fixes

* **clans:** adds created and updated timestamps to clans + adds checkin times to event registrations ([d85761d](https://gitlab.com/clanwars/tournament-manager/commit/d85761d28f825ebc66c28c6dec3898d6574bc01b)), closes [#125](https://gitlab.com/clanwars/tournament-manager/issues/125)

## [1.3.5](https://gitlab.com/clanwars/tournament-manager/compare/1.3.4...1.3.5) (2022-10-03)


### Bug Fixes

* **news:** adds author to news ([e9d51eb](https://gitlab.com/clanwars/tournament-manager/commit/e9d51eb2db6fd191d02e25a5f7371ae6f6f6b938)), closes [#132](https://gitlab.com/clanwars/tournament-manager/issues/132)

## [1.3.4](https://gitlab.com/clanwars/tournament-manager/compare/1.3.3...1.3.4) (2022-09-30)


### Bug Fixes

* **tournaments:** remove background of tournament tree ([bc0348f](https://gitlab.com/clanwars/tournament-manager/commit/bc0348f08bfc97cca6e278e568813461f461470b))

## [1.3.3](https://gitlab.com/clanwars/tournament-manager/compare/1.3.2...1.3.3) (2022-09-30)


### Bug Fixes

* **payments:** show information on payments page if user has no account yet ([1bd25b6](https://gitlab.com/clanwars/tournament-manager/commit/1bd25b6f810fdfb18c04186d35f278349e6acf3d)), closes [#130](https://gitlab.com/clanwars/tournament-manager/issues/130)

## [1.3.2](https://gitlab.com/clanwars/tournament-manager/compare/1.3.1...1.3.2) (2022-09-30)


### Bug Fixes

* **tournaments:** prevent an elimination tournament to be started with less than 3 teams ([bc67712](https://gitlab.com/clanwars/tournament-manager/commit/bc677124df04254bf8dddf779eb42f8104525190)), closes [#128](https://gitlab.com/clanwars/tournament-manager/issues/128)

## [1.3.1](https://gitlab.com/clanwars/tournament-manager/compare/1.3.0...1.3.1) (2022-09-29)


### Bug Fixes

* **payments:** fixes a bug where page of my payment cannot be loaded ([6f57ef3](https://gitlab.com/clanwars/tournament-manager/commit/6f57ef39ba7107d06f8c7013d89da4a883393e58))
* **payments:** fixes loading payment system page when user has no payment account yet ([7b55aa6](https://gitlab.com/clanwars/tournament-manager/commit/7b55aa6da7a62567d96ff5e296145dde621700d5))

# [1.3.0](https://gitlab.com/clanwars/tournament-manager/compare/1.2.3...1.3.0) (2022-09-29)


### Features

* **admission:** add step to create/update payment system accounts ([563c25e](https://gitlab.com/clanwars/tournament-manager/commit/563c25ea13ab34dec43199f30a510db08c039b29)), closes [#75](https://gitlab.com/clanwars/tournament-manager/issues/75)

## [1.2.3](https://gitlab.com/clanwars/tournament-manager/compare/1.2.2...1.2.3) (2022-09-27)


### Bug Fixes

* **lib:** adds pushing lib to nuget.org ([9bec615](https://gitlab.com/clanwars/tournament-manager/commit/9bec6156fa395e61a44744d63a37595931b240d6))

## [1.2.2](https://gitlab.com/clanwars/tournament-manager/compare/1.2.1...1.2.2) (2022-09-27)


### Bug Fixes

* **ci:** adds ci schema file ([fe4b2b1](https://gitlab.com/clanwars/tournament-manager/commit/fe4b2b12d4a927f6ec90d2dba5d7afbab0aac357))

## [1.2.1](https://gitlab.com/clanwars/tournament-manager/compare/1.2.0...1.2.1) (2022-09-27)


### Bug Fixes

* **lib:** publishes lib as nuget package to gitlab clanwars group registry ([a22085e](https://gitlab.com/clanwars/tournament-manager/commit/a22085eb6635289b498c48906d3781ea393605dd))

# [1.2.0](https://gitlab.com/clanwars/tournament-manager/compare/1.1.1...1.2.0) (2022-09-26)


### Features

* **tournament:** add date properties ([a9699d9](https://gitlab.com/clanwars/tournament-manager/commit/a9699d9c193f91d93b759d7099025f8c3397afae))

## [1.1.1](https://gitlab.com/clanwars/tournament-manager/compare/1.1.0...1.1.1) (2022-09-24)


### Bug Fixes

* **user:** add date properties ([110307b](https://gitlab.com/clanwars/tournament-manager/commit/110307b504ed2f08231abb86ed6eeafd39afc73f))

# [1.1.0](https://gitlab.com/clanwars/tournament-manager/compare/1.0.2...1.1.0) (2022-09-24)


### Bug Fixes

* **images:** adds browser caching for images ([616c773](https://gitlab.com/clanwars/tournament-manager/commit/616c7735940528fb10cb4e4edc31dd2fa102b9a3)), closes [#119](https://gitlab.com/clanwars/tournament-manager/issues/119)


### Features

* **feedback-form:** adds a form for feedback by the user ([76dd847](https://gitlab.com/clanwars/tournament-manager/commit/76dd8475a06a2df8125fbbff1c1e161a070ae195)), closes [#117](https://gitlab.com/clanwars/tournament-manager/issues/117)

## [1.0.2](https://gitlab.com/clanwars/tournament-manager/compare/1.0.1...1.0.2) (2022-09-11)


### Bug Fixes

* **layout:** fixes left side-nav autoclosing on small screens and hamburger click to toggle sidenav ([46d37df](https://gitlab.com/clanwars/tournament-manager/commit/46d37df8c8c465f63f123d66d6e9389df2168e64))
* **tournaments:** disable scroing points match if tournament is already finished/closes ([3c255d1](https://gitlab.com/clanwars/tournament-manager/commit/3c255d10db9b7bd6ba01785ab0ed3de510a4ca53))
* **ui:** several small ui polishings accross all pages ([3d39d32](https://gitlab.com/clanwars/tournament-manager/commit/3d39d3230313972b8388987febd963dc4f84b654))

## [1.0.1](https://gitlab.com/clanwars/tournament-manager/compare/1.0.0...1.0.1) (2022-09-10)


### Bug Fixes

* **clans:** fixes missing user avatar picture in pending users list ([b1442c5](https://gitlab.com/clanwars/tournament-manager/commit/b1442c55d0cc47f453610350fce4b528d8735e2b))

# 1.0.0 (2022-09-10)


### Bug Fixes

* **auth:** fixes initial redirection to authentication service provider if unauthenticated ([46764be](https://gitlab.com/clanwars/tournament-manager/commit/46764be0c9c3e61c3e3a13b613906199482c9ba5))
* **Build:** fix build error ([5a313a3](https://gitlab.com/clanwars/tournament-manager/commit/5a313a3973bec4206f0d62a400e4df889da7273d)), closes [#29](https://gitlab.com/clanwars/tournament-manager/issues/29)
* **clans:** fix clans filter toggle for showing only clans which are participating at current event ([f654103](https://gitlab.com/clanwars/tournament-manager/commit/f654103a71d22a5e91453f899091901714ccd7bb)), closes [#69](https://gitlab.com/clanwars/tournament-manager/issues/69)
* **clans:** fixes join view refresh after approving user to clan ([16370d5](https://gitlab.com/clanwars/tournament-manager/commit/16370d582535f5f1be1041c156dc87dbd231608d)), closes [#68](https://gitlab.com/clanwars/tournament-manager/issues/68)
* **Codequality:** ignore migrations folder ([11c30c2](https://gitlab.com/clanwars/tournament-manager/commit/11c30c2075c8e9e8fcb9763f93fad3fff006ce22)), closes [#23](https://gitlab.com/clanwars/tournament-manager/issues/23)
* **dashboard:** reload tournaments on signalr event ([41f7de3](https://gitlab.com/clanwars/tournament-manager/commit/41f7de3170fe927268304fe1f1ab2e2a29dc8762)), closes [#78](https://gitlab.com/clanwars/tournament-manager/issues/78)
* **database:** configure used database version ([6446ad9](https://gitlab.com/clanwars/tournament-manager/commit/6446ad91118ca1d0c9dcaa6966a685bf4af7a9d9))
* **dependencies:** update all dependencies ([91dbde7](https://gitlab.com/clanwars/tournament-manager/commit/91dbde79d51483774d892961f43b46e80abb58a1))
* **deployment:** update keycloak compose file ([d5dc39e](https://gitlab.com/clanwars/tournament-manager/commit/d5dc39e0334a469f80329b4e2e309a550a483c83))
* **deployment:** update keycloak compose file ([91cee00](https://gitlab.com/clanwars/tournament-manager/commit/91cee00b05fab773523749710616a458500d1887))
* **deps:** updates to new blazor image cropper component version ([b92c494](https://gitlab.com/clanwars/tournament-manager/commit/b92c494f020c735ef832fa58554abb23e312c562))
* **events-creation:** fixes bad request with too short event name ([3267f3c](https://gitlab.com/clanwars/tournament-manager/commit/3267f3ca9ae6ff1cdaa7d5f6c35debd485865a9f)), closes [#88](https://gitlab.com/clanwars/tournament-manager/issues/88) [#90](https://gitlab.com/clanwars/tournament-manager/issues/90)
* **fix:** arbeitsstand ([8aec108](https://gitlab.com/clanwars/tournament-manager/commit/8aec108ab0277afaad1026f8a977b008559728fa))
* **fix:** arbeitsstand ([de2d096](https://gitlab.com/clanwars/tournament-manager/commit/de2d0960d24d206bf5a9a111ea34d1da866d86a9))
* **fix:** fix ([f846acf](https://gitlab.com/clanwars/tournament-manager/commit/f846acf716f2cb5e952bfeea926e97ef3b619d6e))
* **fix:** fix !!!! ([9d7a278](https://gitlab.com/clanwars/tournament-manager/commit/9d7a27825833595f4f3d611bfaea5b3dee6f07f5))
* **frontend:** make backend base address configurable ([cce2323](https://gitlab.com/clanwars/tournament-manager/commit/cce23236a0608ce5f121b4a190fd18e9f7fd2196)), closes [#114](https://gitlab.com/clanwars/tournament-manager/issues/114)
* **frontend:** use HostEnvironment.BaseAddress ([210c8a7](https://gitlab.com/clanwars/tournament-manager/commit/210c8a7cb832588932a2f515d86183acc24eaa89))
* **httpclient:** send datetime in query string as ISO8601 and add JsonSerializerOptions ([b45dfe7](https://gitlab.com/clanwars/tournament-manager/commit/b45dfe746720273a0ac3071fa1cb4080ddf664c7)), closes [#87](https://gitlab.com/clanwars/tournament-manager/issues/87) [#91](https://gitlab.com/clanwars/tournament-manager/issues/91)
* **image:** add dotnet environment env ([64b9bf8](https://gitlab.com/clanwars/tournament-manager/commit/64b9bf80d16c08a2017fce5400bcee1379c2ef39))
* **image:** fix RUN command ([e638928](https://gitlab.com/clanwars/tournament-manager/commit/e638928419af780eaacd233e7cc583db02f235b9))
* **images:** fixes img sources to make use of configurable backend url ([53657bb](https://gitlab.com/clanwars/tournament-manager/commit/53657bb75d88c766cf12c7deb2ef8b282d808764))
* **Images:** fixes updating of entities with images ([7bd34c8](https://gitlab.com/clanwars/tournament-manager/commit/7bd34c890ad745497488c03815ffd1deff708307)), closes [#31](https://gitlab.com/clanwars/tournament-manager/issues/31)
* **image:** use env variables for appsettings configuration ([238a14a](https://gitlab.com/clanwars/tournament-manager/commit/238a14a6d6148251f7aa0d02e79f734eb9c2154f))
* **MapIdFilter:** fixes validation state handling in Filter ([5c052f4](https://gitlab.com/clanwars/tournament-manager/commit/5c052f4311ff1370363ad7232a517aabef432d4e))
* **Migrations:** add migration execution to startup ([11cbe26](https://gitlab.com/clanwars/tournament-manager/commit/11cbe2677418e9157c4e1432155a8a8acdc1d27d))
* **navigation:** reloads tournament list in navbar when tournaments changed ([9125889](https://gitlab.com/clanwars/tournament-manager/commit/912588961d246a8d9fe7681073c7272aedc78b02)), closes [#54](https://gitlab.com/clanwars/tournament-manager/issues/54)
* **news:** make UpdatedDate nullable ([5e1850e](https://gitlab.com/clanwars/tournament-manager/commit/5e1850ed7bf289ae13537959cd14c4f96e1fb42b)), closes [#98](https://gitlab.com/clanwars/tournament-manager/issues/98)
* **Project:** fixes folder structure ([6ea7583](https://gitlab.com/clanwars/tournament-manager/commit/6ea7583ad791936b4416cbbd0c6b260735f7bf54))
* **rest:** fixes unrecognized pageSize in query params ([26e52a4](https://gitlab.com/clanwars/tournament-manager/commit/26e52a4957aba97581309eddaf8df1bbf7c7aa8a)), closes [#35](https://gitlab.com/clanwars/tournament-manager/issues/35)
* **rooms:** fixes a bug that user images were not shown in rooms plan ([efd3a31](https://gitlab.com/clanwars/tournament-manager/commit/efd3a3159c3c39dd170b799581d5503d646c41b9))
* **rooms:** fixes newly created grid entries cannot be rotated or deleted ([bdfd808](https://gitlab.com/clanwars/tournament-manager/commit/bdfd808194566070fc74e3b21742914326b8d7a2)), closes [#99](https://gitlab.com/clanwars/tournament-manager/issues/99)
* **Rooms:** return a RoomResponse instead of a Room entity ([06c6200](https://gitlab.com/clanwars/tournament-manager/commit/06c620097c1a7d894fdc6734a9f8b31a79ce576c)), closes [#26](https://gitlab.com/clanwars/tournament-manager/issues/26)
* **Rooms:** small fixes ([7a234b6](https://gitlab.com/clanwars/tournament-manager/commit/7a234b6cd47727258a9eb4c41f9489b582fc1717))
* **Rooms:** update gridtypes and fix issue returning gridsquares ([dea8778](https://gitlab.com/clanwars/tournament-manager/commit/dea8778cfc5316180ced3c9cf66655f9d33a23b6))
* **rooms:** update room dialog form renders currently selected event correctly now ([53098df](https://gitlab.com/clanwars/tournament-manager/commit/53098dfa3bca66f8a2b93885cdca7a043207584a)), closes [#100](https://gitlab.com/clanwars/tournament-manager/issues/100)
* **threading:** fixes an issue with concurrent accesses to db context ([b8bb32d](https://gitlab.com/clanwars/tournament-manager/commit/b8bb32d50caba4549fc93b255fb9e088d007e201))
* **tournament-teams:** fix in create tournament team dialog ([ad2e8b5](https://gitlab.com/clanwars/tournament-manager/commit/ad2e8b57a80d7038c859803399f00542394f92ff)), closes [#95](https://gitlab.com/clanwars/tournament-manager/issues/95)
* **Tournament:** add players per team on creating a tournament ([b614d55](https://gitlab.com/clanwars/tournament-manager/commit/b614d551456cb31c439a6d1e1b8c9750ae42ad98))
* **tournament:** fix get all events ([5a8ed8f](https://gitlab.com/clanwars/tournament-manager/commit/5a8ed8f44a4863d3e2ced953ca51fc710099c17b))
* **Tournament:** fixes creating of tournaments with multiple tracked DB entries ([56ffbd2](https://gitlab.com/clanwars/tournament-manager/commit/56ffbd26025978e38a12fb33675bac556c49f65d))
* **Tournament:** fixes issues in validations for starting a tournament ([5563013](https://gitlab.com/clanwars/tournament-manager/commit/55630136df7205aaba0428578de69cf482c47ef7))
* **Tournament:** fixes touernament/image route ([2b8e9c6](https://gitlab.com/clanwars/tournament-manager/commit/2b8e9c663188519839d46b21f55889268406d8c3))
* **TournamentImage:** fixes a bug setting the return code twice ([058ef39](https://gitlab.com/clanwars/tournament-manager/commit/058ef3902514fa3bce349ae69f8d6d225713a0ad))
* **TournamentMatches:** adds MatchIndex property, to properly sort the matches in the rournament tre ([b0d1217](https://gitlab.com/clanwars/tournament-manager/commit/b0d1217442e339bf7c35cf1326f3d65712a8a374))
* **TournamentMatches:** shuffle teams ([c388ee5](https://gitlab.com/clanwars/tournament-manager/commit/c388ee5daa08e11c942d931cad7e59a8c86abedd))
* **TournamentResponse:** fixes imageUrl ([cd88f18](https://gitlab.com/clanwars/tournament-manager/commit/cd88f1805b123348fc07b5f40b0e53cd9766bd85))
* **Tournaments:** adds bye win generation for double elimination trees ([8614a9f](https://gitlab.com/clanwars/tournament-manager/commit/8614a9f7f328702b7b6d91721db854e1dd61a12b)), closes [#30](https://gitlab.com/clanwars/tournament-manager/issues/30)
* **Tournaments:** fixes loser bracket generation of x.5 matches ([6e0efc9](https://gitlab.com/clanwars/tournament-manager/commit/6e0efc997fb40e4be6f0961b0a9bef7d81b53d60))
* **tournaments:** fixes ui bug in double elimination tree ([adef9b7](https://gitlab.com/clanwars/tournament-manager/commit/adef9b7b4aefbad71bddb50c201d01b8837f119e)), closes [#101](https://gitlab.com/clanwars/tournament-manager/issues/101)
* **tournaments:** small ui improvements ([82348e7](https://gitlab.com/clanwars/tournament-manager/commit/82348e74935827ff2fd272517a22e40f235230f7))
* **tournaments:** use random names for 1-person-teams and bye-teams with specific prefixes ([a2938a2](https://gitlab.com/clanwars/tournament-manager/commit/a2938a259fb0f2fd60132632e514104516f90811))
* **tournamentteam:** add unique constraint on name and tournamentId ([7853a01](https://gitlab.com/clanwars/tournament-manager/commit/7853a0138ff04a8c3b54c04291d0075838775199)), closes [#102](https://gitlab.com/clanwars/tournament-manager/issues/102)
* **TournamentTeam:** do not count invitees as members of team when starting a tournament ([d0651ef](https://gitlab.com/clanwars/tournament-manager/commit/d0651ef89e282ebce2a7831e208af40406ffe3d9))
* **TournamentTeamMember:** fixes TournamentTeamMemberResponse to contain instances of User classes ([8953f81](https://gitlab.com/clanwars/tournament-manager/commit/8953f817daeacdd6c1f09c5ff10154d0f125b08c))
* **tournamentteams:** remove unused function to change permissions for tournamentteammember ([281a3a2](https://gitlab.com/clanwars/tournament-manager/commit/281a3a2d0ded1ef3bceab4c4eb8d648434a228bd)), closes [#83](https://gitlab.com/clanwars/tournament-manager/issues/83)
* **user-avatar:** improves ui design for uploading user avatar ([d1da9dc](https://gitlab.com/clanwars/tournament-manager/commit/d1da9dc1035acd9c38e9fb4c602981a1bc840bd1))
* **User:** adds validations for duplicate user nicknames and email ([a4fc5e0](https://gitlab.com/clanwars/tournament-manager/commit/a4fc5e0c16e7c80965cc8ff3571ce1bb6bafccc9)), closes [#24](https://gitlab.com/clanwars/tournament-manager/issues/24)
* **User:** fix adding clan to user table ([9e76b3a](https://gitlab.com/clanwars/tournament-manager/commit/9e76b3a1b09b4a6bc39bd4c87a9bdbea0e3cf611))
* **user:** new UserAvatar component and implementation on several pages ([a090708](https://gitlab.com/clanwars/tournament-manager/commit/a0907081c8f73771527ca0c56c8aa0ccb0e48e6a))
* **User:** seed admin user ([ccecf6d](https://gitlab.com/clanwars/tournament-manager/commit/ccecf6d0d075de15018ef0e1d41c1c147d25cd56))
* **User:** show Clan in UserResponse ([c014eb0](https://gitlab.com/clanwars/tournament-manager/commit/c014eb0610947b0cf2e635d5b252e25f0b168774))


### Code Refactoring

* **backend:** migrated project to dotnet 6 ([4ee95e8](https://gitlab.com/clanwars/tournament-manager/commit/4ee95e8f130d654851e7e8709841b5b5a5c9c031))


### Features

* **admission:** add ability to place a user to a seat ([a160ba2](https://gitlab.com/clanwars/tournament-manager/commit/a160ba2b4d0d2de4853a153b11a13b346d81bba0)), closes [#74](https://gitlab.com/clanwars/tournament-manager/issues/74)
* **admission:** adds adminission wizard ([f38bde5](https://gitlab.com/clanwars/tournament-manager/commit/f38bde5d659c9f67ccd3d8d7465c37d773481038)), closes [#71](https://gitlab.com/clanwars/tournament-manager/issues/71)
* **admission:** adds admission wizard page and keycloak backend ([f4afb41](https://gitlab.com/clanwars/tournament-manager/commit/f4afb412cb1e1fa7c85d40fd66f448455fe39f64)), closes [#77](https://gitlab.com/clanwars/tournament-manager/issues/77)
* **admission:** adds backend for creating a new external user in the keycloak user store ([7734ca2](https://gitlab.com/clanwars/tournament-manager/commit/7734ca214879d70bb43240f6042b9cff0f1acbb9)), closes [#73](https://gitlab.com/clanwars/tournament-manager/issues/73)
* **admission:** adds create user in user store form in first step of admission wizard ([496afa4](https://gitlab.com/clanwars/tournament-manager/commit/496afa4353964be0065e4e02fe8c5af3474d8b2f)), closes [#73](https://gitlab.com/clanwars/tournament-manager/issues/73)
* **Auth:** add password check and password update route ([65303d7](https://gitlab.com/clanwars/tournament-manager/commit/65303d767e1d2a6e66ae200e9ab9f6717b0296b5)), closes [#13](https://gitlab.com/clanwars/tournament-manager/issues/13)
* **Auth:** adds forcing the user to update his password ([e10b45e](https://gitlab.com/clanwars/tournament-manager/commit/e10b45e6f3e9a7faf6d985468db96bdd3064908f)), closes [#17](https://gitlab.com/clanwars/tournament-manager/issues/17)
* **auth:** adds logout button and authentication framework improvements ([f19990a](https://gitlab.com/clanwars/tournament-manager/commit/f19990a2f996e26b4fcbb0961822fc60708de962)), closes [#92](https://gitlab.com/clanwars/tournament-manager/issues/92)
* **Auth:** adds logout route ([46a1904](https://gitlab.com/clanwars/tournament-manager/commit/46a19042a78aaffe477f1c85ab63b75b5eed229d)), closes [#19](https://gitlab.com/clanwars/tournament-manager/issues/19)
* **Auth:** admin is allowed to change password for user ([bbd3f96](https://gitlab.com/clanwars/tournament-manager/commit/bbd3f961b0fe51bce439b661edbf7a302e327c13))
* **Authentication:** adds auth middleware for cookie authentication ([8da71af](https://gitlab.com/clanwars/tournament-manager/commit/8da71af8ddf6e81b118f0c82e53632ba3ffaf0f6))
* **auth:** updates to use keycloak 18 ([5c75ad3](https://gitlab.com/clanwars/tournament-manager/commit/5c75ad3eeb59b2a09be9db96d5848f0c36eb068f))
* **chat:** adds very simple REST interface for Chat module ([6581d26](https://gitlab.com/clanwars/tournament-manager/commit/6581d26c4add15a1e92b36c55a9825848d35c97f)), closes [#33](https://gitlab.com/clanwars/tournament-manager/issues/33)
* **clan:** add signalr notification for service calls ([8b7eec7](https://gitlab.com/clanwars/tournament-manager/commit/8b7eec738068826448bb1a87df68cebd372b7493)), closes [#93](https://gitlab.com/clanwars/tournament-manager/issues/93)
* **Clan:** adds property ClanTag ([e5887f2](https://gitlab.com/clanwars/tournament-manager/commit/e5887f24f47034b96c16a80cd8bc9ccc3d61b886)), closes [#5](https://gitlab.com/clanwars/tournament-manager/issues/5)
* **clans:** adds clan list view ([13df2f0](https://gitlab.com/clanwars/tournament-manager/commit/13df2f0946d8fe9fce60b1679d1e0ac0690ae685)), closes [#42](https://gitlab.com/clanwars/tournament-manager/issues/42)
* **Clans:** adds CRUD routes for Clans ([8109385](https://gitlab.com/clanwars/tournament-manager/commit/8109385aebbb8cba2605eb1ce7b3692990d110dd))
* **Clans:** adds join/leave routes ([14112e2](https://gitlab.com/clanwars/tournament-manager/commit/14112e28159421a35c95652725880040e8c3f339))
* **clans:** adds possibility to create a new clan (if not already a member of one) ([296bf19](https://gitlab.com/clanwars/tournament-manager/commit/296bf194acd55502cf3c2a1ee08d9e3c8ef9a7c0)), closes [#40](https://gitlab.com/clanwars/tournament-manager/issues/40)
* **clans:** owner is able to reject a user request to join a clan ([7bb6210](https://gitlab.com/clanwars/tournament-manager/commit/7bb6210f4a4e9b0e7f9025331f7872f14cc63bc6)), closes [#67](https://gitlab.com/clanwars/tournament-manager/issues/67)
* **clans:** show pending joins for clan owners ([7fe39e4](https://gitlab.com/clanwars/tournament-manager/commit/7fe39e49491e82733c5803a308660141c19ac039)), closes [#41](https://gitlab.com/clanwars/tournament-manager/issues/41)
* **configuration:** use configuration to configure urls ([7b5d8ef](https://gitlab.com/clanwars/tournament-manager/commit/7b5d8ef35e536f2bbd49cb74c306aedcbe09ea2e)), closes [#84](https://gitlab.com/clanwars/tournament-manager/issues/84)
* **container:** implement building multiple architectures ([1b4e5fa](https://gitlab.com/clanwars/tournament-manager/commit/1b4e5fa8ad9bcce2e3325e5c90f0db884fa89ad8)), closes [#105](https://gitlab.com/clanwars/tournament-manager/issues/105)
* **Controller:** fix todos ([1079cb6](https://gitlab.com/clanwars/tournament-manager/commit/1079cb6e2cf2e1ed65f3cd529f7d94f7cbb0c0e6))
* **CurrentUser:** adds /auth/user route to get current authenticated user ([083c001](https://gitlab.com/clanwars/tournament-manager/commit/083c0017a04d83ebf8bd2b88b0777b33454eb0ac))
* **DB:** adds initial migration including the tournament tables ([0a9c035](https://gitlab.com/clanwars/tournament-manager/commit/0a9c035719213b90ae6a306048bb4b97f414dd3a))
* **event:** adds event change dialog in EventManager ([f68c435](https://gitlab.com/clanwars/tournament-manager/commit/f68c435ad53f52c8de78a3e9fc7d2af25814b324)), closes [#55](https://gitlab.com/clanwars/tournament-manager/issues/55)
* **Events:** add join/leave actions for Users ([3336bb2](https://gitlab.com/clanwars/tournament-manager/commit/3336bb2e4d5a7a9eb5f678f7466ea205e51cf803))
* **Events:** adds CRUD routes for events ([be510b0](https://gitlab.com/clanwars/tournament-manager/commit/be510b0061203bf39e03c3aa92b1dd9b25512094))
* **Events:** implements event repository ([e076f56](https://gitlab.com/clanwars/tournament-manager/commit/e076f56b4d8995674b62f7e5c145317ecc7b9a79))
* **Events:** implements missing members in EventRepository ([a122408](https://gitlab.com/clanwars/tournament-manager/commit/a122408a1c76d0b05e49ebc19b612a26fa4875bf))
* **frontend:** total rewrite the project to use a blazor frontend application ([c264815](https://gitlab.com/clanwars/tournament-manager/commit/c264815cb3b5e3d0ee34cae57884ed9895c31697))
* **image:** add frontend and backend image ([3c52571](https://gitlab.com/clanwars/tournament-manager/commit/3c52571c8c11b6a87c5edef91aaf98cbb081614c)), closes [#104](https://gitlab.com/clanwars/tournament-manager/issues/104)
* **Images:** adds image to User and TournamentTeam ([e6ed7fc](https://gitlab.com/clanwars/tournament-manager/commit/e6ed7fc35c82348ba5248b4d65dd7586af227554)), closes [#18](https://gitlab.com/clanwars/tournament-manager/issues/18)
* **Images:** adds separate image repository to improve image handling ([0f6e04a](https://gitlab.com/clanwars/tournament-manager/commit/0f6e04ab944f3c72c5a5753346781724e66dface)), closes [#31](https://gitlab.com/clanwars/tournament-manager/issues/31)
* **images:** implement new image handling ([f29c9cd](https://gitlab.com/clanwars/tournament-manager/commit/f29c9cdd585af0bcaa01619bb25f95baac3f8f14)), closes [#94](https://gitlab.com/clanwars/tournament-manager/issues/94)
* **images:** use hashIds ([f92d2ab](https://gitlab.com/clanwars/tournament-manager/commit/f92d2ab255d2ff587d055e63fd736d8e4a3c5fdc)), closes [#103](https://gitlab.com/clanwars/tournament-manager/issues/103)
* initial commit ([b25eeb3](https://gitlab.com/clanwars/tournament-manager/commit/b25eeb3acce664799866dda321e42553cf9414ef))
* **Initial:** Commit ([b7c373c](https://gitlab.com/clanwars/tournament-manager/commit/b7c373cc8eed0973f783b4cd40f82c9bb61b04b1))
* **layout:** adds favicon ([fe2ea75](https://gitlab.com/clanwars/tournament-manager/commit/fe2ea7561c563b12b5701f3bbf5c8a29127c538e)), closes [#49](https://gitlab.com/clanwars/tournament-manager/issues/49)
* **live-notifications:** makes notifications hub url configurable ([6533a21](https://gitlab.com/clanwars/tournament-manager/commit/6533a21742bc921d46157a89ba3700a454251b56)), closes [#51](https://gitlab.com/clanwars/tournament-manager/issues/51)
* **news:** adds news at dashboard ([8b6fbd8](https://gitlab.com/clanwars/tournament-manager/commit/8b6fbd87c8851e73bd7825d850cb29e6197de237)), closes [#47](https://gitlab.com/clanwars/tournament-manager/issues/47) [#48](https://gitlab.com/clanwars/tournament-manager/issues/48)
* **news:** adds News entity including all CRUD operations ([8094871](https://gitlab.com/clanwars/tournament-manager/commit/809487195d25d5442d1ad6a0b9be939e382e3309)), closes [#32](https://gitlab.com/clanwars/tournament-manager/issues/32)
* **REST:** add GET route for users ([7328a29](https://gitlab.com/clanwars/tournament-manager/commit/7328a29b18e0bfc062e84be973c23a7d16d93936)), closes [#1](https://gitlab.com/clanwars/tournament-manager/issues/1)
* **REST:** add grant/revoke routes for clans and tournament teams ([18a993e](https://gitlab.com/clanwars/tournament-manager/commit/18a993ec7d3c9af6f523e06fb780388a7ec5f0b9)), closes [#3](https://gitlab.com/clanwars/tournament-manager/issues/3)
* **REST:** add POST route to create new user ([aba7618](https://gitlab.com/clanwars/tournament-manager/commit/aba7618f20e3f112e7e4dd4add2cfbab494866e9))
* **rooms:** add possibility for moderators to place users ([3d390e2](https://gitlab.com/clanwars/tournament-manager/commit/3d390e2b5fb8777bbdaa9ce3c0cb5b0066279b96)), closes [#65](https://gitlab.com/clanwars/tournament-manager/issues/65)
* **Rooms:** Adds additional fields -- adds rotation property ([61f2576](https://gitlab.com/clanwars/tournament-manager/commit/61f2576bbd3a918a422ef2d74c29824494fddb35))
* **rooms:** adds all grid types to room plans ([eeadf6d](https://gitlab.com/clanwars/tournament-manager/commit/eeadf6d2a6fd9be1c9f4f0b48d7db1d8defede6f)), closes [#43](https://gitlab.com/clanwars/tournament-manager/issues/43)
* **rooms:** adds create and edit dialogs for rooms ([02f0e36](https://gitlab.com/clanwars/tournament-manager/commit/02f0e36a03cde9ced7b96f23c99217056a509f0f)), closes [#58](https://gitlab.com/clanwars/tournament-manager/issues/58)
* **rooms:** adds editor for rooms ([98dec4b](https://gitlab.com/clanwars/tournament-manager/commit/98dec4bcd199656dc351d7dff7f6c22e9514533c)), closes [#44](https://gitlab.com/clanwars/tournament-manager/issues/44)
* **Rooms:** adds REST interface for Rooms and their management ([494b91c](https://gitlab.com/clanwars/tournament-manager/commit/494b91c4d1dbc9eff3bb7b42b9632e9df12bfe5f)), closes [#20](https://gitlab.com/clanwars/tournament-manager/issues/20)
* **Rooms:** adds retoation protperty to gridSquares ([f43222d](https://gitlab.com/clanwars/tournament-manager/commit/f43222d8940cfb38dc32208cdd4afc6f13bb37e8)), closes [#25](https://gitlab.com/clanwars/tournament-manager/issues/25)
* **Rooms:** returns gridSquare on updating grid and placing users ([148da6e](https://gitlab.com/clanwars/tournament-manager/commit/148da6e443ee2b0c32e2647f3e9a77e5c6567c1c))
* **rooms:** show number of available seats ([a9ecabc](https://gitlab.com/clanwars/tournament-manager/commit/a9ecabc9bbc421fdee6551c854dae14d93f2e3d4)), closes [#70](https://gitlab.com/clanwars/tournament-manager/issues/70)
* **rooms:** show user information on hovering a seat occupied by a user ([ac41cae](https://gitlab.com/clanwars/tournament-manager/commit/ac41cae91a7b8435306739e378c805d86a3d9db2)), closes [#59](https://gitlab.com/clanwars/tournament-manager/issues/59)
* **Signalr:** add signalr dummies ([95ee05f](https://gitlab.com/clanwars/tournament-manager/commit/95ee05f68ba7673ef003b9240aa78466f5386dcc))
* **signalr:** add signalr hub to notify clients about state changes (for live reloads) ([8f71c9b](https://gitlab.com/clanwars/tournament-manager/commit/8f71c9b61ec159e7aca9b52aa59355d468d9311a)), closes [#50](https://gitlab.com/clanwars/tournament-manager/issues/50)
* **signalr:** adds live refreshs for user avatar in top bar (after avatar update) and tournament tree (after scored match) ([03572b1](https://gitlab.com/clanwars/tournament-manager/commit/03572b10223dc9b3c82bc8fe0e0e8df3d0c77822)), closes [#106](https://gitlab.com/clanwars/tournament-manager/issues/106) [#107](https://gitlab.com/clanwars/tournament-manager/issues/107)
* **Tournament:** adds image to Tournament ([808a574](https://gitlab.com/clanwars/tournament-manager/commit/808a574a13071ca0aab82a01cd172212a35b9eee)), closes [#16](https://gitlab.com/clanwars/tournament-manager/issues/16)
* **Tournament:** generate double elimination tournament tree ([043286d](https://gitlab.com/clanwars/tournament-manager/commit/043286d448d20dfaa6ba49f1cc2c8073717ba424)), closes [#6](https://gitlab.com/clanwars/tournament-manager/issues/6)
* **Tournament:** make Image optional in UpdateTournament ([b00f602](https://gitlab.com/clanwars/tournament-manager/commit/b00f60296d3a3422af5551ae6f7217cdcfa56e30))
* **tournaments:** add dialog to score matches ([e7c8baf](https://gitlab.com/clanwars/tournament-manager/commit/e7c8baf3e0a3e5d72fca6345fc80a647f54fb65c)), closes [#97](https://gitlab.com/clanwars/tournament-manager/issues/97)
* **Tournaments:** add single elimination tournament tree generation ([5195c0a](https://gitlab.com/clanwars/tournament-manager/commit/5195c0a005f61bc352d5610f82f02b8811fa6382)), closes [#8](https://gitlab.com/clanwars/tournament-manager/issues/8)
* **tournaments:** adds match templates to tournament trees ([4f2adb2](https://gitlab.com/clanwars/tournament-manager/commit/4f2adb2fa0a09ab8c3aa11075a4e6b630a27185a)), closes [#61](https://gitlab.com/clanwars/tournament-manager/issues/61)
* **tournaments:** adds modal to set image per tournament, refactored tournament and tournament overview pages ([0bca776](https://gitlab.com/clanwars/tournament-manager/commit/0bca776d80532e1cff8d6399cbc6fb69f5e82901)), closes [#85](https://gitlab.com/clanwars/tournament-manager/issues/85)
* **tournaments:** adds points tournament mode ([717ac7d](https://gitlab.com/clanwars/tournament-manager/commit/717ac7d53941c402bdf403f154770f65d07b987c)), closes [#110](https://gitlab.com/clanwars/tournament-manager/issues/110)
* **Tournaments:** adds route for scoring matches ([dbecf3f](https://gitlab.com/clanwars/tournament-manager/commit/dbecf3f267e175a86fcce026590753fafc54d8da)), closes [#9](https://gitlab.com/clanwars/tournament-manager/issues/9)
* **Tournaments:** adds tournament REST routes ([e21eedb](https://gitlab.com/clanwars/tournament-manager/commit/e21eedb1f7cd463081b86746d9e854286ae68765))
* **tournaments:** adds ui functions for editing and managing tournaments ([e9f0e36](https://gitlab.com/clanwars/tournament-manager/commit/e9f0e36ffc12aeb2676839e264cd4a44b5cc5299))
* **tournaments:** adds views for singleplayer tournaments ([e45cbcd](https://gitlab.com/clanwars/tournament-manager/commit/e45cbcdfc70b71f35ead8b5defb1b013408654d0)), closes [#76](https://gitlab.com/clanwars/tournament-manager/issues/76)
* **Tournaments:** generate double KO matches ([a409d7b](https://gitlab.com/clanwars/tournament-manager/commit/a409d7b41f87715128a3461933f1ba9e72545fa2)), closes [#6](https://gitlab.com/clanwars/tournament-manager/issues/6)
* **Tournaments:** Score Bye wins when starting a tournament ([e2368b1](https://gitlab.com/clanwars/tournament-manager/commit/e2368b1b4b9ada8f278092c4d56fa12bbca97380))
* **tournamentteam:** add components to display tournament teams ([691c79a](https://gitlab.com/clanwars/tournament-manager/commit/691c79a51a7dc4a876cc4ab7e7af80ce110763d5)), closes [#45](https://gitlab.com/clanwars/tournament-manager/issues/45)
* **TournamentTeam:** delete Team if leaver is last Owner ([a8e4a87](https://gitlab.com/clanwars/tournament-manager/commit/a8e4a878ab0363e4c27174797d26a3d164cc39c8)), closes [#15](https://gitlab.com/clanwars/tournament-manager/issues/15)
* **TournamentTeams:** adds REST routes ([edab414](https://gitlab.com/clanwars/tournament-manager/commit/edab4148a44da69e915660068f6a329720cf7aa3))
* **TournamentTeams:** do allow to create team only if tournament is "OpenForRegistration" ([ddc25b6](https://gitlab.com/clanwars/tournament-manager/commit/ddc25b62c945923a53c86440f3df06e12efe354f))
* **TournamentTeams:** prevent changing a teams after it is sealed by starting a tournament ([98991b9](https://gitlab.com/clanwars/tournament-manager/commit/98991b9c23ed9ac687e391ada7f769e72d798d55))
* **TournamentTeams:** seal teams on tournament start and check team member count ([0bb38d3](https://gitlab.com/clanwars/tournament-manager/commit/0bb38d3326581e56acedcf01e49dc53d3aeb91b6))
* **tournamentteams:** update list when team added to tournament ([6e067eb](https://gitlab.com/clanwars/tournament-manager/commit/6e067eb46f12063607241cd65b4aa2f60be1d1e2)), closes [#96](https://gitlab.com/clanwars/tournament-manager/issues/96)
* **user:** force user to complete profile if not already filled out all required fields ([bab7770](https://gitlab.com/clanwars/tournament-manager/commit/bab77709252cfdca75ef8058407baf31957d3567)), closes [#39](https://gitlab.com/clanwars/tournament-manager/issues/39)
* **user:** make user profile configurable including users avatar ([c3a0f6e](https://gitlab.com/clanwars/tournament-manager/commit/c3a0f6e9daa3b0f0690de43c9449388e6010afa4)), closes [#57](https://gitlab.com/clanwars/tournament-manager/issues/57)
* **Users:** adds update/delete routes for Users ([aa6b81e](https://gitlab.com/clanwars/tournament-manager/commit/aa6b81e42b043a9b058378b4594ad2974b19ee5e))
* **Validation:** add team not full validation ([768be57](https://gitlab.com/clanwars/tournament-manager/commit/768be578fbb570bb6184eeef7906005cdce58070)), closes [#11](https://gitlab.com/clanwars/tournament-manager/issues/11)


### BREAKING CHANGES

* **backend:** all migrations got reinitialized!
